package entmoot.shelob.api.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ArrayUtilsTest {

  public static final double[][] TEST_ARRAY = {
    {1, 2, 3},
    {4, 5, 6},
    {7, 8, 9}
  };
  public static final double[][] OTHER_TEST_ARRAY = {
    {3, 2, 1},
    {6, 5, 4},
    {9, 8, 7}
  };
  public static final double[][] TEST_ARRAY_DOT_OTHER_TEST_ARRAY = {
    {42, 36, 30},
    {96, 81, 66},
    {150, 126, 102}
  };
  public static final double[][] TEST_ARRAY_TRANSPOSED = {
    {1, 4, 7},
    {2, 5, 8},
    {3, 6, 9}
  };
  public static final double[] TEST_VECTOR = {2, 3, 4};
  public static final double[][] TEST_ARRAY_VECTOR_SUM = {
    {3, 5, 7},
    {6, 8, 10},
    {9, 11, 13}
  };
  public static final double[] TEST_ARRAY_DOT_VECTOR = {20, 47, 74};
  public static final double VECTOR_DOT_VECTOR = 29;

  @Test
  void testTranspose() {
    assertArrayEquals(TEST_ARRAY_TRANSPOSED, ArrayUtils.transpose(TEST_ARRAY));
  }

  @Test
  void testSumArrayWithVector() {
    assertArrayEquals(TEST_ARRAY_VECTOR_SUM, ArrayUtils.sum(TEST_ARRAY, TEST_VECTOR));
  }

  @Test
  void testDotArrays() {
    assertArrayEquals(
        TEST_ARRAY_DOT_OTHER_TEST_ARRAY, ArrayUtils.dot(TEST_ARRAY, OTHER_TEST_ARRAY));
  }

  @Test
  void testDotArrayAndVector() {
    assertArrayEquals(TEST_ARRAY_DOT_VECTOR, ArrayUtils.dot(TEST_ARRAY, TEST_VECTOR));
  }

  @Test
  void testDotVectors() {
    assertEquals(VECTOR_DOT_VECTOR, ArrayUtils.dot(TEST_VECTOR, TEST_VECTOR));
  }

  @Test
  void testApplyFunction() {}
}
