package entmoot.shelob.api.network.layer.neuron.synapse;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SynapseInitializerTest {

  @Test
  void testInitializeSynapse() {
    double expected = 0.3;
    SynapseInitializer synapseInitializer = () -> expected;
    assertEquals(expected, synapseInitializer.initializeSynapse());
    assertEquals(expected, synapseInitializer.initializeSynapse());
    assertEquals(expected, synapseInitializer.getAsDouble());
  }
}
