package entmoot.shelob.api.network.layer.neuron;

import entmoot.shelob.api.normalization.NormalizationFunctions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NeuronTest {

  public static final int TEST_BIAS = 1;
  public static final int TEST_SYNAPSE = 1;
  public static final Neuron TEST_NEURON =
      Neuron.create(() -> TEST_SYNAPSE, () -> TEST_BIAS, 2, NormalizationFunctions.RECTIFIER);

  @Test
  void testGetSynapticWeights() {
    double[] synapticWeights = TEST_NEURON.getSynapticWeights();
    for (double synapticWeight : synapticWeights) {
      assertEquals(TEST_SYNAPSE, synapticWeight);
    }
  }

  @Test
  void testGetBias() {
    assertEquals(TEST_BIAS, TEST_NEURON.getBias());
  }

  @Test
  void testCreate() {
    assertDoesNotThrow(
        () ->
            Neuron.create(
                () -> TEST_SYNAPSE, () -> TEST_BIAS, 2, NormalizationFunctions.RECTIFIER));
  }

  @Test
  void testNeuronApplyAsDouble() {
    double expected = 0.5;
    assertEquals(expected, TEST_NEURON.applyAsDouble(expected));
  }
}
