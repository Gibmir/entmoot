package entmoot.shelob.api.normalization;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NormalizationFunctionTest {

  public static final double DELTA = 1E-3;

  @Test
  void testNormalize() {
    NormalizationFunction normalizationFunction = value -> value;
    assertEquals(0.1, normalizationFunction.normalize(0.1));
    assertEquals(0.1, normalizationFunction.normalize(0.1));
    assertEquals(0.1, normalizationFunction.applyAsDouble(0.1));
  }

  @Test
  void testSigmoid() {
    assertEquals(0.377, NormalizationFunctions.SIGMOID.applyAsDouble(0.5), DELTA);
    assertEquals(0.622, NormalizationFunctions.SIGMOID.applyAsDouble(-0.5), DELTA);
  }

  @Test
  void testDerivativeSigmoid() {
    assertEquals(0.142, NormalizationFunctions.DERIVATIVE_SIGMOID.applyAsDouble(0.5), DELTA);
    assertEquals(0.387, NormalizationFunctions.DERIVATIVE_SIGMOID.applyAsDouble(-0.5), DELTA);
  }

  @Test
  void testRectifier() {
    assertEquals(0.5, NormalizationFunctions.RECTIFIER.applyAsDouble(0.5), DELTA);
    assertEquals(0.0, NormalizationFunctions.RECTIFIER.applyAsDouble(-0.5), DELTA);
  }
}
