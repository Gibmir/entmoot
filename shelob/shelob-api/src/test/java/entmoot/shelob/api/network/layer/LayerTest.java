package entmoot.shelob.api.network.layer;

import entmoot.shelob.api.normalization.NormalizationFunctions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class LayerTest {

  public static final Layer LAYER = Layer.builder().build(1, 1);

  @Test
  void forward() {
    assertDoesNotThrow(() -> LAYER.forward(new double[][] {{1}}));
  }

  @Test
  void builder() {
    assertDoesNotThrow(
        () ->
            Layer.builder()
                .biasInitializer(() -> 1)
                .normalizationFunction(NormalizationFunctions.RECTIFIER)
                .synapseInitializer(() -> 1)
                .build(1, 1));
  }
}
