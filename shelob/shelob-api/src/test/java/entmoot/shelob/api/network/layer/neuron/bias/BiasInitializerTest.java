package entmoot.shelob.api.network.layer.neuron.bias;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BiasInitializerTest {

  @Test
  void testInitBias() {
    double expected = 0.7;
    BiasInitializer biasInitializer = () -> expected;
    assertEquals(expected, biasInitializer.initializeBias());
    assertEquals(expected, biasInitializer.initializeBias());
    assertEquals(expected, biasInitializer.getAsDouble());
  }
}
