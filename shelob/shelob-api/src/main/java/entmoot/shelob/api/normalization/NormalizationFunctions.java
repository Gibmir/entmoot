package entmoot.shelob.api.normalization;

public enum NormalizationFunctions implements NormalizationFunction {
  SIGMOID(x -> 1 / (1 + Math.exp(x))),
  DERIVATIVE_SIGMOID(x -> SIGMOID.normalize(x) * (1 - SIGMOID.normalize(-x))),
  RECTIFIER(x -> Math.max(0d, x));

  private final NormalizationFunction normalizationFunction;

  NormalizationFunctions(NormalizationFunction normalizationFunction) {
    this.normalizationFunction = normalizationFunction;
  }

  @Override
  public double applyAsDouble(double operand) {
    return normalizationFunction.normalize(operand);
  }
}
