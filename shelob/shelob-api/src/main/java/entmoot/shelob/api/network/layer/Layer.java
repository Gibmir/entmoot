package entmoot.shelob.api.network.layer;

import entmoot.shelob.api.network.layer.neuron.Neuron;
import entmoot.shelob.api.network.layer.neuron.bias.BiasInitializer;
import entmoot.shelob.api.network.layer.neuron.synapse.SynapseInitializer;
import entmoot.shelob.api.normalization.NormalizationFunction;
import entmoot.shelob.api.normalization.NormalizationFunctions;
import entmoot.shelob.api.utils.ArrayUtils;

import java.util.function.DoubleSupplier;
import java.util.function.DoubleUnaryOperator;

/** Represents neural network layer. */
public class Layer {

  private final Neuron[] neurons;

  public Layer(Neuron[] neurons) {
    this.neurons = neurons;
  }

  /**
   * Take inputs and process it with this layer.
   *
   * @param inputs input data array
   * @return output
   */
  public double[][] forward(double[][] inputs) {
    double[][] weights = new double[neurons.length][];
    double[] biases = new double[neurons.length];
    for (int i = 0; i < neurons.length; i++) {
      weights[i] = neurons[i].getSynapticWeights();
      biases[i] = neurons[i].getBias();
    }
    return ArrayUtils.apply(ArrayUtils.sum(ArrayUtils.dot(inputs, weights), biases), neurons);
  }

  /**
   * Creates builder for layer.
   *
   * @return layer builder
   */
  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private SynapseInitializer synapseInitializer = Math::random;
    private BiasInitializer biasInitializer = Math::random;
    private NormalizationFunction normalizationFunction = NormalizationFunctions.RECTIFIER;

    /**
     * Sets bias initializer.
     *
     * @param biasInitializer bias initializer
     * @return builder
     */
    public Builder biasInitializer(BiasInitializer biasInitializer) {
      this.biasInitializer = biasInitializer;
      return this;
    }

    /**
     * Sets bias initializer functional representation.
     *
     * @param biasInitializer functional representation
     * @return builder
     */
    public Builder biasInitializer(DoubleSupplier biasInitializer) {
      this.biasInitializer = biasInitializer::getAsDouble;
      return this;
    }

    /**
     * Provides builder's bias initializer.
     *
     * @return bias initializer
     */
    public BiasInitializer biasInitializer() {
      return biasInitializer;
    }

    /**
     * Sets synapse initializer.
     *
     * @param synapseInitializer synapse initializer
     * @return builder
     */
    public Builder synapseInitializer(SynapseInitializer synapseInitializer) {
      this.synapseInitializer = synapseInitializer;
      return this;
    }

    /**
     * Sets synapse initializer functional representation.
     *
     * @param synapseInitializer functional representation
     * @return builder
     */
    public Builder synapseInitializer(DoubleSupplier synapseInitializer) {
      this.synapseInitializer = synapseInitializer::getAsDouble;
      return this;
    }

    /**
     * Provides builder's synapse initializer.
     *
     * @return synapse initializer
     */
    public SynapseInitializer synapseInitializer() {
      return synapseInitializer;
    }

    /**
     * Sets normalization function.
     *
     * @param normalizationFunction normalization function
     * @return builder
     */
    public Builder normalizationFunction(NormalizationFunction normalizationFunction) {
      this.normalizationFunction = normalizationFunction;
      return this;
    }

    /**
     * Sets normalization function functional representation.
     *
     * @param doubleUnaryOperator functional representation
     * @return builder
     */
    public Builder normalizationFunction(DoubleUnaryOperator doubleUnaryOperator) {
      this.normalizationFunction = doubleUnaryOperator::applyAsDouble;
      return this;
    }

    /**
     * Provides builder's normalization function.
     *
     * @return normalization function
     */
    public NormalizationFunction normalizationFunction() {
      return normalizationFunction;
    }

    /**
     * Build {@link Layer} with specified parameters.
     *
     * @param inputCount input data size
     * @param neuronCount neurons count
     * @return neural network layer
     */
    public Layer build(int inputCount, int neuronCount) {
      Neuron[] neurons = new Neuron[neuronCount];
      for (int i = 0; i < neurons.length; i++) {
        neurons[i] =
            Neuron.create(synapseInitializer, biasInitializer, inputCount, normalizationFunction);
      }
      return new Layer(neurons);
    }
  }
}
