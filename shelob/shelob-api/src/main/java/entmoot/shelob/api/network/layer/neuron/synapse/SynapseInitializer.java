package entmoot.shelob.api.network.layer.neuron.synapse;

import java.util.function.DoubleSupplier;

@FunctionalInterface
public interface SynapseInitializer extends DoubleSupplier {
  /**
   * Calculates synapse value.
   *
   * @return initialized synaptic weight
   */
  default double initializeSynapse() {
    return getAsDouble();
  }
}
