package entmoot.shelob.api.network.layer.neuron;

import entmoot.shelob.api.network.layer.neuron.bias.BiasInitializer;
import entmoot.shelob.api.network.layer.neuron.synapse.SynapseInitializer;
import entmoot.shelob.api.normalization.NormalizationFunction;

import java.util.function.DoubleUnaryOperator;

/**
 * Represents neural network neuron.
 *
 * @author Pavel Usachev
 */
public class Neuron implements DoubleUnaryOperator {
  private final double[] synapticWeights;
  private final double bias;
  private final NormalizationFunction normalizationFunction;

  private Neuron(
      double[] synapticWeights, double bias, NormalizationFunction normalizationFunction) {
    this.synapticWeights = synapticWeights;
    this.bias = bias;
    this.normalizationFunction = normalizationFunction;
  }

  /**
   * Provides neuron synaptic weights.
   *
   * @return synaptic weights
   */
  public double[] getSynapticWeights() {
    return synapticWeights;
  }

  /**
   * Provides neuron bias.
   *
   * @return neuron bias
   */
  public double getBias() {
    return bias;
  }

  /**
   * Creates neuron with specified parameters.
   *
   * @param synapseInitializer synapse initializer
   * @param biasInitializer bias initializer
   * @param inputSize input data size
   * @param normalizationFunction normalization function
   * @return neuron
   */
  public static Neuron create(
      SynapseInitializer synapseInitializer,
      BiasInitializer biasInitializer,
      int inputSize,
      NormalizationFunction normalizationFunction) {
    double[] synapticWeights = new double[inputSize];
    for (int i = 0; i < inputSize; i++) {
      synapticWeights[i] = synapseInitializer.initializeSynapse();
    }
    return new Neuron(synapticWeights, biasInitializer.initializeBias(), normalizationFunction);
  }

  @Override
  public double applyAsDouble(double operand) {
    return normalizationFunction.applyAsDouble(operand);
  }
}
