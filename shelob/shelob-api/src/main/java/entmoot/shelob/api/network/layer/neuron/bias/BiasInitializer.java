package entmoot.shelob.api.network.layer.neuron.bias;

import java.util.function.DoubleSupplier;

@FunctionalInterface
public interface BiasInitializer extends DoubleSupplier {
  /**
   * Calculates bias value.
   *
   * @return initialized bias
   */
  default double initializeBias() {
    return getAsDouble();
  }
}
