package entmoot.shelob.api.normalization;

import java.util.function.DoubleUnaryOperator;

@FunctionalInterface
public interface NormalizationFunction extends DoubleUnaryOperator {

  /**
   * Perform normalization for specified value.
   *
   * @param value double value
   * @return normalized value
   */
  default double normalize(double value) {
    return applyAsDouble(value);
  }
}
