package entmoot.shelob.api.utils;

import java.util.function.DoubleUnaryOperator;

public class ArrayUtils {
  private ArrayUtils() {}

  /**
   * Transpose specified array.
   *
   * @param array array to transpose
   * @return transposed array
   */
  public static double[][] transpose(double[][] array) {
    double[][] transposed = new double[array[0].length][array.length];
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array[0].length; j++) {
        transposed[j][i] = array[i][j];
      }
    }
    return transposed;
  }

  /**
   * Perform dot operation between array and vector.
   *
   * @param matrix matrix
   * @param vector vector
   * @return result vector
   */
  public static double[] dot(double[][] matrix, double[] vector) {
    double[] resultVector = new double[matrix.length];
    for (int i = 0; i < resultVector.length; i++) {
      resultVector[i] = dot(vector, matrix[i]);
    }
    return resultVector;
  }

  /**
   * Perform dot operation between two vectors.
   *
   * @param vector vector
   * @param otherVector other vector
   * @return result
   */
  public static double dot(double[] vector, double[] otherVector) {
    double sum = 0;
    for (int i = 0; i < vector.length; i++) {
      sum += vector[i] * otherVector[i];
    }
    return sum;
  }

  /**
   * Perform dot operation between two arrays.
   *
   * @param array array
   * @param otherArray other array
   * @return result array
   */
  public static double[][] dot(double[][] array, double[][] otherArray) {
    int columnsLength = array[0].length; // array columns length
    int otherRowsLength = otherArray.length; // otherArray rows length
    if (columnsLength != otherRowsLength) {
      String message =
          String.format(
              "Can't perform dot operation.Columns length(%d,%d) not equals",
              columnsLength, otherRowsLength);
      throw new IllegalArgumentException(message);
    }
    int rowsLength = array.length; // m result rows length
    int otherColumnsLength = otherArray[0].length; // m result columns length
    double[][] resultArray = new double[rowsLength][otherColumnsLength];
    for (int i = 0; i < rowsLength; i++) { // rows from array
      for (int j = 0; j < otherColumnsLength; j++) { // columns from otherArray
        for (int k = 0; k < columnsLength; k++) { // columns from array
          resultArray[i][j] += array[i][k] * otherArray[k][j];
        }
      }
    }
    return resultArray;
  }

  /**
   * Performs sum between array and vector.
   *
   * @param array array
   * @param vector vector
   * @return result array
   */
  public static double[][] sum(double[][] array, double[] vector) {
    double[][] result = new double[array.length][];
    for (int i = 0; i < array.length; i++) {
      int rowLength = array[i].length;
      double[] resultRow = new double[rowLength];
      for (int j = 0; j < rowLength; j++) {
        resultRow[j] = array[i][j] + vector[j];
      }
      result[i] = resultRow;
    }
    return result;
  }

  /**
   * Apply specified function to each row of array.
   *
   * @param array array
   * @param doubleOperator functions
   * @return processed array
   */
  public static double[][] apply(double[][] array, DoubleUnaryOperator[] doubleOperator) {
    double[][] result = new double[array.length][];
    for (int i = 0; i < array.length; i++) {
      int rowLength = array[i].length;
      double[] resultRow = new double[rowLength];
      for (int j = 0; j < rowLength; j++) {
        resultRow[j] = doubleOperator[i].applyAsDouble(array[i][j]);
      }
      result[i] = resultRow;
    }
    return result;
  }
}
