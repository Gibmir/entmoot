package entmoot.composition.environment;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.classifier.builder.FromSubsetBuilder;
import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.composition.model.ensemble.Ensemble;
import entmoot.composition.model.ensemble.creators.AbstractEnsembleCreator;
import entmoot.composition.model.ensemble.simple.SimpleEnsemble;
import entmoot.palantir.api.table.cell.type.DoubleCell;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CompositionTestEnvironment {
  public static final DoubleCell TEST_FEATURES_VECTOR_1_CELL = new DoubleCell(0, 2d);
  public static final Feature<Double>[] TEST_FEATURES_VECTOR_1 =
      new TestDataSet.TestFeature[] {new TestDataSet.TestFeature(2d)};
  public static final DoubleCell TEST_FEATURES_VECTOR_2_CELL = new DoubleCell(0, 0d);
  public static final Feature<Double>[] TEST_FEATURES_VECTOR_2 =
      new TestDataSet.TestFeature[] {new TestDataSet.TestFeature(0d)};
  public static final Classifier TEST_CLASSIFIER_WITH_1 =
      (Feature<Double>[] features) -> (Prediction<?>) features[0];

  public static final Classifier TEST_CLASSIFIER_WITH_2 =
      (Feature<Double>[] features) -> (Prediction<?>) features[0];
  public static final DoubleCell[] TEST_FEATURES_WITH_ZERO = {new DoubleCell(0, 0d)};
  public static final DoubleCell[] TEST_FEATURES_WITH_TWO = {new DoubleCell(0, 2d)};
  public static final FromSubsetBuilder TEST_BUILDER =
      new FromSubsetBuilder() {
        @Override
        public Classifier buildFromSubset(int... indices) {
          return TEST_CLASSIFIER_WITH_1;
        }

        @Override
        public Classifier build() {
          return TEST_CLASSIFIER_WITH_1;
        }
      };
  public static final AbstractEnsembleCreator TEST_ENSEMBLE_CREATOR =
      new AbstractEnsembleCreator(TEST_BUILDER, 1, 1) {
        private final Random random = new Random();

        @Override
        protected int generateRandomIndex() {
          return random.nextInt(ensembleSize);
        }

        @Override
        protected Ensemble<Classifier> createBy(Stream<Classifier> classifiers) {
          return new SimpleEnsemble(classifiers.collect(Collectors.toList()));
        }
      };
  public static final DataSet DATA_SET = createDataSet();

  private static TestDataSet createDataSet() {
    return new TestDataSet(false, new Double[][] {{1d, 2d}, {2d, 3d}}, new Integer[] {1, 5});
  }

  private CompositionTestEnvironment() {}
}
