package entmoot.composition.model.boosting.training;

import entmoot.api.data.classifier.Classifier;
import entmoot.composition.model.boosting.training.learners.Learner;
import entmoot.composition.model.boosting.training.trainers.Trainer;
import entmoot.composition.model.boosting.training.trainers.adaboost.AdaBoostTrainer;
import entmoot.composition.model.boosting.training.trainers.exceptions.IncorrectIterationsLimit;
import entmoot.composition.model.ensemble.Ensemble;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static entmoot.composition.environment.CompositionTestEnvironment.DATA_SET;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_ENSEMBLE_CREATOR;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_1;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_1_CELL;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_2;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_2_CELL;

class AdaBoostTrainerTest {
  @Test
  void testTrain() {
    final Trainer trainer = new AdaBoostTrainer(DATA_SET, 1);
    final Ensemble<Classifier> ensemble = TEST_ENSEMBLE_CREATOR.createEnsemble();
    final Ensemble<Learner> learners = trainer.train(ensemble);
    for (Learner learner : learners) {
      Assertions.assertEquals(TEST_FEATURES_VECTOR_2_CELL.get(), learner.classify(TEST_FEATURES_VECTOR_2).get());
      Assertions.assertEquals(TEST_FEATURES_VECTOR_1_CELL.get(), learner.classify(TEST_FEATURES_VECTOR_1).get());
      Assertions.assertEquals(1, learner.getLearnerError());
    }
  }

  @Test
  void testIncorrectIterationLimitException() {
    Assertions.assertThrows(
        IncorrectIterationsLimit.class, () -> new AdaBoostTrainer(DATA_SET, -1));
  }
}
