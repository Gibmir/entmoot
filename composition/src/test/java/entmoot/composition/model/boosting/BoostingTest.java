package entmoot.composition.model.boosting;

import entmoot.api.data.classifier.Classifier;
import entmoot.composition.model.boosting.training.learners.Learner;
import entmoot.composition.model.boosting.training.trainers.Trainer;
import entmoot.composition.model.boosting.training.trainers.adaboost.AdaBoostTrainer;
import entmoot.composition.model.boosting.training.trainers.samme.SammeTrainer;
import entmoot.composition.model.ensemble.Ensemble;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static entmoot.composition.environment.CompositionTestEnvironment.DATA_SET;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_ENSEMBLE_CREATOR;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_2;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_2_CELL;

class BoostingTest {

  @Test
  void testBoostingWithEnsembleCreatorAndSammeTrainer() {
    final Trainer sammeTrainer = new SammeTrainer(DATA_SET, 1);
    final Ensemble<Classifier> ensemble = TEST_ENSEMBLE_CREATOR.createEnsemble();
    final Ensemble<Learner> learners = sammeTrainer.train(ensemble);
    final Boosting boosting = Boosting.withEnsemble(learners);
    Assertions.assertEquals(
        TEST_FEATURES_VECTOR_2_CELL.get(), boosting.classify(TEST_FEATURES_VECTOR_2).get());
  }

  @Test
  void testBoostingOnEnsembleWithSammeTrainer() {
    final Trainer sammeTrainer = new SammeTrainer(DATA_SET, 1);
    final Ensemble<Classifier> ensemble = TEST_ENSEMBLE_CREATOR.createEnsemble();
    final Ensemble<Learner> learners = sammeTrainer.train(ensemble);
    final Boosting boosting = Boosting.on(learners);

    Assertions.assertEquals(
        TEST_FEATURES_VECTOR_2_CELL.get(), boosting.classify(TEST_FEATURES_VECTOR_2).get());
  }

  @Test
  void testBoostingOnEnsembleWithAdaBoostTrainer() {
    final Trainer adaBoostTrainer = new AdaBoostTrainer(DATA_SET, 1);
    final Ensemble<Classifier> ensemble = TEST_ENSEMBLE_CREATOR.createEnsemble();
    final Ensemble<Learner> learners = adaBoostTrainer.train(ensemble);
    final Boosting boosting = Boosting.on(learners);

    Assertions.assertEquals(
        TEST_FEATURES_VECTOR_2_CELL.get(), boosting.classify(TEST_FEATURES_VECTOR_2).get());
  }
}
