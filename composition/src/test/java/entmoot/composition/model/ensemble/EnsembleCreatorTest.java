package entmoot.composition.model.ensemble;

import entmoot.api.data.classifier.Classifier;
import entmoot.composition.model.ensemble.simple.SimpleEnsemble;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static entmoot.composition.environment.CompositionTestEnvironment.TEST_CLASSIFIER_WITH_1;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_ENSEMBLE_CREATOR;

public class EnsembleCreatorTest {

  @Test
  void smoke() {
    Ensemble<Classifier> ensemble =
        new SimpleEnsemble(Collections.singletonList(TEST_CLASSIFIER_WITH_1));
    Assertions.assertNotNull(ensemble);
    final List<Classifier> classifiersList =
        ensemble.classifiersStream().collect(Collectors.toList());
    Assertions.assertFalse(classifiersList.isEmpty());
    Assertions.assertEquals(1, classifiersList.size());
    Assertions.assertTrue(classifiersList.contains(TEST_CLASSIFIER_WITH_1));
  }

  @Test
  void testEnsembleCreator() {
    final Ensemble<Classifier> ensemble = TEST_ENSEMBLE_CREATOR.createEnsemble();
    Assertions.assertNotNull(ensemble);
    final List<Classifier> classifiersList =
        ensemble.classifiersStream().collect(Collectors.toList());
    Assertions.assertFalse(classifiersList.isEmpty());
    Assertions.assertEquals(1, classifiersList.size());
    Assertions.assertTrue(classifiersList.contains(TEST_CLASSIFIER_WITH_1));
  }
}
