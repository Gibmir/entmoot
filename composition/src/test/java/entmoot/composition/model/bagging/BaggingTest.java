package entmoot.composition.model.bagging;

import entmoot.api.data.classifier.Classifier;
import entmoot.composition.model.ensemble.Ensemble;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static entmoot.composition.environment.CompositionTestEnvironment.TEST_ENSEMBLE_CREATOR;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_1;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_1_CELL;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_2;
import static entmoot.composition.environment.CompositionTestEnvironment.TEST_FEATURES_VECTOR_2_CELL;

class BaggingTest {

  @Test
  void testCreateBaggingWithEnsembleCreator() {
    final Bagging bagging = Bagging.createWith(TEST_ENSEMBLE_CREATOR);
    Assertions.assertEquals(
        TEST_FEATURES_VECTOR_1_CELL.get(), bagging.classify(TEST_FEATURES_VECTOR_1).get());
    Assertions.assertEquals(
        TEST_FEATURES_VECTOR_2_CELL.get(), bagging.classify(TEST_FEATURES_VECTOR_2).get());
  }

  @Test
  void testBaggingOnEnsemble() {
    final Ensemble<Classifier> ensemble = TEST_ENSEMBLE_CREATOR.createEnsemble();

    final Bagging baggingOnEnsemble = Bagging.on(ensemble);
    Assertions.assertEquals(
        TEST_FEATURES_VECTOR_1_CELL.get(),
        baggingOnEnsemble.classify(TEST_FEATURES_VECTOR_1).get());
    Assertions.assertEquals(
        TEST_FEATURES_VECTOR_2_CELL.get(),
        baggingOnEnsemble.classify(TEST_FEATURES_VECTOR_2).get());
  }
}
