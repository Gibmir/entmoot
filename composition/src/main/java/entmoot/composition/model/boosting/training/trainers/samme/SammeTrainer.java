package entmoot.composition.model.boosting.training.trainers.samme;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.utils.dataset.DataSetUtils;
import entmoot.composition.model.boosting.training.trainers.AbstractTrainer;

import static java.lang.Math.log;

public class SammeTrainer extends AbstractTrainer {
  private final long classCount;

  public SammeTrainer(DataSet dataSet, int iterationsLimit) {
    super(dataSet, iterationsLimit);
    classCount = DataSetUtils.getClassesCountFor(dataSet);
  }

  @Override
  protected double calculateAlpha(double learnerError) {
    return log((1.0 - learnerError) / learnerError) + log((double) classCount - 1);
  }

  @Override
  public String toString() {
    return "SAMMETrainer{" + "classCount=" + classCount + '}';
  }
}
