package entmoot.composition.model.boosting.training.learners;

import entmoot.composition.model.ensemble.AbstractEnsemble;

public class LearnersEnsemble extends AbstractEnsemble<Learner> {
  public LearnersEnsemble(Iterable<Learner> classifiersIterable) {
    super(classifiersIterable);
  }
}
