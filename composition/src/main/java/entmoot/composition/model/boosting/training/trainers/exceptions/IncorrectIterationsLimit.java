package entmoot.composition.model.boosting.training.trainers.exceptions;

public class IncorrectIterationsLimit extends RuntimeException {
  public IncorrectIterationsLimit(String message) {
    super(message);
  }
}
