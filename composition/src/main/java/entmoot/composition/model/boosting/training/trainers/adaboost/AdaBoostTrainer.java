package entmoot.composition.model.boosting.training.trainers.adaboost;

import entmoot.api.data.dataset.DataSet;
import entmoot.composition.model.boosting.training.trainers.AbstractTrainer;

import static java.lang.Math.log;

public class AdaBoostTrainer extends AbstractTrainer {

  public AdaBoostTrainer(DataSet dataSet, int iterationsLimit) {
    super(dataSet, iterationsLimit);
  }

  @Override
  protected double calculateAlpha(double learnerError) {
    return 0.5 * log((1 - learnerError) / learnerError);
  }
}
