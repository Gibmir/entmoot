package entmoot.composition.model.boosting.training.trainers;

import entmoot.api.data.classifier.Classifier;
import entmoot.composition.model.boosting.training.learners.Learner;
import entmoot.composition.model.ensemble.Ensemble;

public interface Trainer {
  /**
   * Trains ensemble to collection of learners.
   *
   * @param ensemble collection of classifiers
   * @return trained learners
   */
  Ensemble<Learner> train(Ensemble<Classifier> ensemble);
}
