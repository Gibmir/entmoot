package entmoot.composition.model.boosting;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.composition.model.boosting.training.learners.Learner;
import entmoot.composition.model.boosting.training.learners.LearnersEnsemble;
import entmoot.composition.model.ensemble.Ensemble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents boosting method of algorithms composition.
 *
 * @author Pavel Usachev
 */
public class Boosting implements Classifier {
  private static final Logger LOGGER = LoggerFactory.getLogger(Boosting.class);
  private final Ensemble<Learner> learnersEnsemble;

  /**
   * Provide boosting classifier from iterable of learners.
   *
   * @param learners iterable of learners
   * @return boosting classifier
   */
  public static Boosting on(Iterable<Learner> learners) {
    return new Boosting(learners);
  }

  /**
   * Provide boosting classifier with ensemble.
   *
   * @param learnersEnsemble ensemble of learners
   * @return boosting classifier
   */
  public static Boosting withEnsemble(Ensemble<Learner> learnersEnsemble) {
    return new Boosting(learnersEnsemble);
  }

  private Boosting(Iterable<Learner> learners) {
    this.learnersEnsemble = new LearnersEnsemble(learners);
  }

  private Boosting(Ensemble<Learner> learnersEnsemble) {
    this.learnersEnsemble = learnersEnsemble;
  }

  @Override
  public Prediction<?> classify(Feature<Double>[] features) {
    return learnersEnsemble
        .classifiersStream()
        .map(learner -> new WeightedVote(learner.classify(features), learner.getAlpha()))
        .peek(weightedVote -> LOGGER.debug("Learner provides weighted vote [{}]", weightedVote))
        .max(this::compareByAlpha)
        .orElseThrow(NullPointerException::new)
        .prediction;
  }

  private int compareByAlpha(WeightedVote weightedVote, WeightedVote otherWeightedVote) {
    return Double.compare(weightedVote.alpha, otherWeightedVote.alpha);
  }

  private static final class WeightedVote {
    private final Prediction<?> prediction;
    private final double alpha;

    private WeightedVote(Prediction<?> prediction, double alpha) {
      this.prediction = prediction;
      this.alpha = alpha;
    }

    @Override
    public String toString() {
      return "WeightedVote{" + "prediction=" + prediction + ", alpha=" + alpha + '}';
    }
  }
}
