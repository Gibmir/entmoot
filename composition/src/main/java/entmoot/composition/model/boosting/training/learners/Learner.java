package entmoot.composition.model.boosting.training.learners;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;

/**
 * It is {@link Classifier classifier} decorator for boosting.
 *
 * @author Pavel Usachev
 */
public class Learner implements Classifier {
  private double alpha;
  private double epsilon;
  private final Classifier classifier;

  public Learner(Classifier classifier) {
    alpha = 0;
    epsilon = 0;
    this.classifier = classifier;
  }

  @Override
  public Prediction<?> classify(Feature<Double>[] features) {
    return classifier.classify(features);
  }

  /**
   * Provides learner error.
   *
   * @return learner error
   */
  public double getLearnerError() {
    return epsilon;
  }

  /**
   * Sets learner error.
   *
   * @param learnerError learner error
   */
  public void setLearnerError(double learnerError) {
    this.epsilon = learnerError;
  }

  /**
   * Provides learner alpha value.
   *
   * @return learner alpha value
   */
  public double getAlpha() {
    return alpha;
  }

  /**
   * Sets learner alpha value.
   *
   * @param alpha alpha value
   */
  public void setAlpha(double alpha) {
    this.alpha = alpha;
  }

  @Override
  public String toString() {
    return "Learner{"
        + "alpha="
        + alpha
        + ", epsilon="
        + epsilon
        + ", classifier="
        + classifier
        + '}';
  }
}
