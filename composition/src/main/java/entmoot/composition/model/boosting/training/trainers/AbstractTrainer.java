package entmoot.composition.model.boosting.training.trainers;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.composition.model.boosting.training.learners.Learner;
import entmoot.composition.model.boosting.training.learners.LearnersEnsemble;
import entmoot.composition.model.boosting.training.trainers.exceptions.IncorrectIterationsLimit;
import entmoot.composition.model.ensemble.Ensemble;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractTrainer implements Trainer {
  private final double[] exampleWeights;
  private final DataSet dataSet;
  private final int iterationsLimit;

  public AbstractTrainer(DataSet dataSet, int iterationsLimit) {
    this.dataSet = dataSet;
    exampleWeights = new double[this.dataSet.rowsCount()];
    Arrays.fill(exampleWeights, 1.0 / exampleWeights.length);
    if (iterationsLimit <= 0) {
      throw new IncorrectIterationsLimit(
          String.format("Iterations limit [%s] is incorrect. ", iterationsLimit));
    }
    this.iterationsLimit = iterationsLimit;
  }

  @Override
  public Ensemble<Learner> train(Ensemble<Classifier> ensemble) {
    List<Learner> learners = createLearnersFrom(ensemble);
    int counter = Math.min(iterationsLimit, learners.size());
    List<Learner> trainedLearners = new ArrayList<>();

    for (int i = 0; i < counter; i++) {
      trainLearners(learners);
      Learner bestLearner = removeBestLearner(learners);
      double alpha = calculateAlphaFor(bestLearner);
      bestLearner.setAlpha(alpha);
      trainedLearners.add(bestLearner);
      updateExampleWeightsWith(bestLearner);
      normalizeWeights();
      if (bestLearner.getLearnerError() > 0.5) {
        break;
      }
    }
    return new LearnersEnsemble(trainedLearners);
  }

  private List<Learner> createLearnersFrom(Ensemble<Classifier> ensemble) {
    return ensemble.classifiersStream().map(Learner::new).collect(Collectors.toList());
  }

  private void trainLearners(List<Learner> learners) {
    learners.forEach(this::updateLearnerError);
  }

  private void updateLearnerError(Learner learner) {
    learner.setLearnerError(calculateLearnerError(learner));
  }

  private double calculateLearnerError(Learner learner) {
    double learnerError = 0;
    for (int i = 0; i < dataSet.rowsCount(); i++) {
      if (isLearnerWrongClassify(learner, i)) {
        learnerError += exampleWeights[i];
      }
    }
    return learnerError;
  }

  private boolean isLearnerWrongClassify(Learner learner, int rowIndex) {
    Feature<Double>[] featuresInRow = dataSet.getFeaturesInRow(rowIndex);
    return !learner.classify(featuresInRow).equals(dataSet.getPredictionInRow(rowIndex));
  }

  private Learner removeBestLearner(List<Learner> learners) {
    return learners.remove(getBestLearnerIndex(learners));
  }

  private int getBestLearnerIndex(List<Learner> learners) {
    int bestLearnerIndex = 0;
    for (int i = 0; i < learners.size(); i++) {
      if (learners.get(i).getLearnerError() < learners.get(bestLearnerIndex).getLearnerError()) {
        bestLearnerIndex = i;
      }
    }
    return bestLearnerIndex;
  }

  private void updateExampleWeightsWith(Learner bestLearner) {
    for (int i = 0; i < dataSet.rowsCount(); i++) {
      if (isLearnerWrongClassify(bestLearner, i)) {
        exampleWeights[i] *= Math.exp(-bestLearner.getAlpha());
      }
    }
  }

  private void normalizeWeights() {
    double weightsSum = Arrays.stream(exampleWeights).sum();
    for (int i = 0; i < exampleWeights.length; i++) {
      exampleWeights[i] = exampleWeights[i] / weightsSum;
    }
  }

  private double calculateAlphaFor(Learner bestLearner) {
    double learnerError = bestLearner.getLearnerError();
    if (learnerError == 0) {
      return calculateAlpha(Double.MIN_NORMAL);
    } else {
      return calculateAlpha(learnerError);
    }
  }

  protected abstract double calculateAlpha(double learnerError);

  @Override
  public String toString() {
    return "AbstractTrainer{"
        + "exampleWeights="
        + Arrays.toString(exampleWeights)
        + ", iterationsLimit="
        + iterationsLimit
        + '}';
  }
}
