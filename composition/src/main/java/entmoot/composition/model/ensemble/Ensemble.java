package entmoot.composition.model.ensemble;

import entmoot.api.data.classifier.Classifier;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Represent an ensemble of classifiers.
 *
 * @author Pavel Usachev
 * @see Classifier
 */
public interface Ensemble<T extends Classifier> extends Iterable<T> {
  /**
   * Provides {@link Stream} of {@link Classifier classifiers}.
   *
   * @return classifiers stream
   */
  default Stream<T> classifiersStream() {
    return StreamSupport.stream(this.spliterator(), false);
  }
}
