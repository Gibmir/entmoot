package entmoot.composition.model.ensemble;

import entmoot.api.data.classifier.Classifier;

import java.util.Iterator;

public abstract class AbstractEnsemble<T extends Classifier> implements Ensemble<T> {
  protected final Iterable<T> classifiersIterable;

  protected AbstractEnsemble(Iterable<T> classifiersIterable) {
    this.classifiersIterable = classifiersIterable;
  }

  @Override
  public Iterator<T> iterator() {
    return classifiersIterable.iterator();
  }
}
