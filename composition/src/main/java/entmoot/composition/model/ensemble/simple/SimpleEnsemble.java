package entmoot.composition.model.ensemble.simple;

import entmoot.api.data.classifier.Classifier;
import entmoot.composition.model.ensemble.AbstractEnsemble;

public class SimpleEnsemble extends AbstractEnsemble<Classifier> {

  public SimpleEnsemble(Iterable<Classifier> classifiersIterable) {
    super(classifiersIterable);
  }
}
