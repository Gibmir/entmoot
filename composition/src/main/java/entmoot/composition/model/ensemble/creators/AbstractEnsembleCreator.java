package entmoot.composition.model.ensemble.creators;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.classifier.builder.FromSubsetBuilder;
import entmoot.composition.model.ensemble.Ensemble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Stream;

public abstract class AbstractEnsembleCreator implements EnsembleCreator {
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractEnsembleCreator.class);
  protected final FromSubsetBuilder fromSubsetBuilder;
  protected final int ensembleSize;
  protected final int subsetSize;

  protected AbstractEnsembleCreator(
      FromSubsetBuilder fromSubsetBuilder, int ensembleSize, int subsetSize) {
    this.fromSubsetBuilder = fromSubsetBuilder;
    this.ensembleSize = ensembleSize;
    this.subsetSize = subsetSize;
  }

  @Override
  public Ensemble<Classifier> createEnsemble() {
    return createBy(generateClassifiers());
  }

  private Stream<Classifier> generateClassifiers() {
    LOGGER.info("Classifiers generation was started. Ensemble size [{}]", ensembleSize);
    return Stream.generate(this::generateRandomIndicesSubset)
        .limit(ensembleSize)
        .map(fromSubsetBuilder::buildFromSubset);
  }

  private int[] generateRandomIndicesSubset() {
    int[] subsetIndices = new int[subsetSize];
    for (int i = 0; i < subsetSize; i++) {
      subsetIndices[i] = generateRandomIndex();
    }
    return subsetIndices;
  }

  protected abstract int generateRandomIndex();

  protected abstract Ensemble<Classifier> createBy(Stream<Classifier> classifiers);

  @Override
  public String toString() {
    return "AbstractEnsembleCreator{"
        + "ensembleSize="
        + ensembleSize
        + ", subsetSize="
        + subsetSize
        + '}';
  }
}
