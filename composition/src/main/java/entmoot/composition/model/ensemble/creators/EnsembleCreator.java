package entmoot.composition.model.ensemble.creators;

import entmoot.api.data.classifier.Classifier;
import entmoot.composition.model.ensemble.Ensemble;

/**
 * Represents an ensemble creator.
 *
 * @author Pavel Usachev
 */
public interface EnsembleCreator {
  /**
   * Creates ensemble.
   *
   * @return ensemble
   */
  Ensemble<Classifier> createEnsemble();
}
