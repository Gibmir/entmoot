package entmoot.composition.model.bagging;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.composition.model.ensemble.Ensemble;
import entmoot.composition.model.ensemble.creators.EnsembleCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.Map;

import static entmoot.api.data.utils.function.FunctionalUtils.initWith;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * Represents bagging method of algorithms composition.
 *
 * @author Pavel Usachev
 */
public class Bagging implements Classifier {
  private static final Logger LOGGER = LoggerFactory.getLogger(Bagging.class);
  private final Ensemble<Classifier> ensemble;

  private Bagging(Ensemble<Classifier> ensemble) {
    this.ensemble = ensemble;
  }

  private Bagging(EnsembleCreator ensembleCreator) {
    this.ensemble = ensembleCreator.createEnsemble();
  }

  @Override
  public Prediction<?> classify(Feature<Double>[] features) {
    return initWith(this::voting).andThen(this::findMostFrequentClass).apply(features);
  }

  private Map<Prediction<?>, Long> voting(Feature<Double>[] classifiableSignsVector) {
    return ensemble
        .classifiersStream()
        .map(classifier -> classifier.classify(classifiableSignsVector))
        .collect(groupingBy(exampleClass -> exampleClass, counting()));
  }

  private Prediction<?> findMostFrequentClass(Map<Prediction<?>, Long> votingMap) {
    LOGGER.debug("Voting result is [{}]", votingMap);
    return votingMap.entrySet().stream()
        .max(Comparator.comparingLong(Map.Entry::getValue))
        .orElseThrow(NullPointerException::new)
        .getKey();
  }

  /**
   * Initiate bagging on provided ensemble.
   *
   * @param ensemble collection of classifiers
   * @return Bagging instance
   */
  public static Bagging on(Ensemble<Classifier> ensemble) {
    return new Bagging(ensemble);
  }

  /**
   * Initiate bagging on ensemble that constructs by provided ensemble creator.
   *
   * @param ensembleCreator creator of the ensemble
   * @return Bagging instance
   */
  public static Bagging createWith(EnsembleCreator ensembleCreator) {
    return new Bagging(ensembleCreator);
  }

  @Override
  public String toString() {
    return "Bagging{" + "ensemble=" + ensemble + '}';
  }
}
