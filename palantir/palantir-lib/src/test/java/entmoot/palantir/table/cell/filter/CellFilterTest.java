package entmoot.palantir.table.cell.filter;

import entmoot.palantir.api.table.cell.filter.CellFilter;
import org.junit.jupiter.api.Test;

import static entmoot.palantir.environment.PalantirModuleTestEnvironment.DEFAULT_FILTER;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.TEST_DOUBLE_CELL;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.TEST_INT_CELL;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.TEST_NEGATIVE_DOUBLE_CELL;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.TEST_NEGATIVE_INT_CELL;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.TEST_POSITIVE_DOUBLE_CELL;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.TEST_POSITIVE_INT_CELL;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.TEST_STRING_CELL;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CellFilterTest {

  @Test
  void testIntFilter() {
    assertTrue(TEST_INT_CELL.filterWith(CellFilter.forInt(data -> true)));
    assertFalse(TEST_INT_CELL.filterWith(CellFilter.forInt(data -> false)));
    assertTrue(TEST_INT_CELL.filterWith(DEFAULT_FILTER));
  }

  @Test
  void testDoubleFilter() {
    assertTrue(TEST_DOUBLE_CELL.filterWith(CellFilter.forDouble(data -> true)));
    assertFalse(TEST_DOUBLE_CELL.filterWith(CellFilter.forDouble(data -> false)));
    assertTrue(TEST_DOUBLE_CELL.filterWith(DEFAULT_FILTER));
  }

  @Test
  void testStringFilter() {
    assertTrue(TEST_STRING_CELL.filterWith(CellFilter.forString(data -> true)));
    assertFalse(TEST_STRING_CELL.filterWith(CellFilter.forString(data -> false)));
    assertTrue(TEST_STRING_CELL.filterWith(DEFAULT_FILTER));
  }

  @Test
  void testPositiveNumbers() {
    assertTrue(TEST_POSITIVE_DOUBLE_CELL.filterWith(CellFilter.positiveNumbers()));
    assertTrue(TEST_POSITIVE_INT_CELL.filterWith(CellFilter.positiveNumbers()));
  }

  @Test
  void testNegativeNumbers() {
    assertFalse(TEST_NEGATIVE_DOUBLE_CELL.filterWith(CellFilter.positiveNumbers()));
    assertFalse(TEST_NEGATIVE_INT_CELL.filterWith(CellFilter.positiveNumbers()));
  }
}
