package entmoot.palantir.table;

import entmoot.palantir.api.Palantir;
import entmoot.palantir.api.table.cell.filter.CellFilter;
import entmoot.palantir.environment.PalantirModuleTestEnvironment;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static entmoot.palantir.environment.PalantirModuleTestEnvironment.FIRST_COLUMN_NAME;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.SECOND_COLUMN_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PalantirFilterTest {
  private static Palantir palantir;

  @BeforeAll
  static void beforeAll() {
    palantir = PalantirModuleTestEnvironment.createStringTestPalantir();
  }

  @Test
  void testFiltrateWithCorrectStringFilter() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.equals("female"));
    assertEquals(
        2,
        palantir
            .getColumn(FIRST_COLUMN_NAME)
            .orElseThrow(IllegalArgumentException::new)
            .filtrateWith(femaleFilter)
            .rowsCount());
  }

  @Test
  void testFiltrateWithCorrectStringFilterByCorrectName() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.equals("female"));
    assertEquals(2, palantir.filtrateWith(femaleFilter, FIRST_COLUMN_NAME).rowsCount());
    assertEquals(
        palantir.columnsCount(),
        palantir.filtrateWith(femaleFilter, FIRST_COLUMN_NAME).columnsCount());
  }

  @Test
  void testFiltrateWithCorrectStringFilterByIncorrectName() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.equals("female"));
    assertThrows(
        IllegalArgumentException.class,
        () -> palantir.filtrateWith(femaleFilter, "INCORRECT").rowsCount());
  }

  @Test
  void testFiltrateWithIncorrectStringFilterByCorrectName() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.equals("INCORRECT"));
    assertEquals(0, palantir.filtrateWith(femaleFilter, FIRST_COLUMN_NAME).rowsCount());
  }

  @Test
  void testFiltrateWithIncorrectCompositeStringFilterByCorrectNames() {
    CellFilter femaleFilter =
        CellFilter.forString(data -> data.equals("female") /*filter only for sex column*/);
    assertEquals(
        0, palantir.filtrateWith(femaleFilter, FIRST_COLUMN_NAME, SECOND_COLUMN_NAME).rowsCount());
  }

  @Test
  void testFiltrateWithCorrectStringFilterByCorrectNames() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.length() >= 2);
    assertEquals(
        3, palantir.filtrateWith(femaleFilter, FIRST_COLUMN_NAME, SECOND_COLUMN_NAME).rowsCount());
    assertEquals(
        palantir.columnsCount(),
        palantir.filtrateWith(femaleFilter, FIRST_COLUMN_NAME, SECOND_COLUMN_NAME).columnsCount());
  }

  @Test
  void testFiltrateWithCorrectStringFilterByCorrectIndex() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.equals("female"));
    assertEquals(2, palantir.filtrateWith(femaleFilter, 0).rowsCount());
    assertEquals(palantir.columnsCount(), palantir.filtrateWith(femaleFilter, 0).columnsCount());
  }

  @Test
  void testFiltrateWithCorrectStringFilterByCorrectIndices() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.length() >= 2);
    assertEquals(3, palantir.filtrateWith(femaleFilter, 0, 1).rowsCount());
    assertEquals(palantir.columnsCount(), palantir.filtrateWith(femaleFilter, 0, 1).columnsCount());
  }

  @Test
  void testFiltrateWithCorrectStringFilterByIncorrectIndex() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.equals("female"));
    assertEquals(0, palantir.filtrateWith(femaleFilter, 1).rowsCount());
  }

  @Test
  void testFiltrateWithIncorrectCompositeStringFilterByCorrectIndices() {
    CellFilter femaleFilter =
        CellFilter.forString(data -> data.equals("female") /*filter only for sex column*/);
    assertEquals(0, palantir.filtrateWith(femaleFilter, 0, 1).rowsCount());
  }

  @Test
  void testFiltrateWithCorrectStringFilterByIndexOutOfBound() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.equals("female"));
    assertThrows(
        ArrayIndexOutOfBoundsException.class,
        () -> palantir.filtrateWith(femaleFilter, palantir.columnsCount() + 1).rowsCount());
  }

  @Test
  void testFiltrateWithIncorrectStringFilterByCorrectIndex() {
    CellFilter femaleFilter = CellFilter.forString(data -> data.equals("INCORRECT"));
    assertEquals(0, palantir.filtrateWith(femaleFilter, 0).rowsCount());
  }

  @Test
  void testFiltrateWithIncorrectFilter() {
    CellFilter cellFilter = CellFilter.forString(data -> data.equals("INCORRECT"));
    final Palantir filtered = palantir.filtrateWith(cellFilter);
    assertEquals(0, filtered.rowsCount());
  }

  @Test
  void testEmptyFilter() {
    int rowCountBeforeFiltration = palantir.rowsCount();
    Palantir filteredWithNothing = palantir.filtrateWith(new CellFilter() {});
    int rowsCountAfterFiltration = filteredWithNothing.rowsCount();
    assertEquals(rowCountBeforeFiltration, rowsCountAfterFiltration);
  }

  @Test
  void testOnlyStrings() {
    Palantir onlyStrings = palantir.filtrateWith(CellFilter.onlyStrings());
    assertEquals(3, onlyStrings.rowsCount());
  }

  @Test
  void testOnlyInts() {
    Palantir onlyInts = palantir.filtrateWith(CellFilter.onlyInts());
    assertEquals(0, onlyInts.rowsCount());
  }

  @Test
  void testOnlyDoubles() {
    Palantir onlyDoubles = palantir.filtrateWith(CellFilter.onlyDoubles());
    assertEquals(0, onlyDoubles.rowsCount());
  }

  @Test
  void testEmptyStrings() {
    Palantir notEmptyString = palantir.filtrateWith(CellFilter.emptyStrings());
    assertEquals(3, notEmptyString.rowsCount());
  }
}
