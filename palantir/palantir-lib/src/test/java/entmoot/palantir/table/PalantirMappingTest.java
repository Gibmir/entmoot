package entmoot.palantir.table;

import entmoot.palantir.api.Palantir;
import entmoot.palantir.api.cache.CellCache;
import entmoot.palantir.api.table.cell.filter.CellFilter;
import entmoot.palantir.api.table.cell.mapper.CellMapper;
import entmoot.palantir.api.table.cell.type.DoubleCell;
import entmoot.palantir.api.table.cell.type.IntCell;
import entmoot.palantir.api.table.cell.type.StringCell;
import entmoot.palantir.environment.PalantirModuleTestEnvironment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static entmoot.palantir.environment.PalantirModuleTestEnvironment.FIRST_COLUMN_NAME;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.MAPPING_VALUE;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.SECOND_COLUMN_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PalantirMappingTest {
  private Palantir palantir;
  private CellCache<Integer> cellCache;

  @BeforeEach
  void setUp() {
    PalantirModuleTestEnvironment.PalantirWithCache palantirWithCache =
        PalantirModuleTestEnvironment.createStringPalantirWithCache();
    palantir = palantirWithCache.getPalantir();
    cellCache = palantirWithCache.getCellCache();
  }

  @Test
  void testMapWithCorrectMappers() {
    CellMapper toStringWithDouble =
        CellMapper.forString(stringCell -> new StringCell(MAPPING_VALUE, stringCell));
    Palantir notMapped = palantir.map(new CellMapper() {});
    assertEquals(
        palantir.rowsCount(), notMapped.filtrateWith(CellFilter.onlyStrings()).rowsCount());
    Palantir mappedToStringWithDouble = palantir.map(toStringWithDouble);
    assertEquals(
        palantir.rowsCount(),
        mappedToStringWithDouble.filtrateWith(CellFilter.onlyStrings()).rowsCount());

    CellMapper toDouble =
        CellMapper.forString(
            stringCell -> new DoubleCell(Double.valueOf(stringCell.get()), stringCell));
    Palantir mappedToDouble = palantir.map(toDouble);
    assertEquals(
        palantir.rowsCount(), mappedToDouble.filtrateWith(CellFilter.onlyDoubles()).rowsCount());

    CellMapper toInt = CellMapper.forDouble(doubleCell -> new IntCell(1, doubleCell));
    Palantir mappedToInt = palantir.map(toInt);
    assertEquals(palantir.rowsCount(), mappedToInt.filtrateWith(CellFilter.onlyInts()).rowsCount());
  }

  @Test
  void testMapWithCorrectMapperAndCorrectIndex() {
    assertNotEquals(MAPPING_VALUE, cellCache.getBy(0).get());
    CellMapper correctCellMapper =
        CellMapper.forString(stringCell -> new StringCell(MAPPING_VALUE, stringCell));
    palantir.map(correctCellMapper, 0);
    assertEquals(MAPPING_VALUE, cellCache.getBy(0).get());
  }

  @Test
  void testMapWithCorrectMapperAndIncorrectIndex() {
    CellMapper correctCellMapper =
        CellMapper.forString(stringCell -> new StringCell(MAPPING_VALUE, stringCell));
    assertThrows(
        ArrayIndexOutOfBoundsException.class,
        () -> palantir.map(correctCellMapper, palantir.columnsCount() + 1));
  }

  @Test
  void testMapWithIncorrectMapperAndCorrectIndices() {
    CellMapper incorrectCellMapper =
      CellMapper.forString(
        stringCell -> {
          throw new MappingException();
        });

    assertThrows(MappingException.class, () -> palantir.map(incorrectCellMapper, 0, 1));
  }

  @Test
  void testMapWithCorrectMapperAndCorrectIndices() {
    assertNotEquals(MAPPING_VALUE, cellCache.getBy(0).get());
    assertNotEquals(MAPPING_VALUE, cellCache.getBy(1).get());
    CellMapper correctCellMapper =
      CellMapper.forString(stringCell -> new StringCell(MAPPING_VALUE, stringCell));
    palantir.map(correctCellMapper, 0, 1);
    assertEquals(MAPPING_VALUE, cellCache.getBy(0).get());
    assertEquals(MAPPING_VALUE, cellCache.getBy(1).get());
  }

  @Test
  void testMapWithCorrectMapperAndCorrectName() {
    assertNotEquals(MAPPING_VALUE, cellCache.getBy(0).get());
    CellMapper correctCellMapper =
        CellMapper.forString(stringCell -> new StringCell(MAPPING_VALUE, stringCell));
    palantir.map(correctCellMapper, FIRST_COLUMN_NAME);
    assertEquals(MAPPING_VALUE, cellCache.getBy(0).get());
  }

  @Test
  void testMapWithCorrectMapperAndIncorrectName() {
    CellMapper correctCellMapper =
        CellMapper.forString(stringCell -> new StringCell(MAPPING_VALUE, stringCell));
    assertThrows(
        IllegalArgumentException.class, () -> palantir.map(correctCellMapper, "INCORRECT"));
  }

  @Test
  void testMapWithCorrectMapperAndCorrectNames() {
    assertNotEquals(MAPPING_VALUE, cellCache.getBy(0).get());
    assertNotEquals(MAPPING_VALUE, cellCache.getBy(1).get());
    CellMapper correctCellMapper =
        CellMapper.forString(stringCell -> new StringCell(MAPPING_VALUE, stringCell));
    palantir.map(correctCellMapper, FIRST_COLUMN_NAME, SECOND_COLUMN_NAME);
    assertEquals(MAPPING_VALUE, cellCache.getBy(0).get());
    assertEquals(MAPPING_VALUE, cellCache.getBy(1).get());
  }

  @Test
  void testMapWithIncorrectMapperAndCorrectNames() {
    CellMapper incorrectCellMapper =
        CellMapper.forString(
            stringCell -> {
              throw new MappingException();
            });

    assertThrows(
        MappingException.class,
        () -> palantir.map(incorrectCellMapper, FIRST_COLUMN_NAME, SECOND_COLUMN_NAME));
  }

  private static class MappingException extends RuntimeException {}
}
