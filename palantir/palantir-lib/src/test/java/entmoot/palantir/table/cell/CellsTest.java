package entmoot.palantir.table.cell;

import entmoot.palantir.api.table.cell.type.DoubleCell;
import entmoot.palantir.api.table.cell.type.IntCell;
import entmoot.palantir.api.table.cell.type.StringCell;
import org.junit.jupiter.api.Test;

import static entmoot.palantir.environment.PalantirModuleTestEnvironment.CELL_ID;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.DOUBLE_TEST_DATA;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.INT_TEST_DATA;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.STRING_TEST_DATA;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CellsTest {

  @Test
  void stringCellTest() {
    StringCell stringCell = new StringCell(CELL_ID, STRING_TEST_DATA);
    assertEquals(STRING_TEST_DATA, stringCell.get());
  }

  @Test
  void intCellTest() {
    IntCell intCell = new IntCell(CELL_ID, INT_TEST_DATA);
    assertEquals(INT_TEST_DATA, intCell.get());
  }

  @Test
  void doubleCellTest() {
    DoubleCell doubleCell = new DoubleCell(CELL_ID, DOUBLE_TEST_DATA);
    assertEquals(DOUBLE_TEST_DATA, doubleCell.get());
  }
}
