package entmoot.palantir.table;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.palantir.api.Palantir;
import entmoot.palantir.environment.PalantirModuleTestEnvironment;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static entmoot.palantir.environment.PalantirModuleTestEnvironment.FIRST_COLUMN_NAME;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.SECOND_COLUMN_NAME;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.createDoubleTestPalantir;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PalantirTest {

  private static Palantir palantir;

  @BeforeAll
  static void beforeAll() {
    palantir = PalantirModuleTestEnvironment.createStringTestPalantir();
  }

  @Test
  void testRangeFrom() {
    final Palantir selected = palantir.rangeFrom(1);
    assertEquals(palantir.rowsCount() - 1, selected.rowsCount());
  }

  @Test
  void testRangeFromIncorrectIndexException() {
    assertThrows(
        IllegalArgumentException.class, () -> palantir.rangeFrom(palantir.rowsCount() + 1));
  }

  @Test
  void testRangeTill() {
    final Palantir selected = palantir.rangeTill(2);
    assertEquals(2, selected.rowsCount());
  }

  @Test
  void testRangeTillIncorrectIndexException() {
    assertThrows(
        IllegalArgumentException.class, () -> palantir.rangeTill(palantir.rowsCount() + 1));
  }

  @Test
  void testRange() {
    final int from = 1;
    final int to = 2;
    final Palantir selected = palantir.range(from, to);
    assertEquals(to - from, selected.rowsCount());
  }

  @Test
  void testRangeWithIncorrectFromIndex() {
    assertThrows(IllegalArgumentException.class, () -> palantir.range(palantir.rowsCount() + 1, 0));
  }

  @Test
  void testRangeWithIncorrectToIndex() {
    assertThrows(IllegalArgumentException.class, () -> palantir.range(0, palantir.rowsCount() + 1));
  }

  @Test
  void testGetColumnByName() {
    String columnName = FIRST_COLUMN_NAME;
    final Optional<Palantir> selected = palantir.getColumn(columnName);
    assertTrue(selected.isPresent());
    Palantir slice = selected.get();
    assertEquals(columnName, slice.columnNames()[0]);
  }

  @Test
  void testGetColumnByIndex() {
    assertFalse(palantir.getColumn(-1).isPresent());
    assertTrue(palantir.getColumn(0).isPresent());
    assertTrue(palantir.getColumn(1).isPresent());
  }

  @Test
  void testDropColumnWithIndex() {
    assertEquals(palantir.columnsCount(), palantir.dropColumn(-1).columnsCount());
    assertNotEquals(palantir.columnsCount(), palantir.dropColumn(0).columnsCount());
  }

  @Test
  void testDropColumnWithName() {
    assertEquals(palantir.columnsCount(), palantir.dropColumn("INCORRECT").columnsCount());
    assertNotEquals(palantir.columnsCount(), palantir.dropColumn(FIRST_COLUMN_NAME).columnsCount());
  }

  @Test
  void testGetColumnByIncorrectColumnName() {
    assertFalse(palantir.getColumn("INCORRECT").isPresent());
  }

  @Test
  void testToDataSet() {
    Palantir palantir = PalantirModuleTestEnvironment.createDoubleTestPalantir();
    DataSet dataSet = assertDoesNotThrow(palantir::toDataSet);
    Feature<Double>[] featuresInRow = dataSet.getFeaturesInRow(0);
    assertEquals(1, featuresInRow.length);
    assertEquals(0d, featuresInRow[0].get());
  }

  @Test
  void testColumnNames() {
    assertArrayEquals(PalantirModuleTestEnvironment.COLUMN_NAMES, palantir.columnNames());
  }

  @Test
  void testOnlyCorrectColumnIndex() {
    Palantir onlyFirstColumn = palantir.only(0);
    assertEquals(1, onlyFirstColumn.columnsCount());
    assertNotEquals(palantir.columnsCount(), onlyFirstColumn.columnsCount());
    assertEquals(3, onlyFirstColumn.rowsCount());
    assertEquals(palantir.rowsCount(), onlyFirstColumn.rowsCount());
  }

  @Test
  void testOnlyIncorrectColumnIndex() {
    assertThrows(IllegalArgumentException.class, () -> palantir.only(-1));
    assertThrows(IllegalArgumentException.class, () -> palantir.only(palantir.columnsCount() + 1));
    int[] nullArray = null;
    assertThrows(IllegalArgumentException.class, () -> palantir.only(nullArray));
  }

  @Test
  void testOnlyIncorrectColumnName() {
    assertThrows(IllegalArgumentException.class, () -> palantir.only("INCORRECT"));
    assertThrows(IllegalArgumentException.class, () -> palantir.only(new String[] {null}));
    String[] nullArray = null;
    assertThrows(IllegalArgumentException.class, () -> palantir.only(nullArray));
  }

  @Test
  void testOnlyCorrectColumnName() {
    Palantir sex = palantir.only(FIRST_COLUMN_NAME);
    assertEquals(1, sex.columnsCount());
    assertNotEquals(palantir.columnsCount(), sex.columnsCount());
    assertEquals(3, sex.rowsCount());
    assertEquals(palantir.rowsCount(), sex.rowsCount());
  }

  @Test
  void testSwapWithCorrectIndices() {
    Palantir doublePalantir = createDoubleTestPalantir();
    DataSet original = doublePalantir.toDataSet();
    Feature<Double>[] originalFeaturesInRow = original.getFeaturesInRow(0);
    DataSet swapped = doublePalantir.swap(0, 1).toDataSet();
    Feature<Double>[] swappedFeaturesInRow = swapped.getFeaturesInRow(0);
    assertNotEquals(originalFeaturesInRow[0], swappedFeaturesInRow[0]);
  }

  @Test
  void testSwapWithSameIndex() {
    Palantir swapped = palantir.swap(1, 1);
    assertEquals(palantir, swapped);
    assertThrows(IllegalArgumentException.class, () -> palantir.swap(-1, -1));
  }

  @Test
  void testSwapWithIncorrectIndices() {
    assertThrows(
        IllegalArgumentException.class,
        () -> palantir.swap(palantir.columnsCount() + 1, palantir.columnsCount() + 2));
  }

  @Test
  void testSwapWithCorrectNames() {
    Palantir doublePalantir = createDoubleTestPalantir();
    DataSet original = doublePalantir.toDataSet();
    Feature<Double>[] originalFeaturesInRow = original.getFeaturesInRow(0);
    DataSet swapped = doublePalantir.swap(FIRST_COLUMN_NAME, SECOND_COLUMN_NAME).toDataSet();
    Feature<Double>[] swappedFeaturesInRow = swapped.getFeaturesInRow(0);
    assertNotEquals(originalFeaturesInRow[0], swappedFeaturesInRow[0]);
  }

  @Test
  void testSwapWithSameName() {
    Palantir swapped = palantir.swap(FIRST_COLUMN_NAME, FIRST_COLUMN_NAME);
    assertEquals(palantir, swapped);
    assertThrows(IllegalArgumentException.class, () -> palantir.swap("sadasd", "Asdsad"));
  }

  @Test
  void testSwapWithIncorrectNames() {
    assertThrows(
        IllegalArgumentException.class, () -> palantir.swap("INCORRECT" + 1, "INCORRECT" + 2));
  }
}
