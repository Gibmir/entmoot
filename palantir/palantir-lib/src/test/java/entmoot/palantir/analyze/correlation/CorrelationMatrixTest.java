package entmoot.palantir.analyze.correlation;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.test.DataSets;
import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.palantir.lib.analyze.correlation.CorrelationMatrix;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class CorrelationMatrixTest {
  private static final double DELTA = 1e-5;

  @Test
  void correlationMatrixForDotsDataSetTest() {
    DataSet dataSet = TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build();
    final CorrelationMatrix build = CorrelationMatrix.buildFor(dataSet);
    final double[][] dotsCorrelationMatrix = build.calculateCorrelationMatrix();

    assertArrayEquals(new double[]{-0.0}, dotsCorrelationMatrix[0], DELTA);
  }

  @Test
  void correlationMatrixForIrisDataSetTest() {
    DataSet dataSet = TestDataSet.builder().setTestDataSet(DataSets.IRIS).setCategorical(true).build();
    final CorrelationMatrix build = CorrelationMatrix.buildFor(dataSet);

    final double[][] irisCorrelationMatrix = build.calculateCorrelationMatrix();

    assertArrayEquals(new double[]{-0.109369, 0.871754, 0.817953}, irisCorrelationMatrix[0], DELTA);
    assertArrayEquals(new double[]{-0.420516, -0.356544}, irisCorrelationMatrix[1], DELTA);
    assertArrayEquals(new double[]{0.962757}, irisCorrelationMatrix[2], DELTA);
  }
}
