package entmoot.palantir.analyze.statistic;

import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.api.data.dataset.test.DataSets;
import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.palantir.lib.analyze.statistic.DataSetStatisticProvider;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DataSetStatisticProviderTest {
  private static final TestDataSet IRIS_DATA_SET =
      TestDataSet.builder().setTestDataSet(DataSets.IRIS).setCategorical(true).build();
  private static final double DELTA = 1e-5;
  private DataSetStatisticProvider dataSetStatisticProvider =
      new DataSetStatisticProvider(IRIS_DATA_SET);

  @Test
  void classDistributionTest() {
    final Map<Prediction<?>, List<Feature<Double>[]>> classesDistribution =
        dataSetStatisticProvider.getClassesDistribution();
    for (Map.Entry<Prediction<?>, List<Feature<Double>[]>> classDistribution : classesDistribution.entrySet()) {
      assertEquals(50, classDistribution.getValue().size());
    }

    final Set<Prediction<?>> classes = classesDistribution.keySet();
    assertEquals(3, classes.size());
    assertTrue(classes.contains(new TestDataSet.TestPrediction(0)));
    assertTrue(classes.contains(new TestDataSet.TestPrediction(1)));
    assertTrue(classes.contains(new TestDataSet.TestPrediction(2)));

    final TestDataSet.TestFeature[] features =
        classesDistribution.values().stream()
            .flatMap(Collection::stream)
            .flatMap(Arrays::stream)
            .toArray(TestDataSet.TestFeature[]::new);
    assertEquals(600, features.length);
  }

  @Test
  void averageValuesTest() {
    final double[] averageFeatureValues = dataSetStatisticProvider.getAverageFeatureValues();

    assertArrayEquals(
        new double[] {5.843333, 3.054000, 3.758666, 1.198666}, averageFeatureValues, DELTA);
  }

  @Test
  void averageValueTest() {
    assertEquals(5.843333, dataSetStatisticProvider.getAverageFeatureValueBy(0), DELTA);
    assertEquals(3.054000, dataSetStatisticProvider.getAverageFeatureValueBy(1), DELTA);
    assertEquals(3.758666, dataSetStatisticProvider.getAverageFeatureValueBy(2), DELTA);
    assertEquals(1.198666, dataSetStatisticProvider.getAverageFeatureValueBy(3), DELTA);
  }

  @Test
  void mediansVector() {
    final double[] mediansVector = dataSetStatisticProvider.getMediansVector();

    assertArrayEquals(new double[] {5.8, 3.0, 4.4, 1.3}, mediansVector);
  }

  @Test
  void mediansVectorByFeatureIndexTest() {
    assertEquals(5.8, dataSetStatisticProvider.getMedianBy(0), DELTA);
    assertEquals(3.0, dataSetStatisticProvider.getMedianBy(1), DELTA);
    assertEquals(4.4, dataSetStatisticProvider.getMedianBy(2), DELTA);
    assertEquals(1.3, dataSetStatisticProvider.getMedianBy(3), DELTA);
  }

  @Test
  void approximatedFeaturesTest() {
    final double[][] approximated = dataSetStatisticProvider.getApproximated();
    assertEquals(4, approximated.length);
    for (double[] approximatedVector : approximated) {
      assertEquals(150, approximatedVector.length);
      for (double approximatedFeature : approximatedVector) {
        assertTrue(approximatedFeature >= 0 && approximatedFeature <= 1);
      }
    }
  }

  @Test
  void approximatedVectorByFeatureIndexTest() {
    assertEquals(150, dataSetStatisticProvider.getApproximatedFeatureVectorBy(0).length);
    assertEquals(150, dataSetStatisticProvider.getApproximatedFeatureVectorBy(1).length);
    assertEquals(150, dataSetStatisticProvider.getApproximatedFeatureVectorBy(2).length);
    assertEquals(150, dataSetStatisticProvider.getApproximatedFeatureVectorBy(3).length);
  }
}
