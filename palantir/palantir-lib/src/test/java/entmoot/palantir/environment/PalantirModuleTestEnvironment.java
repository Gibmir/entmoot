package entmoot.palantir.environment;

import entmoot.palantir.api.Palantir;
import entmoot.palantir.api.cache.CellCache;
import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.factory.CellFactory;
import entmoot.palantir.api.table.cell.filter.CellFilter;
import entmoot.palantir.api.table.cell.type.DoubleCell;
import entmoot.palantir.api.table.cell.type.IntCell;
import entmoot.palantir.api.table.cell.type.StringCell;
import entmoot.palantir.lib.table.PalantirTable;
import entmoot.palantir.lib.table.cell.factory.CellFactoryImpl;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PalantirModuleTestEnvironment {
  public static final String MAPPING_VALUE = "0.0";
  public static final String FIRST_COLUMN_NAME = "Sex";
  public static final String SECOND_COLUMN_NAME = "Age";
  public static final Path CORRECT_PATH = Paths.get("src", "test", "resources", "titanic.csv");
  public static final String CORRECT_DELIMITER = ";";
  public static final int CELL_ID = 1;
  public static final String STRING_TEST_DATA = "test-data";
  public static final int INT_TEST_DATA = 1;
  public static final double DOUBLE_TEST_DATA = 1.0;
  public static final StringCell TEST_STRING_CELL = new StringCell(CELL_ID, STRING_TEST_DATA);
  public static final IntCell TEST_POSITIVE_INT_CELL = new IntCell(CELL_ID, 2);
  public static final IntCell TEST_NEGATIVE_INT_CELL = new IntCell(CELL_ID, -2);
  public static final DoubleCell TEST_POSITIVE_DOUBLE_CELL = new DoubleCell(CELL_ID, 2.0);
  public static final DoubleCell TEST_NEGATIVE_DOUBLE_CELL = new DoubleCell(CELL_ID, -2.0);
  public static final IntCell TEST_INT_CELL = new IntCell(CELL_ID, INT_TEST_DATA);
  public static final DoubleCell TEST_DOUBLE_CELL = new DoubleCell(CELL_ID, DOUBLE_TEST_DATA);
  public static final CellFilter DEFAULT_FILTER = new CellFilter() {};
  public static final CellFactory CELL_FACTORY = new CellFactoryImpl();
  public static final String[] COLUMN_NAMES = {FIRST_COLUMN_NAME, SECOND_COLUMN_NAME};
  private static final int[][] TABLE = {{0, 1}, {2, 3}, {4, 5}};

  private PalantirModuleTestEnvironment() {}

  public static Palantir createStringTestPalantir() {
    return new PalantirTable(TestCache.stringTestCache(), COLUMN_NAMES, TABLE);
  }

  public static PalantirWithCache createStringPalantirWithCache() {
    TestCache cellsCache = TestCache.stringTestCache();
    Palantir palantirTable = new PalantirTable(cellsCache, COLUMN_NAMES, TABLE);
    return new PalantirWithCache(cellsCache, palantirTable);
  }

  public static Palantir createDoubleTestPalantir() {
    return new PalantirTable(TestCache.doubleTestCache(), COLUMN_NAMES, TABLE);
  }

  public static class TestCache implements CellCache<Integer> {
    private final Map<Integer, Cell<?>> map;

    public TestCache(Map<Integer, Cell<?>> map) {
      this.map = map;
    }

    public static TestCache stringTestCache() {
      Map<Integer, Cell<?>> testMap = new HashMap<>();
      testMap.put(0, new StringCell(0, "male"));
      testMap.put(1, new StringCell(1, "25"));
      testMap.put(2, new StringCell(2, "female"));
      testMap.put(3, new StringCell(3, "30"));
      testMap.put(4, new StringCell(4, "female"));
      testMap.put(5, new StringCell(5, "20"));
      return new TestCache(testMap);
    }

    public static TestCache doubleTestCache() {
      Map<Integer, Cell<?>> testMap = new HashMap<>();
      testMap.put(0, new DoubleCell(0, 0d));
      testMap.put(1, new DoubleCell(1, 25d));
      testMap.put(2, new DoubleCell(2, 1d));
      testMap.put(3, new DoubleCell(3, 15d));
      testMap.put(4, new DoubleCell(4, 1d));
      testMap.put(5, new DoubleCell(5, 10d));
      return new TestCache(testMap);
    }

    @Override
    public void put(Integer key, Cell<?> cell) {
      map.put(key, cell);
    }

    @Override
    public Cell<?> getBy(Integer key) {
      return map.get(key);
    }

    @Override
    public Iterator<Cell<?>> cells() {
      return map.values().iterator();
    }
  }

  public static class PalantirWithCache {
    private final CellCache<Integer> cellCache;
    private final Palantir palantir;

    public PalantirWithCache(CellCache<Integer> cellCache, Palantir palantir) {
      this.cellCache = cellCache;
      this.palantir = palantir;
    }

    public CellCache<Integer> getCellCache() {
      return cellCache;
    }

    public Palantir getPalantir() {
      return palantir;
    }
  }
}
