package entmoot.palantir.reader;

import entmoot.palantir.api.Palantir;
import entmoot.palantir.environment.PalantirModuleTestEnvironment;
import entmoot.palantir.lib.reader.FeanorFileReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;

import static entmoot.palantir.environment.PalantirModuleTestEnvironment.CELL_FACTORY;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.CORRECT_DELIMITER;
import static entmoot.palantir.environment.PalantirModuleTestEnvironment.CORRECT_PATH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FeanorTest {

  private FeanorFileReader.FeanorFileReaderBuilder builder;

  @BeforeEach
  void setUp() {
    builder =
        FeanorFileReader.builder()
            .cellCache(new PalantirModuleTestEnvironment.TestCache(new HashMap<>()))
            .path(CORRECT_PATH)
            .cellFactory(CELL_FACTORY)
            .delimiter(CORRECT_DELIMITER);
  }

  @Test
  void smoke() {
    assertEquals(builder.path(), CORRECT_PATH);
    assertEquals(builder.cellFactory(), CELL_FACTORY);
    assertEquals(builder.delimiter(), CORRECT_DELIMITER);
  }

  @Test
  void testCreate() {
    Palantir palantir = assertDoesNotThrow(() -> builder.build().create());
    assertEquals(891 /*-1 header*/, palantir.rowsCount());
  }

  @Test
  void testIncorrectPath() {
    assertThrows(
        IOException.class, () -> builder.path(Paths.get("incorrect/path")).build().create());
  }

  @Test
  void testCreateWithCache() {
    assertDoesNotThrow(() -> builder.build().create());
  }
}
