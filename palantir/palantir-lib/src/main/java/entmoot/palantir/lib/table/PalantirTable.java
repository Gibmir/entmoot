package entmoot.palantir.lib.table;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.feature.DoubleFeature;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.palantir.api.Palantir;
import entmoot.palantir.api.cache.CellCache;
import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.filter.CellFilter;
import entmoot.palantir.api.table.cell.mapper.CellMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class PalantirTable implements Palantir, DataSet {
  private static final Logger LOGGER = LoggerFactory.getLogger(PalantirTable.class);
  private final CellCache<Integer> cellsCache;
  private final String[] columnNames;
  private final int rowsCount;
  private final int columnsCount;
  private final int[][] table;

  public PalantirTable(CellCache<Integer> cellsCache, String[] columnNames, int[][] table) {
    this.cellsCache = cellsCache;
    this.columnNames = columnNames;
    this.columnsCount = this.columnNames.length;
    this.table = table;
    this.rowsCount = this.table.length;
  }

  @Override
  public Optional<Palantir> getColumn(String columnName) {
    LOGGER.debug("Getting column with name [{}] in [{}]", columnName, this.columnNames);
    int columnIndex = getIndexOfColumnWithName(columnName);
    if (columnIndex != -1) {
      return Optional.of(getColumnWith(columnName, columnIndex));
    }
    return Optional.empty();
  }

  @Override
  public Optional<Palantir> getColumn(int columnIndex) {
    LOGGER.debug("Getting [{}] column. Columns count [{}]", columnIndex, this.columnNames.length);
    if (isValidColumnIndex(columnIndex)) {
      return Optional.of(getColumnWith(columnNames[columnIndex], columnIndex));
    }
    return Optional.empty();
  }

  @Override
  public Palantir only(String... columnNames) {
    if (columnNames == null) {
      throw new IllegalArgumentException("Column names must not be null");
    }
    int[] columnIndices = resolveIndices(columnNames);
    return only(columnIndices);
  }

  @Override
  public Palantir only(int... columnIndices) {
    if (columnIndices == null) {
      throw new IllegalArgumentException("Column names must not be null");
    }
    for (int columnIndex : columnIndices) {
      if (!isValidColumnIndex(columnIndex)) {
        throw new IllegalArgumentException("There is no column with index [" + columnIndex + ']');
      }
    }
    return new PalantirTable(cellsCache, extractNames(columnIndices), extractTable(columnIndices));
  }

  /**
   * Resolves indices for specified column names.
   *
   * @param columnNames names of columns for whom need to find indices
   * @return column indices for specified names
   * @throws IllegalArgumentException if column with name was not present in table
   */
  private int[] resolveIndices(String[] columnNames) {
    int[] columnIndices = new int[columnNames.length];
    for (int i = 0; i < columnNames.length; i++) {
      columnIndices[i] = getColumnIndexBy(columnNames[i]);
    }
    LOGGER.debug("{} was resolved for {}", columnIndices, columnNames);
    return columnIndices;
  }

  private int getColumnIndexBy(String columnName) {
    int columnIndex = getIndexOfColumnWithName(columnName);
    if (columnIndex == -1) {
      String message = String.format("Column with name [%s] is not present", columnName);
      throw new IllegalArgumentException(message);
    }
    return columnIndex;
  }

  private boolean isValidColumnIndex(int columnIndex) {
    return columnIndex < columnsCount && columnIndex >= 0;
  }

  /**
   * Resolves index of column with specified name.
   *
   * @param columnName name of column for which need to find index
   * @return column index
   * @implNote returns -1 if column with such name wasn't present
   */
  private int getIndexOfColumnWithName(String columnName) {
    for (int i = 0; i < columnNames.length; i++) {
      if (columnNames[i].equals(columnName)) {
        return i;
      }
    }
    return -1;
  }

  private Palantir getColumnWith(String columnName, int columnIndex) {
    int[][] newTable = new int[table.length][];
    for (int i = 0; i < table.length; i++) {
      newTable[i] = Arrays.copyOfRange(table[i], columnIndex, columnIndex + 1) /*copy one column*/;
    }
    return new PalantirTable(cellsCache, new String[] {columnName}, newTable);
  }

  /**
   * {@inheritDoc}
   *
   * @implNote this method uses {@link Arrays#copyOfRange(Object[], int, int)}
   * @throws IllegalArgumentException if <b>from</b> > rows count and <b>to</b> > rows count
   */
  @Override
  public Palantir range(int from, int to) {
    int length = table.length;
    LOGGER.debug("Getting rows in range from [{}] to [{}]. Rows count [{}]", from, to, length);
    if (length < from || length < to) {
      throw new IllegalArgumentException();
    }
    return new PalantirTable(cellsCache, columnNames, Arrays.copyOfRange(table, from, to));
  }

  /**
   * {@inheritDoc}
   *
   * @implNote this method uses {@link Arrays#copyOfRange(Object[], int, int)}
   */
  @Override
  public Palantir rangeFrom(int from) {
    int length = table.length;
    LOGGER.debug("Getting rows in range from [{}]. Rows count [{}]", from, length);
    return new PalantirTable(cellsCache, columnNames, Arrays.copyOfRange(table, from, length));
  }

  /**
   * {@inheritDoc}
   *
   * @implNote this method uses {@link Arrays#copyOfRange(Object[], int, int)}
   */
  @Override
  public Palantir rangeTill(int till) {
    int length = table.length;
    LOGGER.debug("Getting rows in range till [{}]. Rows count [{}]", till, length);
    if (length < till) {
      final String message = String.format("Table length [%s] less than [%s]", length, till);
      throw new IllegalArgumentException(message);
    }
    return new PalantirTable(cellsCache, columnNames, Arrays.copyOfRange(table, 0, till));
  }

  @Override
  public Feature<Double>[] getFeaturesInRow(int dataSetRowIndex) {
    Feature<Double>[] features = new DoubleFeature[columnsCount - 1];
    for (int i = 0; i < table[dataSetRowIndex].length - 1; i++) {
      features[i] = (DoubleFeature) cellsCache.getBy(table[dataSetRowIndex][i]);
    }
    return features;
  }

  @Override
  public Prediction<?> getPredictionInRow(int dataSetRowIndex) {
    return cellsCache.getBy(table[dataSetRowIndex][columnsCount - 1]);
  }

  @Override
  public int rowsCount() {
    return rowsCount;
  }

  @Override
  public Feature<Double>[] getFeatureValuesVectorBy(int featureIndex) {
    return getColumn(featureIndex)
        .orElseThrow(IllegalArgumentException::new)
        .toDataSet()
        .getFeatureValuesVectorBy(featureIndex);
  }

  @Override
  public int getFeaturesCount() {
    return columnsCount - 1;
  }

  @Override
  public int columnsCount() {
    return columnsCount;
  }

  @Override
  public Palantir filtrateWith(CellFilter cellFilter) {
    LOGGER.debug("Filtrates data in table. Length before [{}]", table.length);
    int[][] filteredTable = filtrate(cellFilter);
    LOGGER.debug("Data in table was filtered. After [{}]", filteredTable.length);
    return new PalantirTable(cellsCache, columnNames, filteredTable);
  }

  @Override
  public Palantir filtrateWith(CellFilter cellFilter, int... columnIndices) {
    LOGGER.debug("Filtrates data in table. Length before [{}]", table.length);
    int[][] filteredTable = filtrate(cellFilter, columnIndices);
    LOGGER.debug("Data in table was filtered. After [{}]", filteredTable.length);
    return new PalantirTable(cellsCache, this.columnNames, filteredTable);
  }

  @Override
  public Palantir filtrateWith(CellFilter cellFilter, String... columnNames) {
    LOGGER.debug("Filtrates data in table. Length before [{}]", table.length);
    int[] columnIndices = resolveIndices(columnNames);
    int[][] filteredTable = filtrate(cellFilter, columnIndices);
    LOGGER.debug("Data in table was filtered. After [{}]", filteredTable.length);
    return new PalantirTable(cellsCache, this.columnNames, filteredTable);
  }

  private int[][] filtrate(CellFilter cellFilter) {
    List<int[]> filtered = new ArrayList<>();
    for (int[] rowIndices : table) {
      if (filtrateRow(cellFilter, rowIndices)) {
        filtered.add(rowIndices);
      }
    }
    int[][] filteredTable = new int[filtered.size()][];
    return filtered.toArray(filteredTable);
  }

  private int[][] filtrate(CellFilter cellFilter, int[] columnIndices) {
    List<int[]> filtered = new ArrayList<>();
    for (int[] rowIndices : table) {
      if (filtrateRowByIndices(cellFilter, rowIndices, columnIndices)) {
        filtered.add(rowIndices);
      }
    }
    int[][] filteredTable = new int[filtered.size()][];
    return filtered.toArray(filteredTable);
  }

  private boolean filtrateRow(CellFilter cellFilter, int[] row) {
    int counter = 0;
    for (int cellId : row) {
      if (cellsCache.getBy(cellId).filterWith(cellFilter)) {
        counter++;
      }
    }
    return counter == columnsCount;
  }

  private boolean filtrateRowByIndices(CellFilter cellFilter, int[] row, int[] columnIndices) {
    int counter = 0;
    for (int columnIndex : columnIndices) {
      if (cellsCache.getBy(row[columnIndex]).filterWith(cellFilter)) {
        counter++;
      }
    }
    return counter == columnIndices.length;
  }

  @Override
  public Palantir map(CellMapper cellMapper) {
    for (int[] row : table) {
      for (int cellIndex : row) {
        mapCellWithKey(cellIndex, cellMapper);
      }
    }
    return this;
  }

  @Override
  public Palantir map(CellMapper cellMapper, String... columnNames) {
    int[] columnIndices = resolveIndices(columnNames);
    return map(cellMapper, columnIndices);
  }

  @Override
  public Palantir map(CellMapper cellMapper, int... columnIndices) {
    for (int columnIndex : columnIndices) {
      for (int[] row : table) {
        mapCellWithKey(row[columnIndex], cellMapper);
      }
    }
    return this;
  }

  private void mapCellWithKey(int key, CellMapper cellMapper) {
    Cell<?> cell = cellsCache.getBy(key);
    Cell<?> mapped = cell.mapWith(cellMapper);
    if (!mapped.equals(cell)) {
      LOGGER.trace("Cell [{}] was updated. New value [{}]", cell, mapped);
      cellsCache.put(cell.getId(), mapped);
    } else {
      LOGGER.trace("Cell [{}] wasn't updated", cell);
    }
  }

  @Override
  public Palantir dropColumn(int columnIndex) {
    if (isValidColumnIndex(columnIndex)) {
      return new PalantirTable(
          cellsCache, getNamesWithoutColumn(columnIndex), getTableWithoutColumn(columnIndex));
    }
    return this;
  }

  @Override
  public Palantir dropColumn(String columnName) {
    int columnIndex = getIndexOfColumnWithName(columnName);
    if (columnIndex != -1) {
      return new PalantirTable(
          cellsCache, getNamesWithoutColumn(columnIndex), getTableWithoutColumn(columnIndex));
    }
    return this;
  }

  @Override
  public Palantir swap(String columnName, String otherColumnName) {
    return swap(getColumnIndexBy(columnName), getColumnIndexBy(otherColumnName));
  }

  @Override
  public Palantir swap(int columnIndex, int otherColumnIndex) {
    checkColumnIndex(columnIndex);
    checkColumnIndex(otherColumnIndex);
    if (columnIndex == otherColumnIndex) {
      return this;
    }
    int[][] swappedTable = swapTable(columnIndex, otherColumnIndex);
    String[] swappedNames =
        swapNames(
            columnIndex,
            otherColumnIndex,
            Arrays.copyOf(this.columnNames, this.columnNames.length));
    return new PalantirTable(cellsCache, swappedNames, swappedTable);
  }

  private void checkColumnIndex(int columnIndex) {
    if (!isValidColumnIndex(columnIndex)) {
      String message = String.format("Provided index is not valid [%d]", columnIndex);
      throw new IllegalArgumentException(message);
    }
  }

  private int[][] swapTable(int columnIndex, int otherColumnIndex) {
    int[][] swappedTable = new int[table.length][];
    for (int i = 0; i < table.length; i++) {
      int[] row = table[i];
      swappedTable[i] = swapIndices(columnIndex, otherColumnIndex, Arrays.copyOf(row, row.length));
    }
    return swappedTable;
  }

  private static int[] swapIndices(int columnIndex, int otherColumnIndex, int[] row) {
    int valueToSwap = row[columnIndex];
    int otherValueToSwap = row[otherColumnIndex];
    row[columnIndex] = otherValueToSwap;
    row[otherColumnIndex] = valueToSwap;
    return row;
  }

  private static String[] swapNames(int columnIndex, int otherColumnIndex, String[] row) {
    String valueToSwap = row[columnIndex];
    String otherValueToSwap = row[otherColumnIndex];
    row[columnIndex] = otherValueToSwap;
    row[otherColumnIndex] = valueToSwap;
    return row;
  }

  @Override
  public DataSet toDataSet() {
    return this;
  }

  @Override
  public String[] columnNames() {
    return columnNames;
  }

  private String[] getNamesWithoutColumn(int columnIndex) {
    return IntStream.range(0, columnNames.length)
        .filter(index -> index != columnIndex)
        .mapToObj(index -> columnNames[index])
        .toArray(String[]::new);
  }

  private String[] extractNames(int[] columnIndices) {
    String[] columnNames = new String[columnIndices.length];
    for (int i = 0; i < columnIndices.length; i++) {
      columnNames[i] = this.columnNames[columnIndices[i]];
    }
    return columnNames;
  }

  private int[][] extractTable(int[] columnIndices) {
    int[][] extractedTable = new int[table.length][];
    for (int i = 0; i < table.length; i++) {
      int[] row = extractRow(columnIndices, table[i]);
      extractedTable[i] = row;
    }
    return extractedTable;
  }

  private static int[] extractRow(int[] columnIndices, int[] row) {
    int[] extractedRow = new int[columnIndices.length];
    for (int i = 0; i < columnIndices.length; i++) {
      extractedRow[i] = row[columnIndices[i]];
    }
    return extractedRow;
  }

  private int[][] getTableWithoutColumn(int columnIndex) {
    int[][] tableWithDroppedColumn = new int[table.length][];
    for (int i = 0; i < table.length; i++) {
      int[] row = table[i];
      tableWithDroppedColumn[i] =
          IntStream.range(0, row.length)
              .filter(index -> index != columnIndex)
              .map(index -> row[index])
              .toArray();
    }
    return tableWithDroppedColumn;
  }

  @Override
  public String toString() {
    return "PalantirTable{" + "rowsCount=" + rowsCount + ", columnsCount=" + columnsCount + '}';
  }
}
