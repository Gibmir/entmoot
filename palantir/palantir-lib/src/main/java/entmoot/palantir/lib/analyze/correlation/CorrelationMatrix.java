package entmoot.palantir.lib.analyze.correlation;

import entmoot.api.data.dataset.DataSet;

/**
 * Provides correlation matrix for {@link DataSet} features.
 *
 * @author Pavel Usachev
 */
public class CorrelationMatrix {
  private final double[][] features;

  private CorrelationMatrix(double[][] features) {
    this.features = features;
  }

  /**
   * Calculates correlation matrix for pairs of features vectors.
   *
   * @return correlation matrix
   */
  public double[][] calculateCorrelationMatrix() {
    double[][] correlationMatrix = new double[features.length - 1][];
    for (int i = 0; i < features.length - 1; i++) {
      double[] correlationVector = new double[features.length - (i + 1)];
      for (int j = i + 1; j < features.length; j++) {
        correlationVector[j - (i + 1)] = calculateCorrelationCoefficient(features[i], features[j]);
      }
      correlationMatrix[i] = correlationVector;
    }
    return correlationMatrix;
  }

  private double calculateCorrelationCoefficient(double[] vector, double[] otherVector) {
    return new VectorsCorrelation(vector, otherVector).calculateCorrelation();
  }

  /**
   * Builds correlation matrix from data set.
   *
   * @param dataSet a data by which matrix will be built
   * @return correlation matrix object
   */
  public static CorrelationMatrix buildFor(DataSet dataSet) {
    double[][] table = new double[dataSet.getFeaturesCount()][];
    for (int i = 0; i < dataSet.getFeaturesCount(); i++) {
      double[] features = new double[dataSet.rowsCount()];
      for (int j = 0; j < dataSet.rowsCount(); j++) {
        features[j] = dataSet.getFeaturesInRow(j)[i].get();
      }
      table[i] = features;
    }
    return new CorrelationMatrix(table);
  }

  private static class VectorsCorrelation {
    private final double vectorSum;
    private final double otherVectorSum;
    private final double vectorSquaresSum;
    private final double otherVectorSquaresSum;
    private final double vectorsMultiplySum;
    private final int length;

    private VectorsCorrelation(double[] vector, double[] otherVector) {
      this.length = vector.length;
      double sum = 0;
      double otherSum = 0;
      double squaresSum = 0;
      double otherSquaresSum = 0;
      double multiplicationSum = 0;
      for (int i = 0; i < length; i++) {
        sum += vector[i];
        otherSum += otherVector[i];
        multiplicationSum += vector[i] * otherVector[i];
        squaresSum += Math.pow(vector[i], 2);
        otherSquaresSum += Math.pow(otherVector[i], 2);
      }
      this.vectorSum = sum;
      this.otherVectorSum = otherSum;
      this.vectorsMultiplySum = multiplicationSum;
      this.vectorSquaresSum = squaresSum;
      this.otherVectorSquaresSum = otherSquaresSum;
    }

    private double calculateCorrelation() {
      final double numerator = (length * (vectorsMultiplySum)) - (vectorSum * otherVectorSum);
      final double denominator =
          Math.sqrt(
              ((length * vectorSquaresSum) - Math.pow(vectorSum, 2))
                  * ((length * otherVectorSquaresSum) - Math.pow(otherVectorSum, 2)));
      return numerator / denominator;
    }
  }
}
