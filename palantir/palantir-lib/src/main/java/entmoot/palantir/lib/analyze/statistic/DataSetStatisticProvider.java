package entmoot.palantir.lib.analyze.statistic;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * Provides some statistic for specified data set.
 *
 * @author Pavel Usachev
 */
public class DataSetStatisticProvider {
  private final Map<Prediction<?>, List<Feature<Double>[]>> classDistribution;
  private final double[] averageFeatureValues;
  private final double[][] featureExampleVectors;
  private final double[] mediansVector;
  private final double[][] approximated;

  public DataSetStatisticProvider(DataSet dataSet) {
    featureExampleVectors = getFeatureExampleVectors(dataSet);
    classDistribution = calculateClassDistribution(dataSet);
    averageFeatureValues = calculateAverageFeatureValues(featureExampleVectors);
    mediansVector = calculateMedianVector(dataSet);
    approximated = approximateFeatures(dataSet);
  }

  private double[][] approximateFeatures(DataSet dataSet) {
    double[][] result = new double[dataSet.getFeaturesCount()][dataSet.rowsCount()];
    for (int i = 0; i < featureExampleVectors.length; i++) {
      result[i] = approximate(featureExampleVectors[i]);
    }
    return result;
  }

  private double[] calculateMedianVector(DataSet dataSet) {
    double[] result = new double[dataSet.getFeaturesCount()];
    for (int i = 0; i < result.length; i++) {
      final double[] featureExampleVector = featureExampleVectors[i];
      Arrays.sort(featureExampleVector);
      if (featureExampleVector.length % 2 == 0) {
        final double feature = featureExampleVector[featureExampleVector.length / 2];
        final double otherFeature = featureExampleVector[featureExampleVector.length / 2 + 1];
        result[i] = (feature + otherFeature) / 2;
      } else {
        result[i] = featureExampleVector[featureExampleVector.length / 2 + 1];
      }
    }
    return result;
  }

  private double[][] getFeatureExampleVectors(DataSet dataSet) {
    double[][] result = new double[dataSet.getFeaturesCount()][dataSet.rowsCount()];
    for (int columnIndex = 0; columnIndex < dataSet.getFeaturesCount(); columnIndex++) {
      for (int rowIndex = 0; rowIndex < dataSet.rowsCount(); rowIndex++) {
        result[columnIndex][rowIndex] = dataSet.getFeaturesInRow(rowIndex)[columnIndex].get();
      }
    }
    return result;
  }

  private Map<Prediction<?>, List<Feature<Double>[]>> calculateClassDistribution(DataSet dataSet) {
    return IntStream.range(0, dataSet.rowsCount())
        .boxed()
        .collect(
            Collectors.groupingBy(
                dataSet::getPredictionInRow,
                Collectors.mapping(dataSet::getFeaturesInRow, Collectors.toList())));
  }

  private double[] calculateAverageFeatureValues(double[][] featureExamplesVector) {
    double[] result = new double[featureExamplesVector.length];
    for (int featureIndex = 0; featureIndex < result.length; featureIndex++) {
      result[featureIndex] =
          DoubleStream.of(featureExamplesVector[featureIndex]).sum()
              / featureExamplesVector[featureIndex].length;
    }
    return result;
  }

  private double[] approximate(double[] vector) {
    final DoubleSummaryStatistics doubleSummaryStatistics =
        DoubleStream.of(vector).summaryStatistics();
    final double min = doubleSummaryStatistics.getMin();
    final double max = doubleSummaryStatistics.getMax();
    final double distance = max - min;

    double[] result = new double[vector.length];
    for (int i = 0; i < vector.length; i++) {
      result[i] = (vector[i] - min) / distance;
    }
    return result;
  }

  /**
   * Provides classes distribution. Key is the class int representation, value is the features
   * vector.
   *
   * @return classes distribution map
   */
  public Map<Prediction<?>, List<Feature<Double>[]>> getClassesDistribution() {
    return classDistribution;
  }

  /**
   * Provides the average value of the feature vectors.
   *
   * @return average values of the feature vectors
   * @implSpec for each features vector will be calculate average value. For example: if features
   *     vector is [1,2,3,4,5] then average value is (1+2+3+4+5) /5
   */
  public double[] getAverageFeatureValues() {
    return averageFeatureValues;
  }

  /**
   * Provide the average value of the single feature vectors chosen by feature index.
   *
   * @param featureIndex index of feature (column) in data set
   * @return average value
   */
  public double getAverageFeatureValueBy(int featureIndex) {
    return averageFeatureValues[featureIndex];
  }

  /**
   * Provides medians for each feature vector.
   *
   * @return vector of medians
   */
  public double[] getMediansVector() {
    return mediansVector;
  }

  /**
   * Provide the median of the single feature vectors chosen by feature index.
   *
   * @param featureVectorIndex index of feature (column) in data set
   * @return median for specified vector
   */
  public double getMedianBy(int featureVectorIndex) {
    return mediansVector[featureVectorIndex];
  }

  /**
   * Approximates all feature vectors of data set.
   *
   * @return approximated feature vectors
   */
  public double[][] getApproximated() {
    return approximated;
  }

  /**
   * Approximates feature vectors chosen by feature index.
   *
   * @param featureIndex index of feature (column) in data set
   * @return approximated feature vector
   */
  public double[] getApproximatedFeatureVectorBy(int featureIndex) {
    return approximated[featureIndex];
  }
}
