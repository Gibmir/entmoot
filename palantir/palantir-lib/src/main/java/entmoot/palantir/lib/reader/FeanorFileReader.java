package entmoot.palantir.lib.reader;

import entmoot.palantir.api.Palantir;
import entmoot.palantir.api.cache.CellCache;
import entmoot.palantir.api.feanor.Feanor;
import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.factory.CellFactory;
import entmoot.palantir.lib.table.PalantirTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FeanorFileReader implements Feanor {
  private static final Logger LOGGER = LoggerFactory.getLogger(FeanorFileReader.class);
  private final String delimiter;
  private final CellFactory cellFactory;
  private final CellCache<Integer> cellCache;
  private final Path path;

  private FeanorFileReader(
      String delimiter, CellFactory cellFactory, CellCache<Integer> cellCache, Path path) {
    this.delimiter = delimiter;
    this.cellFactory = cellFactory;
    this.cellCache = cellCache;
    this.path = path;
  }

  /**
   * Creates new feanor builder.
   *
   * @return builder
   */
  public static FeanorFileReaderBuilder builder() {
    return new FeanorFileReaderBuilder();
  }

  @Override
  public Palantir create() throws IOException {
    LOGGER.info("Reading data from [{}]", path);
    try (Scanner scanner = new Scanner(path)) {
      String[] columnNames = extractColumnNames(scanner);
      int columnsCount = columnNames.length;
      int rowsCount = 0;
      int id = 0;
      List<int[]> indices = new ArrayList<>();
      while (scanner.hasNextLine()) {
        String[] featuresExample = scanner.nextLine().split(delimiter, columnsCount);
        int[] rowIndices = new int[columnsCount];
        for (int column = 0; column < columnsCount; column++) {
          Cell<?> cell = cellFactory.createFrom(id, featuresExample[column]);
          cellCache.put(id, cell);
          rowIndices[column] = id;
          id++;
        }
        indices.add(rowIndices);
        rowsCount++;
      }
      int[][] table = indices.toArray(new int[rowsCount][]);
      LOGGER.info(
          "Data was successfully read. Rows count [{}], columns count [{}], column names [{}]",
          rowsCount,
          columnsCount,
          columnNames);
      return new PalantirTable(cellCache, columnNames, table);
    }
  }

  private String[] extractColumnNames(Scanner scanner) {
    return scanner.nextLine().split(delimiter);
  }

  public static class FeanorFileReaderBuilder {
    private CellCache<Integer> cellCache;
    private CellFactory cellFactory;
    private String delimiter = /*default*/ ";";

    private Path path;

    /**
     * Sets cell factory to builder.
     *
     * @param cellFactory cell factory
     * @return this builder
     */
    public FeanorFileReaderBuilder cellFactory(CellFactory cellFactory) {
      this.cellFactory = cellFactory;
      return this;
    }

    /**
     * Provides cell factory.
     *
     * @return current cell factory
     */
    public CellFactory cellFactory() {
      return cellFactory;
    }

    /**
     * Sets cache to builder.
     *
     * @param cellCache cell cache
     * @return this builder
     */
    public FeanorFileReaderBuilder cellCache(CellCache<Integer> cellCache) {
      this.cellCache = cellCache;
      return this;
    }

    /**
     * Provides current cells cache.
     *
     * @return current cache
     */
    public CellCache<Integer> cellCache() {
      return cellCache;
    }

    /**
     * Sets delimeter to builder.
     *
     * @param delimiter file line delimiter
     * @return this builder
     */
    public FeanorFileReaderBuilder delimiter(String delimiter) {
      this.delimiter = delimiter;
      return this;
    }

    /**
     * Provides delimiter.
     *
     * @return current delimiter
     */
    public String delimiter() {
      return delimiter;
    }

    /**
     * Sets csv file path.
     *
     * @param path csv file path
     * @return this builder
     */
    public FeanorFileReaderBuilder path(Path path) {
      this.path = path;
      return this;
    }

    /**
     * Provides path to csv file.
     *
     * @return current file path
     */
    public Path path() {
      return path;
    }

    /**
     * Builds a new feanor from specified configuration.
     *
     * @return feanor
     */
    public Feanor build() {
      return new FeanorFileReader(delimiter, cellFactory, cellCache, path);
    }
  }
}
