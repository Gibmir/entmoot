package entmoot.palantir.lib.table.cell.factory;

import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.factory.CellFactory;
import entmoot.palantir.api.table.cell.type.StringCell;

public class CellFactoryImpl implements CellFactory {
  @Override
  public Cell<?> createFrom(Integer id, String data) {
    return new StringCell(id, data);
  }
}
