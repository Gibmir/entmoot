package entmoot.palantir.api;

import entmoot.api.data.dataset.DataSet;
import entmoot.palantir.api.filter.DataFilter;
import entmoot.palantir.api.mapper.DataMapper;

import java.util.Optional;

/**
 * Represents data for analyze.
 *
 * @author Pavel Usachev
 */
public interface Palantir extends DataFilter<Palantir>, DataMapper<Palantir> {

  /**
   * Tries to find column with specified column name.
   *
   * @param columnName name of slice
   * @return optional slice of table
   */
  Optional<Palantir> getColumn(String columnName);

  /**
   * Tries to find column with specified column index.
   *
   * @param columnIndex index of slice
   * @return optional slice of table
   */
  Optional<Palantir> getColumn(int columnIndex);

  /**
   * Provides palantir with specified column names.
   *
   * @param columnNames selected column names
   * @return palantir with specified column names
   * @throws NullPointerException if specified names were null
   */
  Palantir only(String... columnNames);

  /**
   * Provides palantir with specified column indices.
   *
   * @param columnIndices selected column indices
   * @return palantir with specified column indices
   * @throws NullPointerException if specified indices were null
   */
  Palantir only(int... columnIndices);

  /**
   * Provides rows in specified range.
   *
   * @param from range start index
   * @param to range end index
   * @return palantir with rows in specified range
   */
  Palantir range(int from, int to);

  /**
   * Provides rows in range that starts from specified index.
   *
   * @param from range start index
   * @return palantir with rows in specified range
   */
  Palantir rangeFrom(int from);

  /**
   * Provides rows in range that ends with specified index.
   *
   * @param till range end index
   * @return palantir with rows in specified range
   */
  Palantir rangeTill(int till);

  /**
   * Provides rows count in palantir.
   *
   * @return rows count
   */
  int rowsCount();

  /**
   * Provides columns count in palantir.
   *
   * @return columns count
   */
  int columnsCount();

  /**
   * Removes from table column with specified index.
   *
   * @param columnIndex index of column to be removed
   * @return palantir with dropped column
   * @implSpec Do not remove column from cache
   */
  Palantir dropColumn(int columnIndex);

  /**
   * Removes from table column with specified name.
   *
   * @param columnName name of column to be removed
   * @return palantir with dropped column
   * @implSpec Do not remove column from cache
   */
  Palantir dropColumn(String columnName);

  /**
   * Swaps columns with each other.
   *
   * @param columnIndex index to swap
   * @param otherColumnIndex index to swap
   * @return palantir with swapped columns
   */
  Palantir swap(int columnIndex, int otherColumnIndex);

  /**
   * Swaps columns with each other.
   *
   * @param columnName column name to swap
   * @param otherColumnName column name to swap
   * @return palantir with swapped columns
   */
  Palantir swap(String columnName, String otherColumnName);

  /**
   * Provides ability to work with palantir like with data set.
   *
   * @return data set
   * @implSpec Remember! Data set works with <b>numeric data</b>. All data set features <b>must be
   *     typed as {@link entmoot.api.data.dataset.feature.DoubleFeature}</b>. It is currently
   *     assumed that last column is for {@link entmoot.api.data.dataset.prediction.Prediction}
   */
  DataSet toDataSet();

  /**
   * Provides column names.
   *
   * @return column names
   */
  String[] columnNames();
}
