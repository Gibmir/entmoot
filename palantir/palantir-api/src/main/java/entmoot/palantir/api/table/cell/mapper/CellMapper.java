package entmoot.palantir.api.table.cell.mapper;

import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.type.DoubleCell;
import entmoot.palantir.api.table.cell.type.IntCell;
import entmoot.palantir.api.table.cell.type.StringCell;

import java.util.function.Function;

/**
 * Map {@link Cell cell} with specified function.
 *
 * @author Pavel Usachev
 */
public interface CellMapper {

  /**
   * Map cell with string data into specified cell.
   *
   * @param cell cell with string data
   * @return mapped cell
   * @implSpec default implementation always returns origin cell
   */
  default Cell<?> map(StringCell cell) {
    return cell;
  }

  /**
   * Map cell with double data into specified cell.
   *
   * @param cell cell with double data
   * @return mapped cell
   * @implSpec default implementation always returns origin cell
   */
  default Cell<?> map(DoubleCell cell) {
    return cell;
  }

  /**
   * Map cell with int data into specified cell.
   *
   * @param cell cell with int data
   * @return mapped cell
   * @implSpec default implementation always returns origin cell
   */
  default Cell<?> map(IntCell cell) {
    return cell;
  }

  /**
   * Static factory for string cells mapping.
   *
   * @param stringCellFunction mapping rule
   * @return cell mapper
   */
  static <R extends Cell<?>> CellMapper forString(Function<StringCell, R> stringCellFunction) {
    return new CellMapper() {
      @Override
      public R map(StringCell cell) {
        return stringCellFunction.apply(cell);
      }
    };
  }

  /**
   * Static factory for integer cells mapping.
   *
   * @param intCellFunction mapping rule
   * @return cell mapper
   */
  static <R extends Cell<?>> CellMapper forInt(Function<IntCell, R> intCellFunction) {
    return new CellMapper() {
      @Override
      public R map(IntCell cell) {
        return intCellFunction.apply(cell);
      }
    };
  }

  /**
   * Static factory for double cells mapping.
   *
   * @param doubleCellFunction mapping rule
   * @return cell mapper
   */
  static <R extends Cell<?>> CellMapper forDouble(Function<DoubleCell, R> doubleCellFunction) {
    return new CellMapper() {
      @Override
      public R map(DoubleCell cell) {
        return doubleCellFunction.apply(cell);
      }
    };
  }
}
