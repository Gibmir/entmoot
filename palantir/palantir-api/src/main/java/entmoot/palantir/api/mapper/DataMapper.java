package entmoot.palantir.api.mapper;

import entmoot.palantir.api.Palantir;
import entmoot.palantir.api.table.cell.mapper.CellMapper;

public interface DataMapper<T extends DataMapper<T>> {
  /**
   * Map all cells with specified mapper.
   *
   * @param cellMapper mapping rule
   * @return mapped data
   * @implSpec highly recommended for use in conjunction with {@link Palantir#getColumn(String)}
   */
  T map(CellMapper cellMapper);

  /**
   * Map all cells with specified mapper.
   *
   * @param cellMapper mapping rule
   * @param columnIndices column indices to map
   * @return mapped data
   */
  T map(CellMapper cellMapper, int... columnIndices);

  /**
   * Map all cells with specified mapper.
   *
   * @param cellMapper mapping rule
   * @param columnNames column names to map
   * @return mapped data
   */
  T map(CellMapper cellMapper, String... columnNames);
}
