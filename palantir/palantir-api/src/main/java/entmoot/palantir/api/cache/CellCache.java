package entmoot.palantir.api.cache;

import entmoot.palantir.api.table.cell.Cell;

import java.util.Iterator;

/**
 * Represents cache for table cells.
 *
 * @author Pavel Usachev
 * @param <K> key for cell resolving
 */
public interface CellCache<K> {
  /**
   * Put cell into cache with specified key.
   *
   * @param key cache key
   * @param cell cell with data
   */
  void put(K key, Cell<?> cell);

  /**
   * Returns cell with data by specified key.
   *
   * @param key cache key
   * @return cell with data
   */
  Cell<?> getBy(K key);

  /**
   * Provides iterator over cells.
   *
   * @return cells iterator
   */
  Iterator<Cell<?>> cells();
}
