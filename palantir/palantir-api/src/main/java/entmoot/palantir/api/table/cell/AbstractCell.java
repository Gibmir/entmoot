package entmoot.palantir.api.table.cell;

import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;

public abstract class AbstractCell<T> implements Cell<T>, Prediction<T>, Feature<T> {
  private final Integer id;

  protected AbstractCell(Integer id) {
    this.id = id;
  }

  protected AbstractCell(Cell<?> origin) {
    this.id = origin.getId();
  }

  @Override
  public Integer getId() {
    return id;
  }
}
