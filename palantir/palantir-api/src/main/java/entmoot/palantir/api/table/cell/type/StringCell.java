package entmoot.palantir.api.table.cell.type;

import entmoot.palantir.api.table.cell.AbstractCell;
import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.filter.CellFilter;
import entmoot.palantir.api.table.cell.mapper.CellMapper;

public class StringCell extends AbstractCell<String> {
  private final String data;

  public StringCell(Integer id, String data) {
    super(id);
    this.data = data;
  }

  public StringCell(String data, Cell<?> origin) {
    super(origin);
    this.data = data;
  }

  @Override
  public String get() {
    return data;
  }

  @Override
  public boolean filterWith(CellFilter cellFilter) {
    return cellFilter.filter(data);
  }

  @Override
  public Cell<?> mapWith(CellMapper cellMapper) {
    return cellMapper.map(this);
  }
}
