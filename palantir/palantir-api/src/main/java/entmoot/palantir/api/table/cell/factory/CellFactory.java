package entmoot.palantir.api.table.cell.factory;

import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.type.DoubleCell;
import entmoot.palantir.api.table.cell.type.IntCell;
import entmoot.palantir.api.table.cell.type.StringCell;

/**
 * Analyze data and creates {@link Cell}.
 *
 * @author Pavel Usachev
 * @see DoubleCell
 * @see IntCell
 * @see StringCell
 * @implSpec You need to provide pattern-matching like function to retrieve concrete cell type from
 *     specified string
 */
public interface CellFactory {

  /**
   * Creates {@link Cell cell} from provided data.
   *
   * @param id cell id
   * @param data string data representation
   * @return cell with data
   */
  Cell<?> createFrom(Integer id, String data);
}
