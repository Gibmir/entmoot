package entmoot.palantir.api.filter;

import entmoot.palantir.api.table.cell.filter.CellFilter;

public interface DataFilter<T extends DataFilter<T>> {
  /**
   * Filtrates data with specified {@link CellFilter predicate}.
   *
   * @param cellFilter cell's data predicate
   * @return filtered data
   */
  T filtrateWith(CellFilter cellFilter);

  /**
   * Filtrates data with specified {@link CellFilter predicate}. Filter will be applied only to
   * columns with specified column indices
   *
   * @param cellFilter cell's data predicate
   * @param columnIndices column indices to filtrate
   * @return filtered data
   */
  T filtrateWith(CellFilter cellFilter, int... columnIndices);

  /**
   * Filtrates data with specified {@link CellFilter predicate}. Filter will be applied only to
   * columns with specified column names
   *
   * @param cellFilter cell's data predicate
   * @param columnNames column names to filtrate
   * @return filtered data
   */
  T filtrateWith(CellFilter cellFilter, String... columnNames);
}
