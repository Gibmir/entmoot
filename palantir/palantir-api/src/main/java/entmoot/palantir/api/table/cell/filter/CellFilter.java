package entmoot.palantir.api.table.cell.filter;

import java.util.function.Predicate;

/**
 * Filtrates cells with specified rules.
 *
 * @author Pavel Usachev
 * @implSpec implement the interface only if you need a special filters for several attributes. In
 *     other situations - use specialized factories.
 */
public interface CellFilter {

  CellFilter ONLY_DOUBLES_CELL_FILTER =
      new CellFilter() {
        @Override
        public boolean filter(Integer data) {
          return false;
        }

        @Override
        public boolean filter(String data) {
          return false;
        }
      };
  CellFilter ONLY_INTS_CELL_FILTER =
      new CellFilter() {
        @Override
        public boolean filter(Double data) {
          return false;
        }

        @Override
        public boolean filter(String data) {
          return false;
        }
      };
  CellFilter ONLY_STRINGS_CELL_FILTER =
      new CellFilter() {
        @Override
        public boolean filter(Integer data) {
          return false;
        }

        @Override
        public boolean filter(Double data) {
          return false;
        }
      };
  CellFilter EMPTY_STRINGS =
      new CellFilter() {
        @Override
        public boolean filter(String data) {
          return !data.isEmpty();
        }
      };
  CellFilter POSITIVE_NUMBERS =
      new CellFilter() {
        @Override
        public boolean filter(Integer data) {
          return data > 0;
        }

        @Override
        public boolean filter(Double data) {
          return data > 0;
        }

        @Override
        public boolean filter(String data) {
          return false;
        }
      };

  /**
   * Filtrates provided int data.
   *
   * @param data int data
   * @return true if provided data complies with specified rule
   * @implSpec default implementation always returns true
   */
  default boolean filter(Integer data) {
    return true;
  }

  /**
   * Filtrates provided double data.
   *
   * @param data double data
   * @return true if provided data complies with specified rule
   * @implSpec default implementation always returns true
   */
  default boolean filter(Double data) {
    return true;
  }

  /**
   * Filtrates provided string data.
   *
   * @param data string data
   * @return true if provided data complies with specified rule
   * @implSpec default implementation always returns true
   */
  default boolean filter(String data) {
    return true;
  }

  /**
   * Static factory for string attributes filtration.
   *
   * @param stringPredicate filtration rule
   * @return cell filter
   */
  static CellFilter forString(final Predicate<String> stringPredicate) {
    return new CellFilter() {
      @Override
      public boolean filter(String data) {
        return stringPredicate.test(data);
      }
    };
  }

  /**
   * Static factory for integer attributes filtration.
   *
   * @param intPredicate filtration rule
   * @return cell filter
   */
  static CellFilter forInt(final Predicate<Integer> intPredicate) {
    return new CellFilter() {
      @Override
      public boolean filter(Integer data) {
        return intPredicate.test(data);
      }
    };
  }

  /**
   * Static factory for double attributes filtration.
   *
   * @param doublePredicate filtration rule
   * @return cell filter
   */
  static CellFilter forDouble(final Predicate<Double> doublePredicate) {
    return new CellFilter() {
      @Override
      public boolean filter(Double data) {
        return doublePredicate.test(data);
      }
    };
  }

  /**
   * Provides cell filter that leaves only strings and filtrates other types.
   *
   * @return cell filter
   */
  static CellFilter onlyStrings() {
    return ONLY_STRINGS_CELL_FILTER;
  }

  /**
   * Provides cell filter that leaves only ints and filtrates other types.
   *
   * @return cell filter
   */
  static CellFilter onlyInts() {
    return ONLY_INTS_CELL_FILTER;
  }

  /**
   * Provides cell filter that leaves only doubles and filtrates other types.
   *
   * @return cell filter
   */
  static CellFilter onlyDoubles() {
    return ONLY_DOUBLES_CELL_FILTER;
  }

  /**
   * Provides cell filter that leaves only not empty strings.
   *
   * @return cell filter
   */
  static CellFilter emptyStrings() {
    return EMPTY_STRINGS;
  }

  /**
   * Provides cell filter that leaves only positive numbers.
   *
   * @return cell filter
   */
  static CellFilter positiveNumbers() {
    return POSITIVE_NUMBERS;
  }
}
