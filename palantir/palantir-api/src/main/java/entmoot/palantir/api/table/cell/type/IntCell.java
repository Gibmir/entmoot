package entmoot.palantir.api.table.cell.type;

import entmoot.palantir.api.table.cell.AbstractCell;
import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.filter.CellFilter;
import entmoot.palantir.api.table.cell.mapper.CellMapper;

public class IntCell extends AbstractCell<Integer> {
  private final Integer data;

  public IntCell(Integer id, Integer data) {
    super(id);
    this.data = data;
  }

  public IntCell(Integer data, Cell<?> origin) {
    super(origin);
    this.data = data;
  }

  @Override
  public Integer get() {
    return data;
  }

  @Override
  public boolean filterWith(CellFilter cellFilter) {
    return cellFilter.filter(data);
  }

  @Override
  public Cell<?> mapWith(CellMapper cellMapper) {
    return cellMapper.map(this);
  }

  @Override
  public String toString() {
    return "IntCell{" + "data=" + data + '}';
  }
}
