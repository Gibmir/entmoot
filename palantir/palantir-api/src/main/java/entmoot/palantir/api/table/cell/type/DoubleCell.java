package entmoot.palantir.api.table.cell.type;

import entmoot.api.data.dataset.feature.DoubleFeature;
import entmoot.palantir.api.table.cell.AbstractCell;
import entmoot.palantir.api.table.cell.Cell;
import entmoot.palantir.api.table.cell.filter.CellFilter;
import entmoot.palantir.api.table.cell.mapper.CellMapper;

public class DoubleCell extends AbstractCell<Double> implements DoubleFeature {
  private final Double data;

  public DoubleCell(Integer id, Double data) {
    super(id);
    this.data = data;
  }

  public DoubleCell(Double data, Cell<?> origin) {
    super(origin);
    this.data = data;
  }

  @Override
  public Double get() {
    return data;
  }

  @Override
  public boolean filterWith(CellFilter cellFilter) {
    return cellFilter.filter(data);
  }

  @Override
  public Cell<?> mapWith(CellMapper cellMapper) {
    return cellMapper.map(this);
  }

  @Override
  public String toString() {
    return "DoubleCell{" + "data=" + data + '}';
  }
}
