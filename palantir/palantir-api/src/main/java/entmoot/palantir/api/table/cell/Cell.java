package entmoot.palantir.api.table.cell;

import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.palantir.api.table.cell.filter.CellFilter;
import entmoot.palantir.api.table.cell.mapper.CellMapper;

/**
 * Represents table cell with data.
 *
 * @author Pavel Usachev
 * @implSpec Implementations should be immutable. Row, column and id were represented for debug
 *     purposes.
 */
public interface Cell<T> extends Feature<T>, Prediction<T> {

  /**
   * Provides cell's id in table.
   *
   * @return id
   */
  Integer getId();

  /**
   * Tests that cell's data complies with specified filter.
   *
   * @param cellFilter data filter
   * @return true if cell data matches specified filter rule
   */
  boolean filterWith(CellFilter cellFilter);

  /**
   * Return new cell provided by {@link CellMapper} logic.
   *
   * @param cellMapper cell mapper
   * @return mapped cell
   * @implSpec Should provide new cell
   */
  Cell<?> mapWith(CellMapper cellMapper);
}
