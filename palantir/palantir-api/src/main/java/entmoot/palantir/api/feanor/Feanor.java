package entmoot.palantir.api.feanor;

import entmoot.palantir.api.Palantir;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Represents {@link Palantir palantir} creator.
 *
 * @author Pavel Usachev
 */
public interface Feanor {

  /**
   * Creates palantir for data analyze.
   *
   * @return palantir for data analyze
   * @throws IOException when can't read a file
   * @implSpec reads file at the specified {@link Path path} and use specified cells cache as data
   *     storage
   */
  Palantir create() throws IOException;
}
