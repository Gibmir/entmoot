package benchmarking;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.test.DataSets;
import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.api.data.quality.QualityControl;
import entmoot.tree.api.builder.AbstractDecisionTreeBuilder;
import entmoot.tree.api.builder.pruning.PrePruningDecisionTreeBuilder;
import entmoot.tree.api.evaluator.NodeQualityFunctions;
import entmoot.tree.model.BinaryDecisionTree;
import org.openjdk.jmh.Main;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.runner.RunnerException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BinaryDecisionTreeBenchmarks {
  private static final TestDataSet NUMERIC_DOTS =
      TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();
  private static final TestDataSet CAT_DOTS =
      TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build();
  private static final AbstractDecisionTreeBuilder NUMERIC_TREE_BUILDER =
      BinaryDecisionTree.builder()
          .trainDataSet(NUMERIC_DOTS)
          .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX);
  private static final AbstractDecisionTreeBuilder CAT_TREE_BUILDER =
      BinaryDecisionTree.builder()
          .trainDataSet(CAT_DOTS)
          .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX);
  private static final int[] INDICES = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
  private static final PrePruningDecisionTreeBuilder NUMERIC_PRE_PRUNING_TREE_BUILDER;

  static {
    final QualityControl qualityControl = new QualityControl(NUMERIC_DOTS);
    NUMERIC_PRE_PRUNING_TREE_BUILDER =
        NUMERIC_TREE_BUILDER
            .withPrePruning()
            .qualityEvaluator(qualityControl::calculateAccuracy)
            .threshold(0.2);
  }

  public static void main(String[] args) throws IOException, RunnerException {
    Main.main(args);
  }

  @Benchmark
  @BenchmarkMode({Mode.AverageTime, Mode.Throughput})
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  @Fork(value = 1, warmups = 1)
  public Classifier numericDecisionTreeBuildingBenchmark() {
    return NUMERIC_TREE_BUILDER.build();
  }

  @Benchmark
  @BenchmarkMode({Mode.AverageTime, Mode.Throughput})
  @OutputTimeUnit(TimeUnit.MILLISECONDS)
  @Fork(value = 1, warmups = 1)
  public Classifier numericPrePruningTreeBuildingBenchmark() {
    return NUMERIC_PRE_PRUNING_TREE_BUILDER.build();
  }

  @Benchmark
  @BenchmarkMode({Mode.AverageTime, Mode.Throughput})
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  @Fork(value = 1, warmups = 1)
  public Classifier numericDecisionTreeBuildingFromSubsetBenchmark() {
    return NUMERIC_TREE_BUILDER.buildFromSubset(INDICES);
  }

  @Benchmark
  @BenchmarkMode({Mode.AverageTime, Mode.Throughput})
  @OutputTimeUnit(TimeUnit.MILLISECONDS)
  @Fork(value = 1, warmups = 1)
  public Classifier numericPrePruningTreeBuildingFromSubsetBenchmark() {
    return NUMERIC_PRE_PRUNING_TREE_BUILDER.buildFromSubset(INDICES);
  }

  @Benchmark
  @BenchmarkMode({Mode.AverageTime, Mode.Throughput})
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  @Fork(value = 1, warmups = 1)
  public Classifier catDecisionTreeBuildingBenchmark() {
    return CAT_TREE_BUILDER.build();
  }

  @Benchmark
  @BenchmarkMode({Mode.AverageTime, Mode.Throughput})
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  @Fork(value = 1, warmups = 1)
  public Classifier catDecisionTreeBuildingFromSubsetBenchmark() {
    return CAT_TREE_BUILDER.buildFromSubset(INDICES);
  }
}
