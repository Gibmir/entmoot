package entmoot.tree.model;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.api.data.dataset.test.DataSets;
import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.api.data.quality.QualityControl;
import entmoot.api.data.quality.QualityEvaluator;
import entmoot.tree.api.builder.pruning.AbstractPrePruningBuilder;
import entmoot.tree.api.builder.pruning.PrePruningDecisionTreeBuilder;
import entmoot.tree.api.evaluator.NodeQualityEvaluator;
import entmoot.tree.api.evaluator.NodeQualityFunctions;
import entmoot.tree.model.builder.BinaryDecisionTreeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.DoubleStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BinaryDecisionTreeTest {
  private static final double DELTA = 1e-2;
  public static final TestDataSet.TestFeature[] FEATURES_1_1 =
      DoubleStream.of(1d, 1d)
          .mapToObj(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);
  public static final TestDataSet.TestFeature[] FEATURES_1_2 =
      DoubleStream.of(1d, 2d)
          .mapToObj(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);
  public static final TestDataSet.TestFeature[] FEATURES_2_2 =
      DoubleStream.of(2d, 2d)
          .mapToObj(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);
  private static Classifier numClassifier;
  private static Classifier fromSubsetClassifier;
  private static Classifier catClassifier;
  public static final Feature<Double>[] FEATURES_NULL_2 =
      Arrays.stream(new Double[] {null, 2d})
          .map(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);
  public static final Feature<Double>[] FEATURES_2_NULL =
      Arrays.stream(new Double[] {null, 2d})
          .map(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);
  public static final Feature<Double>[] FEATURES_NULL_NULL =
      Arrays.stream(new Double[] {null, null})
          .map(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);

  @BeforeAll
  static void setUp() {
    final TestDataSet numericDots =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();
    final TestDataSet categoricalDots =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build();
    catClassifier =
        BinaryDecisionTree.builder()
            .trainDataSet(categoricalDots)
            .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX)
            .build();
    numClassifier =
        BinaryDecisionTree.builder()
            .trainDataSet(numericDots)
            .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX)
            .build();
    fromSubsetClassifier =
        BinaryDecisionTree.builder()
            .trainDataSet(numericDots)
            .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX)
            .buildFromSubset(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
  }

  @Test
  void classifyWithCartNumericFeaturesTest1() {
    assertEquals(1, numClassifier.classify(FEATURES_1_2).get());
  }

  @Test
  void classifyWithCartNumericFeaturesTest2() {
    assertEquals(2, numClassifier.classify(FEATURES_2_2).get());
  }

  @Test
  void classifyWithCartFromSubsetNumericFeaturesTest1() {
    assertEquals(1, fromSubsetClassifier.classify(FEATURES_1_2).get());
  }

  @Test
  void classifyWithCartFromSubsetNumericFeaturesTest2() {
    assertEquals(2, fromSubsetClassifier.classify(FEATURES_2_2).get());
  }

  @Test
  void classifyWithCartFromSubsetCategoricalFeaturesTest1() {
    assertEquals(1, fromSubsetClassifier.classify(FEATURES_1_2).get());
  }

  @Test
  void classifyWithCartCategoricalFeaturesTest2() {
    assertEquals(2, catClassifier.classify(FEATURES_2_2).get());
  }

  @Test
  void testClassifyWithNullValues() {
    assertEquals(2, catClassifier.classify(FEATURES_NULL_2).get());
    assertEquals(2, catClassifier.classify(FEATURES_2_NULL).get());
    assertEquals(3, catClassifier.classify(FEATURES_NULL_NULL).get());

    assertEquals(1, numClassifier.classify(FEATURES_NULL_2).get());
    assertEquals(1, numClassifier.classify(FEATURES_2_NULL).get());
    assertEquals(3, numClassifier.classify(FEATURES_NULL_NULL).get());
  }

  @Test
  void testPrePruneTreeWithOptimalThreshold() {
    final TestDataSet numericDots =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();
    final QualityControl qualityControl = new QualityControl(numericDots);
    final Classifier tree =
        BinaryDecisionTree.builder()
            .trainDataSet(numericDots)
            .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX)
            .withPrePruning()
            .threshold(0.2)
            .qualityEvaluator(qualityControl::calculateAccuracy)
            .build();
    assertEquals(0.83, qualityControl.calculateAccuracy(tree), DELTA);
    assertEquals(1, tree.classify(FEATURES_1_1).get());
    assertEquals(2, tree.classify(FEATURES_2_2).get());
  }

  @Test
  void testPrePruneTreeWithOptimalThresholdAndCategoricalDataSet() {
    final TestDataSet numericDots =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build();
    final QualityControl qualityControl = new QualityControl(numericDots);
    final Classifier tree =
        BinaryDecisionTree.builder()
            .trainDataSet(numericDots)
            .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX)
            .withPrePruning()
            .threshold(0.2)
            .qualityEvaluator(qualityControl::calculateAccuracy)
            .build();
    assertEquals(0.83, qualityControl.calculateAccuracy(tree), DELTA);
    assertEquals(1, tree.classify(FEATURES_1_1).get());
    assertEquals(2, tree.classify(FEATURES_2_2).get());
  }

  @Test
  void testPrePruneFromSubsetTreeWithOptimalThreshold() {
    final TestDataSet numericDots =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();
    final QualityControl qualityControl = new QualityControl(numericDots);
    final Classifier tree =
        BinaryDecisionTree.builder()
            .trainDataSet(numericDots)
            .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX)
            .withPrePruning()
            .threshold(0.2)
            .qualityEvaluator(qualityControl::calculateAccuracy)
            .buildFromSubset(1, 3, 5, 7, 9, 11);
    assertEquals(0.83, qualityControl.calculateAccuracy(tree), DELTA);
    assertEquals(1, tree.classify(FEATURES_1_1).get());
    assertEquals(2, tree.classify(FEATURES_2_2).get());
  }

  @Test
  void testPrePruneFromSubsetTreeWithOptimalThresholdAndCategoricalDataSet() {
    final TestDataSet numericDots =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build();
    final QualityControl qualityControl = new QualityControl(numericDots);
    final Classifier tree =
        BinaryDecisionTree.builder()
            .trainDataSet(numericDots)
            .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX)
            .withPrePruning()
            .threshold(0.2)
            .qualityEvaluator(qualityControl::calculateAccuracy)
            .buildFromSubset(1, 3, 5, 7, 9, 11);
    assertEquals(0.83, qualityControl.calculateAccuracy(tree), DELTA);
    assertEquals(1, tree.classify(FEATURES_1_1).get());
    assertEquals(2, tree.classify(FEATURES_2_2).get());
  }

  @Test
  void builderTest() {
    final BinaryDecisionTreeBuilder builder = BinaryDecisionTree.builder();
    final TestDataSet expectedDataSet =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();
    final FeaturesRecognizable actualDataSet = builder.trainDataSet(expectedDataSet).trainDataSet();

    assertEquals(expectedDataSet, actualDataSet);
    final NodeQualityFunctions expectedNodeQualityEvaluator = NodeQualityFunctions.GINI_INDEX;
    final NodeQualityEvaluator actualNodeQualityEvaluator =
        builder.nodeQualityEvaluator(expectedNodeQualityEvaluator).nodeQualityEvaluator();
    assertEquals(expectedNodeQualityEvaluator, actualNodeQualityEvaluator);
    final PrePruningDecisionTreeBuilder prePruningBuilder = builder.withPrePruning();
    Assertions.assertEquals(
        AbstractPrePruningBuilder.DEFAULT_THRESHOLD, prePruningBuilder.threshold());
    final int expectedThreshold = 1;
    prePruningBuilder.threshold(expectedThreshold);
    assertEquals(expectedThreshold, prePruningBuilder.threshold());
    final QualityControl qualityControl = new QualityControl(expectedDataSet);
    final QualityEvaluator calculateAccuracy = qualityControl::calculateAccuracy;
    prePruningBuilder.qualityEvaluator(calculateAccuracy);
    assertEquals(calculateAccuracy, prePruningBuilder.qualityEvaluator());
  }

  @Test
  void testBuildWithOnlyOneIndex() {
    BinaryDecisionTree.builder()
        .trainDataSet(
            TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build())
        .nodeQualityEvaluator(NodeQualityFunctions.GINI_INDEX)
        .buildFromSubset(0);
  }
}
