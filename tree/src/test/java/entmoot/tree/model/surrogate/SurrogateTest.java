package entmoot.tree.model.surrogate;

import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.tree.api.node.surrogate.CategoricalSurrogate;
import entmoot.tree.api.node.surrogate.NumericalSurrogate;
import entmoot.tree.api.node.surrogate.Surrogate;
import org.junit.jupiter.api.Test;

import java.util.stream.DoubleStream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SurrogateTest {
  public static final TestDataSet.TestFeature[] FEATURES_0 =
      DoubleStream.of(0d)
          .mapToObj(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);
  public static final TestDataSet.TestFeature[] FEATURES_1 =
      DoubleStream.of(1d)
          .mapToObj(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);
  public static final TestDataSet.TestFeature[] FEATURES_MINUS_1 =
      DoubleStream.of(-1)
          .mapToObj(TestDataSet.TestFeature::new)
          .toArray(TestDataSet.TestFeature[]::new);

  @Test
  void testCategoricalSurrogate() {
    Surrogate categoricalSurrogate = new CategoricalSurrogate(0, 0d);
    assertTrue(categoricalSurrogate.checkConditionOf(0d));
    assertFalse(categoricalSurrogate.checkConditionOf(1d));
    assertTrue(categoricalSurrogate.test(FEATURES_0));
    assertFalse(categoricalSurrogate.test(FEATURES_1));
  }

  @Test
  void testNumericalSurrogate() {
    Surrogate numericalSurrogate = new NumericalSurrogate(0, 0d);
    assertTrue(numericalSurrogate.checkConditionOf(0d));
    assertTrue(numericalSurrogate.checkConditionOf(-1d));
    assertFalse(numericalSurrogate.checkConditionOf(1d));
    assertTrue(numericalSurrogate.test(FEATURES_0));
    assertTrue(numericalSurrogate.test(FEATURES_MINUS_1));
    assertFalse(numericalSurrogate.test(FEATURES_1));
  }
}
