package entmoot.tree.quality;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.test.DataSets;
import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.tree.api.evaluator.NodeQualityFunctions;
import entmoot.tree.api.frame.node.AbstractFrameNode;
import entmoot.tree.model.frame.node.BinaryFrame;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GiniIndexTest {
  private static DataSet testData;
  private static AbstractFrameNode testNode;
  private static NodeQualityFunctions nodeQualityCalculator;
  private static final double DELTA = 1e-5;

  @BeforeAll
  static void init() {
    testData = TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();
    final BinaryFrame left = new BinaryFrame();
    left.setDataSetIndices(Stream.of(0, 1, 2, 3).collect(Collectors.toList()));

    final BinaryFrame right = new BinaryFrame();
    right.setDataSetIndices(Stream.of(4, 5, 6, 7, 8, 9, 10, 11).collect(Collectors.toList()));

    final BinaryFrame test = new BinaryFrame();
    test.setDataSetIndices(Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11).collect(Collectors.toList()));
    test.setLeft(left);
    test.setRight(right);
    testNode = test;
    nodeQualityCalculator = NodeQualityFunctions.GINI_INDEX;
  }

  @Test
  void calculateGiniIndexForSplitTest() {
    double expectedGiniIndex = 4.66666;

    double actualGiniIndex = nodeQualityCalculator.evaluate(testNode, testData);

    assertEquals(expectedGiniIndex, actualGiniIndex, DELTA);
  }
}
