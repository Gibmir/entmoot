package entmoot.tree.api.node.surrogate;

public class CategoricalSurrogate extends AbstractSurrogate {

  public CategoricalSurrogate(Integer featuresIndex, Double feature) {
    super(featuresIndex, feature);
  }

  @Override
  public boolean checkConditionOf(Double feature) {
    return feature.equals(this.feature);
  }
}
