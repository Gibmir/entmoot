package entmoot.tree.api.node;

import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.tree.api.frame.node.FrameNode;
import entmoot.tree.api.node.surrogate.Surrogate;

import java.util.Collection;

/**
 * Represents abstract decision tree node.
 *
 * @param <F> frame on which node will be build
 * @implSpec first you need to build mutable frame tree and use it's nodes for creating immutable
 *     nodes
 */
public abstract class AbstractNode<F extends FrameNode> implements Node {
  protected final int indicesCount;
  protected final Collection<Surrogate> surrogates;
  protected final Surrogate surrogate;
  private final boolean isLeaf;
  private final Prediction<?> nodeFrequentClass;

  public AbstractNode(F frame) {
    surrogate = frame.toSurrogate();
    isLeaf = frame.isLeaf();
    nodeFrequentClass = frame.getNodeMostFrequentClass();
    indicesCount = frame.getIndicesCount();
    surrogates = frame.getSurrogates();
  }

  @Override
  public boolean isLeaf() {
    return isLeaf;
  }

  @Override
  public Prediction<?> getNodeMostFrequentClass() {
    return nodeFrequentClass;
  }

  @Override
  public Collection<Surrogate> getSurrogates() {
    return surrogates;
  }

  @Override
  public String toString() {
    return "AbstractNode{"
        + "indicesCount="
        + indicesCount
        + ", surrogates="
        + surrogates
        + ", surrogate="
        + surrogate
        + ", isLeaf="
        + isLeaf
        + ", nodeFrequentClass="
        + nodeFrequentClass
        + '}';
  }
}
