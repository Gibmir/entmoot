package entmoot.tree.api.node.surrogate;

public class NumericalSurrogate extends AbstractSurrogate {

  public NumericalSurrogate(Integer featuresIndex, Double threshold) {
    super(featuresIndex, threshold);
  }

  @Override
  public boolean checkConditionOf(Double feature) {
    return feature <= this.feature;
  }
}
