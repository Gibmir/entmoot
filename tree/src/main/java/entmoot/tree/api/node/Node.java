package entmoot.tree.api.node;

import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.tree.api.node.surrogate.Surrogate;

import java.util.Collection;

/**
 * Represents a decision tree node.
 *
 * @author Pavel Usachev
 */
public interface Node {

  /**
   * Checks if this node is the leaf.
   *
   * @return true if node is leaf else false
   */
  boolean isLeaf();

  /**
   * Produces the most frequent class of data set.
   *
   * @return leaf class
   */
  Prediction<?> getNodeMostFrequentClass();

  /**
   * Provides sorted surrogate nodes to process missing values.
   *
   * @return surrogate nodes
   */
  Collection<Surrogate> getSurrogates();

  /**
   * Provides inner node surrogate.
   *
   * @return surrogate
   */
  Surrogate toSurrogate();
}
