package entmoot.tree.api.node.surrogate;

import entmoot.api.data.dataset.feature.Feature;

public abstract class AbstractSurrogate implements Surrogate {
  protected final int featuresIndex;
  protected final double feature;

  protected AbstractSurrogate(int featuresIndex, double feature) {
    this.featuresIndex = featuresIndex;
    this.feature = feature;
  }

  @Override
  public double getFeature() {
    return feature;
  }

  @Override
  public int getFeaturesIndex() {
    return featuresIndex;
  }

  @Override
  public boolean test(Feature<Double>[] features) {
    return checkConditionOf(features[featuresIndex].get());
  }
}
