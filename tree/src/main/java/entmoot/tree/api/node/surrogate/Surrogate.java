package entmoot.tree.api.node.surrogate;

import entmoot.api.data.dataset.feature.Feature;

import java.util.function.Predicate;

public interface Surrogate extends Predicate<Feature<Double>[]> {
  /**
   * Provides feature value that contains in the node.
   *
   * @return feature value
   */
  double getFeature();

  /**
   * Provides feature index (index of column in data set) that contains in the node.
   *
   * @return feature index
   */
  int getFeaturesIndex();

  /**
   * Check feature with inner surrogate condition.
   *
   * @param feature example feature to be checked
   * @return true if inner surrogate condition was passed
   */
  boolean checkConditionOf(Double feature);
}
