package entmoot.tree.api.frame.node;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.tree.api.node.Node;
import entmoot.tree.api.node.surrogate.Surrogate;

import java.util.Collection;
import java.util.List;

/**
 * Represents frame node that will be used for frame tree building.
 *
 * @author Pavel Usachev
 */
public interface FrameNode extends Node {

  /**
   * Provides node quality.
   *
   * @return node quality
   */
  double getNodeQuality();

  /**
   * Sets a quality to node.
   *
   * @param quality node quality
   */
  void setNodeQuality(double quality);

  /**
   * Sets a data set indices to node.
   *
   * @param dataSetIndices indices of data set
   */
  void setDataSetIndices(List<Integer> dataSetIndices);

  /**
   * Sets an example class to node.
   *
   * @param exampleClass a class of node
   */
  void setClass(Prediction<?> exampleClass);

  /**
   * Provides data set indices.
   *
   * @return data set indices
   */
  List<Integer> getDataSetIndices();

  /**
   * Provides count of indices that stored in node.
   *
   * @return count of indices
   */
  int getIndicesCount();

  /**
   * Sets the leaf node.
   *
   * @param leaf the leaf node
   */
  void setLeaf(boolean leaf);

  /**
   * Checks if this node is a leaf.
   *
   * @param dataSet data set for checking node
   */
  void leafCheck(DataSet dataSet);

  /**
   * Sets surrogate to node.
   *
   * @param surrogate node surrogate
   */
  void setSurrogate(Surrogate surrogate);

  /**
   * Sets surrogate nodes.
   *
   * @param surrogates sorted collection of surrogates
   */
  void setSurrogates(Collection<Surrogate> surrogates);
}
