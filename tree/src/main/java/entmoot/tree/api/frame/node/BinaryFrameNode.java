package entmoot.tree.api.frame.node;

/**
 * Represents binary frame node (with two child node).
 *
 * @author Pavel Usachev
 */
public interface BinaryFrameNode extends FrameNode {

  /**
   * Provides left child of binary node.
   *
   * @return left child
   */
  BinaryFrameNode getLeft();

  /**
   * Provides right child of binary node.
   *
   * @return right child
   */
  BinaryFrameNode getRight();

  /**
   * Set a left child to node.
   *
   * @param left left child
   */
  void setLeft(BinaryFrameNode left);

  /**
   * Set a right child to node.
   *
   * @param right right child
   */
  void setRight(BinaryFrameNode right);
}
