package entmoot.tree.api.frame.node;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.tree.api.node.surrogate.Surrogate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractFrameNode implements FrameNode {
  public static final int LEAF_CLASS_COUNT = 1;
  protected Surrogate surrogate;
  protected boolean isLeaf;
  protected double quality;
  private List<Integer> dataSetIndices;
  private Prediction<?> exampleClass;
  private Collection<Surrogate> surrogates;

  protected AbstractFrameNode() {
    this.dataSetIndices = new ArrayList<>();
  }

  protected AbstractFrameNode(FrameNode frameNode) {
    this.dataSetIndices = frameNode.getDataSetIndices();
    this.isLeaf = frameNode.isLeaf();
    this.surrogate = frameNode.toSurrogate();
    this.quality = frameNode.getNodeQuality();
    this.exampleClass = frameNode.getNodeMostFrequentClass();
    this.surrogates = frameNode.getSurrogates();
  }

  @Override
  public double getNodeQuality() {
    return quality;
  }

  @Override
  public boolean isLeaf() {
    return isLeaf;
  }

  @Override
  public Prediction<?> getNodeMostFrequentClass() {
    return exampleClass;
  }

  @Override
  public void setNodeQuality(double quality) {
    this.quality = quality;
  }

  @Override
  public void setDataSetIndices(List<Integer> dataSetIndices) {
    this.dataSetIndices = dataSetIndices;
  }

  @Override
  public void setClass(Prediction<?> exampleClass) {
    this.exampleClass = exampleClass;
  }

  @Override
  public List<Integer> getDataSetIndices() {
    return dataSetIndices;
  }

  @Override
  public int getIndicesCount() {
    return dataSetIndices.size();
  }

  @Override
  public void setLeaf(boolean leaf) {
    this.isLeaf = leaf;
  }

  @Override
  public void leafCheck(DataSet dataSet) {
    isLeaf =
        dataSetIndices.stream().map(dataSet::getPredictionInRow).distinct().count()
            == LEAF_CLASS_COUNT;
  }

  @Override
  public Collection<Surrogate> getSurrogates() {
    return surrogates;
  }

  @Override
  public void setSurrogates(Collection<Surrogate> surrogates) {
    this.surrogates = surrogates;
  }

  @Override
  public void setSurrogate(Surrogate surrogate) {
    this.surrogate = surrogate;
  }

  @Override
  public Surrogate toSurrogate() {
    return surrogate;
  }

  @Override
  public String toString() {
    return "FrameNode{"
        + "dataSetIndices="
        + dataSetIndices
        + ", quality="
        + quality
        + ", surrogates="
        + surrogates
        + '}';
  }
}
