package entmoot.tree.api.frame;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.tree.api.frame.node.FrameNode;
import entmoot.tree.api.splitter.FrameSplitter;
import entmoot.tree.api.stopper.NodeDivisionBuildStopper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Queue;

import static entmoot.api.data.utils.dataset.DataSetUtils.findFrequentClass;

public abstract class AbstractFrameTree<N extends FrameNode> {
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFrameTree.class);
  protected final FeaturesRecognizable dataSet;
  protected final N root;
  protected final FrameSplitter<N> splitter;
  protected final NodeDivisionBuildStopper<N> nodeDivisionBuildStopper;

  public AbstractFrameTree(
      FeaturesRecognizable dataSet,
      NodeDivisionBuildStopper<N> nodeDivisionBuildStopper,
      FrameSplitter<N> splitter) {
    this.dataSet = dataSet;
    this.nodeDivisionBuildStopper = nodeDivisionBuildStopper;
    this.splitter = splitter;
    root = initRoot();
    buildTree();
  }

  public AbstractFrameTree(
      FeaturesRecognizable dataSet,
      NodeDivisionBuildStopper<N> nodeDivisionBuildStopper,
      FrameSplitter<N> splitter,
      int... indices) {
    this.dataSet = dataSet;
    this.nodeDivisionBuildStopper = nodeDivisionBuildStopper;
    this.splitter = splitter;
    root = initRoot(indices);
    buildTree();
  }

  protected abstract N initRoot();

  protected abstract N initRoot(int[] indices);

  protected void buildTree() {
    Queue<N> nodesQueue = new LinkedList<>();
    nodesQueue.add(root);
    N currentNode;
    while (!nodesQueue.isEmpty()) {
      currentNode = nodesQueue.poll();
      N bestCurrentNodeSplit = splitter.split(dataSet, currentNode);
      LOGGER.debug("Best node split is: [{}]", bestCurrentNodeSplit);
      if (currentNode != bestCurrentNodeSplit) {
        if (nodeDivisionBuildStopper.test(currentNode, bestCurrentNodeSplit)) {
          LOGGER.trace("Leaf was found: [{}]. Stop split", currentNode);
          currentNode.setLeaf(true);
          currentNode.setClass((findFrequentClass(currentNode.getDataSetIndices(), dataSet)));
        } else {
          addChildrenOf(bestCurrentNodeSplit, nodesQueue);
          copy(bestCurrentNodeSplit, currentNode);
        }
      } else {
        LOGGER.trace("The best node is current. Stop build");
        currentNode.setLeaf(true);
        currentNode.setClass((findFrequentClass(currentNode.getDataSetIndices(), dataSet)));
      }
    }
  }

  /**
   * Provides the root node of tree.
   *
   * @return root node
   */
  public N getRoot() {
    return root;
  }

  /**
   * Provides the train data set.
   *
   * @return train data set
   */
  public FeaturesRecognizable getDataSet() {
    return dataSet;
  }

  protected abstract void copy(N source, N target);

  protected abstract void addChildrenOf(N frameNode, Queue<N> nodesQueue);
}
