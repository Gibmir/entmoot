package entmoot.tree.api.builder;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.tree.api.evaluator.NodeQualityEvaluator;
import entmoot.tree.api.evaluator.NodeQualityFunctions;

public abstract class AbstractDecisionTreeBuilder
    implements DecisionTreeBuilder<AbstractDecisionTreeBuilder> {
  protected NodeQualityEvaluator nodeQualityEvaluator = NodeQualityFunctions.GINI_INDEX;
  protected FeaturesRecognizable dataSet;

  @Override
  public FeaturesRecognizable trainDataSet() {
    return dataSet;
  }

  @Override
  public AbstractDecisionTreeBuilder trainDataSet(FeaturesRecognizable trainDataSet) {
    this.dataSet = trainDataSet;
    return this;
  }

  @Override
  public AbstractDecisionTreeBuilder nodeQualityEvaluator(
      NodeQualityEvaluator nodeQualityEvaluator) {
    this.nodeQualityEvaluator = nodeQualityEvaluator;
    return this;
  }

  @Override
  public NodeQualityEvaluator nodeQualityEvaluator() {
    return nodeQualityEvaluator;
  }
}
