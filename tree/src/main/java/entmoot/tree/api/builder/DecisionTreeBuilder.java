package entmoot.tree.api.builder;

import entmoot.api.data.classifier.builder.FromSubsetBuilder;
import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.tree.api.builder.pruning.PrePruningDecisionTreeBuilder;
import entmoot.tree.api.evaluator.NodeQualityEvaluator;
import entmoot.tree.api.evaluator.NodeQualityFunctions;

public interface DecisionTreeBuilder<T extends DecisionTreeBuilder<T>> extends FromSubsetBuilder {
  /**
   * Provides node quality evaluator.
   *
   * @return function that will evaluate node
   * @see NodeQualityFunctions#GINI_INDEX
   */
  NodeQualityEvaluator nodeQualityEvaluator();

  /**
   * Sets node quality evaluator.
   *
   * @param nodeQualityEvaluator function that will evaluate node
   * @return this
   */
  T nodeQualityEvaluator(NodeQualityEvaluator nodeQualityEvaluator);

  /**
   * Provides set train data set.
   *
   * @return this
   */
  FeaturesRecognizable trainDataSet();

  /**
   * Sets data set for tree building.
   *
   * @param trainDataSet data set
   * @return this
   */
  T trainDataSet(FeaturesRecognizable trainDataSet);

  /**
   * Provides special builder for tree pre pruning.
   *
   * @return builder for tree pre pruning
   */
  PrePruningDecisionTreeBuilder withPrePruning();
}
