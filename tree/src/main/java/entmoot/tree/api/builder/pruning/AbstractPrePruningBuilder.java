package entmoot.tree.api.builder.pruning;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.api.data.quality.QualityEvaluator;
import entmoot.tree.api.builder.AbstractDecisionTreeBuilder;
import entmoot.tree.api.evaluator.NodeQualityEvaluator;

public abstract class AbstractPrePruningBuilder implements PrePruningDecisionTreeBuilder {
  public static final double DEFAULT_THRESHOLD = -1;
  protected final AbstractDecisionTreeBuilder abstractDecisionTreeBuilder;
  protected double threshold = DEFAULT_THRESHOLD;
  protected QualityEvaluator qualityEvaluator;

  protected AbstractPrePruningBuilder(AbstractDecisionTreeBuilder abstractDecisionTreeBuilder) {
    this.abstractDecisionTreeBuilder = abstractDecisionTreeBuilder;
  }

  @Override
  public NodeQualityEvaluator nodeQualityEvaluator() {
    return abstractDecisionTreeBuilder.nodeQualityEvaluator();
  }

  @Override
  public PrePruningDecisionTreeBuilder nodeQualityEvaluator(
      NodeQualityEvaluator nodeQualityEvaluator) {
    abstractDecisionTreeBuilder.nodeQualityEvaluator(nodeQualityEvaluator);
    return this;
  }

  @Override
  public FeaturesRecognizable trainDataSet() {
    return abstractDecisionTreeBuilder.trainDataSet();
  }

  @Override
  public PrePruningDecisionTreeBuilder trainDataSet(FeaturesRecognizable trainDataSet) {
    abstractDecisionTreeBuilder.trainDataSet(trainDataSet);
    return this;
  }

  @Override
  public PrePruningDecisionTreeBuilder withPrePruning() {
    return this;
  }

  @Override
  public PrePruningDecisionTreeBuilder threshold(double threshold) {
    this.threshold = threshold;
    return this;
  }

  @Override
  public double threshold() {
    return threshold;
  }

  @Override
  public PrePruningDecisionTreeBuilder qualityEvaluator(QualityEvaluator qualityEvaluator) {
    this.qualityEvaluator = qualityEvaluator;
    return this;
  }

  @Override
  public QualityEvaluator qualityEvaluator() {
    return qualityEvaluator;
  }
}
