package entmoot.tree.api.builder.pruning;

import entmoot.api.data.quality.QualityEvaluator;
import entmoot.tree.api.builder.DecisionTreeBuilder;

/**
 * Represents special builder for configure pre pruning.
 *
 * @author Pavel Usachev
 */
public interface PrePruningDecisionTreeBuilder
    extends DecisionTreeBuilder<PrePruningDecisionTreeBuilder> {
  /**
   * Provides set (or default) threshold.
   *
   * @return quality threshold for build stopper
   */
  double threshold();

  /**
   * Sets threshold.
   *
   * @param threshold quality threshold for build stopper.
   * @return this
   */
  PrePruningDecisionTreeBuilder threshold(double threshold);

  /**
   * Sets quality evaluator for build stopper.
   *
   * @param qualityEvaluatorFunction classifier quality evaluator
   * @return this
   */
  PrePruningDecisionTreeBuilder qualityEvaluator(QualityEvaluator qualityEvaluatorFunction);

  /**
   * Provides quality which will be used in tree build stopper.
   *
   * @return classifier quality evaluator
   */
  QualityEvaluator qualityEvaluator();
}
