package entmoot.tree.api.splitter;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.tree.api.frame.node.FrameNode;

/**
 * Splits frame node.
 *
 * @param <N> frame node
 * @author Pavel Usachev
 */
public interface FrameSplitter<N extends FrameNode> {

  /**
   * Splits frame by each data set's feature and provides split with best {@link
   * entmoot.tree.api.evaluator.NodeQualityEvaluator quality}.
   *
   * @param dataSet train data
   * @param frameNode node that will be split
   * @return best split
   */
  N split(FeaturesRecognizable dataSet, N frameNode);
}
