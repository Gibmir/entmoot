package entmoot.tree.api.splitter;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.api.data.dataset.recognizable.exceptions.UnknownFeatureTypeException;
import entmoot.api.data.utils.pair.Pair;
import entmoot.tree.api.frame.node.FrameNode;
import entmoot.tree.api.node.surrogate.Surrogate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractFrameSplitter<N extends FrameNode> implements FrameSplitter<N> {
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFrameSplitter.class);

  @Override
  public N split(FeaturesRecognizable dataSet, N frameNode) {
    List<N> splits = getAllSplits(dataSet, frameNode);
    return findBestSplit(frameNode, splits);
  }

  /**
   * Build split for each feature in data set.
   *
   * @param dataSet data for training
   * @param frameNode node that will be split
   * @return splits that was made by all features
   */
  private List<N> getAllSplits(FeaturesRecognizable dataSet, N frameNode) {
    List<N> featureSplits = new ArrayList<>();
    for (int splitFeaturesIndex = 0;
        splitFeaturesIndex < dataSet.getFeaturesCount();
        splitFeaturesIndex++) {
      featureSplits.addAll(splitByFeature(splitFeaturesIndex, dataSet, frameNode));
    }
    LOGGER.debug("Feature splits was prepared. Splits count: [{}]", featureSplits.size());
    return featureSplits;
  }

  /**
   * Provide all splits for specified feature.
   *
   * @param featureIndex index of feature in data set(column)
   * @param dataSet data for training
   * @param node node that will be split
   * @return best feature split
   */
  private Collection<N> splitByFeature(int featureIndex, FeaturesRecognizable dataSet, N node) {
    if (dataSet.isCategoricalFeatures(featureIndex)) {
      LOGGER.trace("Feature column [{}] is categorical. Splitting node [{}]", featureIndex, node);
      return categorical(featureIndex, node, dataSet);
    } else if (dataSet.isNumericFeatures(featureIndex)) {
      LOGGER.trace("Feature column [{}] is numeric. Splitting node [{}]", featureIndex, node);
      return numerical(featureIndex, node, dataSet);
    } else {
      throw new UnknownFeatureTypeException("Unknown type of sign taken by " + featureIndex);
    }
  }

  /**
   * Finds best split.
   *
   * @param frameNode origin split node
   * @param splits origin node splits by each feature
   * @return split with best node quality or origin split node (if splits is empty)
   */
  private N findBestSplit(N frameNode, List<N> splits) {
    N bestSplit =
        splits.stream().max(Comparator.comparingDouble(N::getNodeQuality)).orElse(frameNode);
    bestSplit.setSurrogates(getSurrogates(bestSplit, splits));
    return bestSplit;
  }

  /**
   * Calculate surrogate splits to perform classification for vectors with null-values.
   *
   * @param bestSplit split with best node quality
   * @param splits other splits
   * @return surrogates
   */
  private List<Surrogate> getSurrogates(N bestSplit, List<N> splits) {
    splits.remove(bestSplit);
    return getSortedSurrogates(bestSplit, splits);
  }

  private List<Surrogate> getSortedSurrogates(N bestSplit, List<N> splits) {
    return splits.stream()
        .map(alternativeSplit -> getSurrogatePerSimilarity(alternativeSplit, bestSplit))
        .sorted(Comparator.comparingDouble(Pair::getValue))
        .map(Pair::getKey)
        .collect(Collectors.toList());
  }

  private Pair<Surrogate, Double> getSurrogatePerSimilarity(N alternativeSplit, N bestSplit) {
    return new Pair<>(
        alternativeSplit.toSurrogate(),
        calculateSimilarityBetweenNodes(alternativeSplit, bestSplit));
  }

  /**
   * Calculates similarity between alternative split and best split of the node.
   *
   * @param alternativeSplit split with different attribute
   * @param bestSplit best split of the node
   * @return similarity
   */
  protected abstract double calculateSimilarityBetweenNodes(N alternativeSplit, N bestSplit);

  /**
   * Provides a categorical node splitting.
   *
   * @param featuresIndex index of features vector
   * @param node node to be split
   * @param dataSet the data for node indices mapping
   * @return categorical partition
   */
  protected abstract Collection<N> categorical(
      int featuresIndex, N node, FeaturesRecognizable dataSet);

  /**
   * Provides a numeric node splitting.
   *
   * @param featuresIndex index of features vector
   * @param node node to be split
   * @param dataSet the data for node indices mapping
   * @return numeric partition
   */
  protected abstract Collection<N> numerical(
      int featuresIndex, N node, FeaturesRecognizable dataSet);
}
