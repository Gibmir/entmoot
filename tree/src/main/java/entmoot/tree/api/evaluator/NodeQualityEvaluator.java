package entmoot.tree.api.evaluator;

import entmoot.api.data.dataset.DataSet;
import entmoot.tree.api.frame.node.FrameNode;

/**
 * Calculates a quality of node.
 *
 * @author Pavel Usachev
 * @see NodeQualityFunctions
 */
public interface NodeQualityEvaluator {
  
  /**
   * Evaluates a quality of the split.
   *
   * @param frame node for which the calculation will be made
   * @param dataSet the data set
   */
  double evaluate(FrameNode frame, DataSet dataSet);
}
