package entmoot.tree.api.evaluator;

import entmoot.api.data.dataset.DataSet;
import entmoot.tree.api.frame.node.FrameNode;

import java.util.function.ToDoubleBiFunction;

import static entmoot.api.data.utils.dataset.DataSetUtils.getClassesDistributionMapFor;

/**
 * Contains a default node quality evaluation functions like the gini index.
 *
 * @author Pavel Usachev
 * @see NodeQualityEvaluator
 */
public enum NodeQualityFunctions implements NodeQualityEvaluator {
  GINI_INDEX(NodeQualityFunctions::calculateGiniIndex),
  ;

  private final ToDoubleBiFunction<FrameNode, DataSet> qualityFunc;

  NodeQualityFunctions(ToDoubleBiFunction<FrameNode, DataSet> qualityFunc) {
    this.qualityFunc = qualityFunc;
  }

  @Override
  public double evaluate(FrameNode frame, DataSet dataSet) {
    return qualityFunc.applyAsDouble(frame, dataSet);
  }

  private static double calculateGiniIndex(FrameNode frameNode, DataSet dataSet) {
    double sum = 0;
    for (Long distribution :
        getClassesDistributionMapFor(frameNode.getDataSetIndices(), dataSet).values()) {
      sum += Math.pow(distribution, 2) / frameNode.getIndicesCount();
    }
    return sum;
  }
}
