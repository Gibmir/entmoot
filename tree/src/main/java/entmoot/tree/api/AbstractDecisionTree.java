package entmoot.tree.api;

import entmoot.api.data.classifier.Classifier;
import entmoot.tree.api.node.Node;

public abstract class AbstractDecisionTree<N extends Node> implements Classifier {
  protected final N root;

  protected AbstractDecisionTree(N root) {
    this.root = root;
  }
}
