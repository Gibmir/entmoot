package entmoot.tree.api.stopper;

import entmoot.tree.api.node.Node;

import java.util.function.BiPredicate;

/**
 * Represents binary predicate that will stop tree build if it's condition will be true.
 *
 * @param <N> {@link Node decision tree node}
 */
public interface NodeDivisionBuildStopper<N extends Node> extends BiPredicate<N, N> {
  @Override
  boolean test(N currentNode, N bestSplit);
}
