package entmoot.tree.api.stopper;

import entmoot.tree.api.frame.node.FrameNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents abstract composition of stoppers.
 *
 * @param <N> {@link FrameNode frame}
 */
public abstract class AbstractNodeDivisionStoppersComposition<N extends FrameNode>
    implements NodeDivisionBuildStopper<N> {
  private final List<NodeDivisionBuildStopper<? super N>> stoppers = new ArrayList<>();

  protected AbstractNodeDivisionStoppersComposition(NodeDivisionBuildStopper<? super N> stopper) {
    this.stoppers.add(stopper);
  }

  /**
   * Add specified stopper to stopping predicate.
   *
   * @param stopper specified stopper
   */
  public void add(NodeDivisionBuildStopper<N> stopper) {
    this.stoppers.add(stopper);
  }

  @Override
  public boolean test(final N currentNode, final N currentNodeBestSplit) {
    for (NodeDivisionBuildStopper<? super N> stopper : stoppers) {
      if (stopper.test(currentNode, currentNodeBestSplit)) {
        return true;
      }
    }
    return false;
  }
}
