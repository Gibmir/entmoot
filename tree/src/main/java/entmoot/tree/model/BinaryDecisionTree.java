package entmoot.tree.model;

import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.tree.api.AbstractDecisionTree;
import entmoot.tree.model.builder.BinaryDecisionTreeBuilder;

/**
 * Represents classification model. It based on CART algorithm, but with some differences:
 *
 * <ul>
 *   <li>based on frame tree
 *   <li>using of immutable nodes
 *   <li>nodes do not contain data set information(they are clean)
 * </ul>
 */
public class BinaryDecisionTree extends AbstractDecisionTree<BinaryNode> {

  public BinaryDecisionTree(BinaryNode root) {
    super(root);
  }

  @Override
  public Prediction<?> classify(Feature<Double>[] features) {
    BinaryNode currentNode = root;
    while (!currentNode.isLeaf()) {
      if (currentNode.isLeftStep(features)) {
        currentNode = currentNode.getLeft();
      } else {
        currentNode = currentNode.getRight();
      }
    }
    return currentNode.getNodeMostFrequentClass();
  }

  /**
   * Provides builder of the tree.
   *
   * @return tree builder
   */
  public static BinaryDecisionTreeBuilder builder() {
    return new BinaryDecisionTreeBuilder();
  }

  @Override
  public String toString() {
    return "BinaryDecisionTree{" + "root=" + root + '}';
  }
}
