package entmoot.tree.model.builder.pruning;

import entmoot.api.data.classifier.Classifier;
import entmoot.tree.api.builder.pruning.AbstractPrePruningBuilder;
import entmoot.tree.api.stopper.NodeDivisionBuildStopper;
import entmoot.tree.model.BinaryDecisionTree;
import entmoot.tree.model.BinaryNode;
import entmoot.tree.model.builder.BinaryDecisionTreeBuilder;
import entmoot.tree.model.frame.node.BinaryPrePruningNode;
import entmoot.tree.model.frame.splitter.BinaryFrameSplitter;
import entmoot.tree.model.frame.splitter.PrePruningSplitter;
import entmoot.tree.model.frame.stopper.BinaryPrePruningNodeDivisionStoppersComposition;
import entmoot.tree.model.frame.tree.BiPrePruningTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static entmoot.api.data.utils.function.FunctionalUtils.initWith;

public class BinaryPrePruningBuilder extends AbstractPrePruningBuilder {
  private static final Logger LOGGER = LoggerFactory.getLogger(BinaryPrePruningBuilder.class);
  private final BinaryPrePruningNodeDivisionStoppersComposition prePruningStopper;

  public BinaryPrePruningBuilder(BinaryDecisionTreeBuilder builder) {
    super(builder);
    prePruningStopper = new BinaryPrePruningNodeDivisionStoppersComposition(builder.getStopper());
  }

  @Override
  public Classifier buildFromSubset(int... indices) {
    LOGGER.info(
        "Building decision tree. Data set [{}],"
            + " quality evaluator [{}],"
            + " pruning stopper [{}] ,"
            + " threshold [{}],"
            + " indices [{}]",
        trainDataSet(),
        qualityEvaluator,
        prePruningStopper,
        threshold,
        indices.length);
    prePruningStopper.add(getPrePruningStopper());
    return initWith(BiPrePruningTree::getRoot)
        .andThen(BinaryNode::new)
        .andThen(BinaryDecisionTree::new)
        .apply(createFrameTreeFromSubset(indices));
  }

  @Override
  public Classifier build() {
    LOGGER.info(
        "Building decision tree. Data set [{}],"
            + " quality evaluator [{}],"
            + " pruning stopper [{}] ,"
            + " threshold [{}]",
        trainDataSet(),
        qualityEvaluator,
        prePruningStopper,
        threshold);
    prePruningStopper.add(getPrePruningStopper());
    return initWith(BiPrePruningTree::getRoot)
        .andThen(BinaryNode::new)
        .andThen(BinaryDecisionTree::new)
        .apply(createFrameTree());
  }

  private NodeDivisionBuildStopper<BinaryPrePruningNode> getPrePruningStopper() {
    return (current, bestSplit) ->
        qualityEvaluator.evaluate(bestSplit) - qualityEvaluator.evaluate(current) < threshold;
  }

  private BiPrePruningTree createFrameTree() {
    return new BiPrePruningTree(
        trainDataSet(),
        prePruningStopper,
        new PrePruningSplitter(new BinaryFrameSplitter(nodeQualityEvaluator())));
  }

  private BiPrePruningTree createFrameTreeFromSubset(int[] indices) {
    return new BiPrePruningTree(
        trainDataSet(),
        prePruningStopper,
        new PrePruningSplitter(new BinaryFrameSplitter(nodeQualityEvaluator())),
        indices);
  }
}
