package entmoot.tree.model.builder;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.tree.api.builder.AbstractDecisionTreeBuilder;
import entmoot.tree.api.builder.pruning.PrePruningDecisionTreeBuilder;
import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.stopper.NodeDivisionBuildStopper;
import entmoot.tree.model.BinaryDecisionTree;
import entmoot.tree.model.BinaryNode;
import entmoot.tree.model.builder.pruning.BinaryPrePruningBuilder;
import entmoot.tree.model.frame.splitter.BinaryFrameSplitter;
import entmoot.tree.model.frame.tree.BinaryFrameTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static entmoot.api.data.utils.function.FunctionalUtils.initWith;

public class BinaryDecisionTreeBuilder extends AbstractDecisionTreeBuilder {
  private static final Logger LOGGER = LoggerFactory.getLogger(BinaryDecisionTreeBuilder.class);
  private static final NodeDivisionBuildStopper<BinaryFrameNode> NODE_DIVISION_BUILD_STOPPER =
      (currentNode, currentNodeBestSplit) ->
          currentNodeBestSplit.getLeft().getDataSetIndices().isEmpty()
              || currentNodeBestSplit.getRight().getDataSetIndices().isEmpty();

  @Override
  public PrePruningDecisionTreeBuilder withPrePruning() {
    return new BinaryPrePruningBuilder(this);
  }

  @Override
  public BinaryDecisionTree build() {
    LOGGER.debug(
        "Building decision tree. Data set [{}], quality evaluator[{}]",
        dataSet,
        nodeQualityEvaluator);
    return initWith(this::createFrameTree)
        .andThen(BinaryFrameTree::getRoot)
        .andThen(BinaryNode::new)
        .andThen(BinaryDecisionTree::new)
        .apply(dataSet);
  }

  @Override
  public BinaryDecisionTree buildFromSubset(int... indices) {
    LOGGER.debug(
        "Building decision tree. Data set [{}], quality evaluator[{}], indices length [{}]",
        dataSet,
        nodeQualityEvaluator,
        indices.length);
    return initWith(BinaryFrameTree::getRoot)
        .andThen(BinaryNode::new)
        .andThen(BinaryDecisionTree::new)
        .apply(createFrameTreeFromSubset(indices));
  }

  private BinaryFrameTree createFrameTree(FeaturesRecognizable dataSet) {
    return new BinaryFrameTree(
        dataSet, NODE_DIVISION_BUILD_STOPPER, new BinaryFrameSplitter(nodeQualityEvaluator));
  }

  private BinaryFrameTree createFrameTreeFromSubset(int[] indices) {
    return new BinaryFrameTree(
        dataSet,
        NODE_DIVISION_BUILD_STOPPER,
        new BinaryFrameSplitter(nodeQualityEvaluator),
        indices);
  }

  /**
   * Provides current build stopper.
   *
   * @return build stopper
   * @see BinaryPrePruningBuilder#BinaryPrePruningBuilder(BinaryDecisionTreeBuilder)
   */
  public NodeDivisionBuildStopper<BinaryFrameNode> getStopper() {
    return NODE_DIVISION_BUILD_STOPPER;
  }
}
