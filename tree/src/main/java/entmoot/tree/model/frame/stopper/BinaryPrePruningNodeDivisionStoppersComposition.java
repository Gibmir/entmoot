package entmoot.tree.model.frame.stopper;

import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.stopper.AbstractNodeDivisionStoppersComposition;
import entmoot.tree.api.stopper.NodeDivisionBuildStopper;
import entmoot.tree.model.frame.node.BinaryPrePruningNode;

/**
 * Represents division stoppers composition used for pre pruning.
 *
 * @author Pavel Usachev
 */
public class BinaryPrePruningNodeDivisionStoppersComposition
    extends AbstractNodeDivisionStoppersComposition<BinaryPrePruningNode> {
  public BinaryPrePruningNodeDivisionStoppersComposition(
      NodeDivisionBuildStopper<BinaryFrameNode> stopper) {
    super(stopper);
  }
}
