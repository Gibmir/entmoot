package entmoot.tree.model.frame.splitter;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.splitter.FrameSplitter;
import entmoot.tree.model.frame.node.BinaryPrePruningNode;

import static entmoot.api.data.utils.dataset.DataSetUtils.findFrequentClass;

public class PrePruningSplitter implements FrameSplitter<BinaryPrePruningNode> {
  private final BinaryFrameSplitter splitter;

  public PrePruningSplitter(BinaryFrameSplitter splitter) {
    this.splitter = splitter;
  }

  @Override
  public BinaryPrePruningNode split(FeaturesRecognizable dataSet, BinaryPrePruningNode frameNode) {
    final BinaryFrameNode bestSplit = splitter.split(dataSet, frameNode);
    setClass(dataSet, bestSplit.getLeft());
    setClass(dataSet, bestSplit.getRight());
    return new BinaryPrePruningNode(bestSplit, dataSet);
  }

  private void setClass(FeaturesRecognizable dataSet, BinaryFrameNode frameNode) {
    frameNode.setClass(findFrequentClass(frameNode.getDataSetIndices(), dataSet));
  }
}
