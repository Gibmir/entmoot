package entmoot.tree.model.frame.tree;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.tree.api.frame.AbstractFrameTree;
import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.splitter.FrameSplitter;
import entmoot.tree.api.stopper.NodeDivisionBuildStopper;
import entmoot.tree.model.frame.node.BinaryFrame;
import entmoot.tree.model.frame.node.BinaryPrePruningNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.stream.IntStream;

public class BiPrePruningTree extends AbstractFrameTree<BinaryPrePruningNode> {

  public BiPrePruningTree(
      FeaturesRecognizable dataSet,
      NodeDivisionBuildStopper<BinaryPrePruningNode> nodeDivisionBuildStopper,
      FrameSplitter<BinaryPrePruningNode> splitter) {
    super(dataSet, nodeDivisionBuildStopper, splitter);
  }

  public BiPrePruningTree(
      FeaturesRecognizable dataSet,
      NodeDivisionBuildStopper<BinaryPrePruningNode> nodeDivisionBuildStopper,
      FrameSplitter<BinaryPrePruningNode> splitter,
      int[] indices) {
    super(dataSet, nodeDivisionBuildStopper, splitter, indices);
  }

  @Override
  protected BinaryPrePruningNode initRoot() {
    int dataSetSize = dataSet.rowsCount();
    List<Integer> dataSetIndices = new ArrayList<>(dataSetSize);
    for (int i = 0; i < dataSetSize; i++) {
      dataSetIndices.add(i);
    }
    final BinaryFrame binaryFrameNode = new BinaryFrame();
    binaryFrameNode.setDataSetIndices(dataSetIndices);
    return new BinaryPrePruningNode(binaryFrameNode, dataSet);
  }

  @Override
  protected BinaryPrePruningNode initRoot(int[] indices) {
    final BinaryFrame binaryFrameNode = new BinaryFrame();
    binaryFrameNode.setDataSetIndices(
        IntStream.of(indices).collect(ArrayList::new, ArrayList::add, ArrayList::addAll));
    return new BinaryPrePruningNode(binaryFrameNode, dataSet);
  }

  @Override
  protected void copy(BinaryPrePruningNode source, BinaryPrePruningNode target) {
    target.setLeaf(source.isLeaf());
    target.setSurrogate(source.toSurrogate());
    target.setNodeQuality(source.getNodeQuality());
    target.setLeft(source.getLeft());
    target.setRight(source.getRight());
    target.setDataSetIndices(source.getDataSetIndices());
  }

  @Override
  protected void addChildrenOf(
      BinaryPrePruningNode frameNode, Queue<BinaryPrePruningNode> nodesQueue) {
    BinaryFrameNode left = frameNode.getLeft();
    if (!left.getDataSetIndices().isEmpty()) {
      left.leafCheck(dataSet);
      if (!left.isLeaf()) {
        nodesQueue.add(new BinaryPrePruningNode(left, dataSet));
      }
    }
    BinaryFrameNode right = frameNode.getRight();
    if (!right.getDataSetIndices().isEmpty()) {
      right.leafCheck(dataSet);
      if (!right.isLeaf()) {
        nodesQueue.add(new BinaryPrePruningNode(right, dataSet));
      }
    }
  }
}
