package entmoot.tree.model.frame.node;

import entmoot.tree.api.frame.node.AbstractFrameNode;
import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.frame.node.FrameNode;
import entmoot.tree.api.node.surrogate.Surrogate;

public class BinaryFrame extends AbstractFrameNode implements BinaryFrameNode {
  private BinaryFrameNode left;
  private BinaryFrameNode right;

  public BinaryFrame() {
    super();
  }

  public BinaryFrame(FrameNode frameNode, Surrogate surrogate) {
    super(frameNode);
    this.surrogate = surrogate;
    left = new BinaryFrame();
    right = new BinaryFrame();
  }

  @Override
  public BinaryFrameNode getLeft() {
    return left;
  }

  @Override
  public void setLeft(BinaryFrameNode left) {
    this.left = left;
  }

  @Override
  public BinaryFrameNode getRight() {
    return right;
  }

  @Override
  public void setRight(BinaryFrameNode right) {
    this.right = right;
  }
}
