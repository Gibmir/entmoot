package entmoot.tree.model.frame.node;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.node.surrogate.Surrogate;

import java.util.Collection;
import java.util.List;

import static entmoot.api.data.utils.dataset.DataSetUtils.findFrequentClass;

public class BinaryPrePruningNode implements Classifier, BinaryFrameNode {
  private final BinaryFrameNode binaryFrameNode;
  private final DataSet dataSet;

  public BinaryPrePruningNode(BinaryFrameNode binaryFrameNode, DataSet dataSet) {
    this.binaryFrameNode = binaryFrameNode;
    this.dataSet = dataSet;
  }

  @Override
  public Prediction<?> classify(Feature<Double>[] features) {
    BinaryFrameNode currentNode = binaryFrameNode;
    while (!currentNode.isLeaf()) {
      if (currentNode.getLeft() == null || currentNode.getRight() == null) {
        break;
      }
      if (currentNode.toSurrogate().test(features)) {
        currentNode = currentNode.getLeft();
      } else {
        currentNode = currentNode.getRight();
      }
    }
    return findFrequentClass(currentNode.getDataSetIndices(), dataSet);
  }

  @Override
  public List<Integer> getDataSetIndices() {
    return binaryFrameNode.getDataSetIndices();
  }

  @Override
  public double getNodeQuality() {
    return binaryFrameNode.getNodeQuality();
  }

  @Override
  public boolean isLeaf() {
    return binaryFrameNode.isLeaf();
  }

  @Override
  public Prediction<?> getNodeMostFrequentClass() {
    return binaryFrameNode.getNodeMostFrequentClass();
  }

  @Override
  public void setNodeQuality(double quality) {
    binaryFrameNode.setNodeQuality(quality);
  }

  @Override
  public void setDataSetIndices(List<Integer> dataSetIndices) {
    binaryFrameNode.setDataSetIndices(dataSetIndices);
  }

  @Override
  public void setClass(Prediction<?> exampleClass) {
    binaryFrameNode.setClass(exampleClass);
  }

  @Override
  public int getIndicesCount() {
    return binaryFrameNode.getIndicesCount();
  }

  @Override
  public void setLeaf(boolean leaf) {
    binaryFrameNode.setLeaf(leaf);
  }

  @Override
  public void leafCheck(DataSet dataSet) {
    binaryFrameNode.leafCheck(dataSet);
  }

  @Override
  public void setSurrogate(Surrogate surrogate) {
    binaryFrameNode.setSurrogate(surrogate);
  }

  @Override
  public Surrogate toSurrogate() {
    return binaryFrameNode.toSurrogate();
  }

  @Override
  public BinaryFrameNode getLeft() {
    return binaryFrameNode.getLeft();
  }

  @Override
  public void setLeft(BinaryFrameNode left) {
    binaryFrameNode.setLeft(left);
  }

  @Override
  public BinaryFrameNode getRight() {
    return binaryFrameNode.getRight();
  }

  @Override
  public void setRight(BinaryFrameNode right) {
    binaryFrameNode.setRight(right);
  }

  @Override
  public void setSurrogates(Collection<Surrogate> surrogates) {
    binaryFrameNode.setSurrogates(surrogates);
  }

  @Override
  public Collection<Surrogate> getSurrogates() {
    return binaryFrameNode.getSurrogates();
  }
}
