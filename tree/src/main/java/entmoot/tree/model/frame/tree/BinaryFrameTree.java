package entmoot.tree.model.frame.tree;

import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.tree.api.frame.AbstractFrameTree;
import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.splitter.FrameSplitter;
import entmoot.tree.api.stopper.NodeDivisionBuildStopper;
import entmoot.tree.model.frame.node.BinaryFrame;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.stream.IntStream;

import static entmoot.api.data.utils.dataset.DataSetUtils.findFrequentClass;

public class BinaryFrameTree extends AbstractFrameTree<BinaryFrameNode> {
  protected final BinaryFrameNode root;

  public BinaryFrameTree(
      FeaturesRecognizable dataSet,
      NodeDivisionBuildStopper<BinaryFrameNode> nodeDivisionBuildStopper,
      FrameSplitter<BinaryFrameNode> splitter) {
    super(dataSet, nodeDivisionBuildStopper, splitter);
    root = initRoot();
    buildTree();
  }

  public BinaryFrameTree(
      FeaturesRecognizable dataSet,
      NodeDivisionBuildStopper<BinaryFrameNode> nodeDivisionBuildStopper,
      FrameSplitter<BinaryFrameNode> splitter,
      int[] indices) {
    super(dataSet, nodeDivisionBuildStopper, splitter, indices);
    root = initRoot(indices);
    buildTree();
  }

  @Override
  protected BinaryFrame initRoot() {
    int dataSetSize = dataSet.rowsCount();
    List<Integer> dataSetIndices = new ArrayList<>(dataSetSize);
    for (int i = 0; i < dataSetSize; i++) {
      dataSetIndices.add(i);
    }
    final BinaryFrame binaryFrameNode = new BinaryFrame();
    binaryFrameNode.setDataSetIndices(dataSetIndices);
    return binaryFrameNode;
  }

  @Override
  protected BinaryFrame initRoot(int[] indices) {
    final BinaryFrame binaryFrameNode = new BinaryFrame();
    binaryFrameNode.setDataSetIndices(
        IntStream.of(indices).collect(ArrayList::new, ArrayList::add, ArrayList::addAll));
    return binaryFrameNode;
  }

  @Override
  protected void copy(BinaryFrameNode source, BinaryFrameNode target) {
    target.setLeaf(source.isLeaf());
    target.setSurrogate(source.toSurrogate());
    target.setNodeQuality(source.getNodeQuality());
    target.setLeft(source.getLeft());
    target.setRight(source.getRight());
    target.setDataSetIndices(source.getDataSetIndices());
    target.setSurrogates(source.getSurrogates());
  }

  @Override
  protected void addChildrenOf(BinaryFrameNode frameNode, Queue<BinaryFrameNode> nodesQueue) {
    BinaryFrameNode left = frameNode.getLeft();
    if (!left.getDataSetIndices().isEmpty()) {
      left.leafCheck(dataSet);
      left.setClass(findFrequentClass(left.getDataSetIndices(), dataSet));
      if (!left.isLeaf()) {
        nodesQueue.add(left);
      }
    }
    BinaryFrameNode right = frameNode.getRight();
    if (!right.getDataSetIndices().isEmpty()) {
      right.leafCheck(dataSet);
      right.setClass(findFrequentClass(right.getDataSetIndices(), dataSet));
      if (!right.isLeaf()) {
        nodesQueue.add(right);
      }
    }
  }
}
