package entmoot.tree.model.frame.splitter;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.api.data.utils.dataset.DataSetUtils;
import entmoot.api.data.utils.pair.Pair;
import entmoot.tree.api.evaluator.NodeQualityEvaluator;
import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.node.surrogate.CategoricalSurrogate;
import entmoot.tree.api.node.surrogate.NumericalSurrogate;
import entmoot.tree.api.node.surrogate.Surrogate;
import entmoot.tree.api.splitter.AbstractFrameSplitter;
import entmoot.tree.model.frame.node.BinaryFrame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static entmoot.tree.api.frame.node.AbstractFrameNode.LEAF_CLASS_COUNT;
import static java.util.Comparator.comparingDouble;

public class BinaryFrameSplitter extends AbstractFrameSplitter<BinaryFrameNode> {
  private final NodeQualityEvaluator nodeQualityEvaluator;

  public BinaryFrameSplitter(NodeQualityEvaluator nodeQualityEvaluator) {
    this.nodeQualityEvaluator = nodeQualityEvaluator;
  }

  @Override
  protected double calculateSimilarityBetweenNodes(
      BinaryFrameNode alternativeSplit, BinaryFrameNode bestSplit) {
    final int bestSplitIndicesCount = bestSplit.getDataSetIndices().size();
    int similarityCounter = 0;
    similarityCounter += getNumberOfCoincidences(alternativeSplit.getLeft(), bestSplit.getLeft());
    similarityCounter += getNumberOfCoincidences(alternativeSplit.getRight(), bestSplit.getRight());
    return similarityCounter / (double) bestSplitIndicesCount;
  }

  private static int getNumberOfCoincidences(
      BinaryFrameNode alternativeSplit, BinaryFrameNode bestSplit) {
    int coincidence = 0;
    final List<Integer> bestSplitDataSetIndices = bestSplit.getDataSetIndices();
    for (Integer dataSetIndex : alternativeSplit.getDataSetIndices()) {
      if (bestSplitDataSetIndices.contains(dataSetIndex)) {
        coincidence++;
      }
    }
    return coincidence;
  }

  @Override
  protected Collection<BinaryFrameNode> categorical(
      int featuresIndex, BinaryFrameNode node, FeaturesRecognizable dataSet) {
    Feature<Double>[] featuresVector = dataSet.getFeatureValuesVectorBy(featuresIndex);
    List<BinaryFrameNode> splits = new ArrayList<>(featuresVector.length);
    for (Feature<Double> feature : featuresVector) {
      if (feature != null) {
        splits.add(split(node, new CategoricalSurrogate(featuresIndex, feature.get()), dataSet));
      }
    }
    return splits;
  }

  @Override
  protected Collection<BinaryFrameNode> numerical(
      int featureIndex, BinaryFrameNode node, FeaturesRecognizable dataSet) {
    Collection<Double> thresholds = getThresholds(node.getDataSetIndices(), featureIndex, dataSet);
    List<BinaryFrameNode> splits = new ArrayList<>(thresholds.size());
    for (double threshold : thresholds) {
      splits.add(split(node, new NumericalSurrogate(featureIndex, threshold), dataSet));
    }
    return splits;
  }

  private static Collection<Double> getThresholds(
      List<Integer> dataSetIndices, int featuresIndex, FeaturesRecognizable dataSet) {
    List<Pair<Double, Prediction<?>>> featurePerClassPairs = new ArrayList<>();
    for (Integer dataSetIndex : dataSetIndices) {
      Feature<Double>[] exampleVector = dataSet.getFeaturesInRow(dataSetIndex);
      final Double feature = exampleVector[featuresIndex].get();
      if (feature != null) {
        Pair<Double, Prediction<?>> featurePerClassPairBy =
            new Pair<>(feature, dataSet.getPredictionInRow(dataSetIndex));
        featurePerClassPairs.add(featurePerClassPairBy);
      }
    }
    featurePerClassPairs.sort(comparingDouble(Pair::getKey));
    return getThresholdsFromPairs(featurePerClassPairs);
  }

  private static Collection<Double> getThresholdsFromPairs(
      List<Pair<Double, Prediction<?>>> sortedFeaturePerClassPairs) {
    List<Double> thresholds = new ArrayList<>();
    // starts with first pair
    Pair<Double, Prediction<?>> previousPair = sortedFeaturePerClassPairs.get(0);
    for (Pair<Double, Prediction<?>> currentPair : sortedFeaturePerClassPairs) {
      if (isClassHasBeenChanged(previousPair, currentPair)) {
        thresholds.add(calculateThreshold(previousPair, currentPair));
        previousPair = currentPair;
      }
    }
    return thresholds;
  }

  private static boolean isClassHasBeenChanged(
      Pair<Double, Prediction<?>> previousFeaturePerClassPair,
      Pair<Double, Prediction<?>> currentFeaturePerClassPair) {
    return !previousFeaturePerClassPair.getValue().equals(currentFeaturePerClassPair.getValue());
  }

  private static double calculateThreshold(
      Pair<Double, Prediction<?>> previousFeaturePerClassPair,
      Pair<Double, Prediction<?>> currentFeaturePerClassPair) {
    return previousFeaturePerClassPair.getKey()
        + ((currentFeaturePerClassPair.getKey() - previousFeaturePerClassPair.getKey()) / 2);
  }

  /**
   * Split node by specified parameters.
   *
   * @param node frame node
   * @param surrogate surrogate for split
   * @param dataSet train data set
   * @return split
   * @implNote example vector must not be null. You can find checks in {@link
   *     BinaryFrameSplitter#getThresholds(List, int, FeaturesRecognizable) numerical} and {@link
   *     BinaryFrameSplitter#categorical(int, BinaryFrameNode, FeaturesRecognizable) categorical}
   *     methods
   */
  private BinaryFrameNode split(
      BinaryFrameNode node, Surrogate surrogate, FeaturesRecognizable dataSet) {
    BinaryFrameNode copy = new BinaryFrame(node, surrogate);
    for (Integer dataSetIndex : copy.getDataSetIndices()) {
      Feature<Double>[] exampleVector = dataSet.getFeaturesInRow(dataSetIndex);
      if (surrogate.test(exampleVector)) {
        copy.getLeft().getDataSetIndices().add(dataSetIndex);
      } else {
        copy.getRight().getDataSetIndices().add(dataSetIndex);
      }
    }
    copy.setNodeQuality(calculateQuality(copy, dataSet));
    final int classesCount = getNodeClassesCount(dataSet, copy);
    copy.setLeaf(classesCount == LEAF_CLASS_COUNT);
    return copy;
  }

  /**
   * Calculates quality for split with {@link entmoot.tree.api.evaluator.NodeQualityEvaluator}.
   *
   * @param split split of the frame node
   * @param dataSet the data for node indices mapping
   * @return node quality
   */
  private double calculateQuality(BinaryFrameNode split, FeaturesRecognizable dataSet) {
    return nodeQualityEvaluator.evaluate(split.getLeft(), dataSet)
        + nodeQualityEvaluator.evaluate(split.getRight(), dataSet);
  }

  private static int getNodeClassesCount(DataSet dataSet, BinaryFrameNode copy) {
    return DataSetUtils.getClassesDistributionMapFor(copy.getDataSetIndices(), dataSet)
        .keySet()
        .size();
  }
}
