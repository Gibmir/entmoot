package entmoot.tree.model;

import entmoot.api.data.dataset.feature.Feature;
import entmoot.tree.api.frame.node.BinaryFrameNode;
import entmoot.tree.api.node.AbstractNode;
import entmoot.tree.api.node.surrogate.Surrogate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BinaryNode extends AbstractNode<BinaryFrameNode> {
  private static final Logger LOGGER = LoggerFactory.getLogger(BinaryNode.class);
  private BinaryNode left;
  private BinaryNode right;

  public BinaryNode(BinaryFrameNode frame) {
    super(frame);
    BinaryFrameNode frameLeft = frame.getLeft();
    if (frameLeft != null) {
      this.left = new BinaryNode(frameLeft);
    }
    BinaryFrameNode frameRight = frame.getRight();
    if (frameRight != null) {
      this.right = new BinaryNode(frameRight);
    }
  }

  /**
   * Provides left child of binary node.
   *
   * @return left child
   */
  public BinaryNode getLeft() {
    return left;
  }

  /**
   * Provides right child of binary node.
   *
   * @return right child
   */
  public BinaryNode getRight() {
    return right;
  }

  @Override
  public String toString() {
    return "BinaryNode{" + "left=" + left + ", right=" + right + '}';
  }

  /**
   * Checks in which direction to take a step.
   *
   * @param example example vector to check
   * @return true if need to take step to the left
   */
  public boolean isLeftStep(Feature<Double>[] example) {
    if (example[surrogate.getFeaturesIndex()].get() == null) {
      LOGGER.debug("Feature with column index [{}] is null", surrogate.getFeaturesIndex());
      for (Surrogate surrogate : surrogates) {
        final Double surrogateFeature = example[surrogate.getFeaturesIndex()].get();
        if (surrogateFeature != null) {
          LOGGER.trace("Tries check features with column index [{}]", surrogate.getFeaturesIndex());
          return surrogate.checkConditionOf(surrogateFeature);
        }
      }
      return left.indicesCount > right.indicesCount;
    }
    LOGGER.trace("Tries check features with column index [{}]", surrogate.getFeaturesIndex());
    return surrogate.checkConditionOf(example[surrogate.getFeaturesIndex()].get());
  }

  @Override
  public Surrogate toSurrogate() {
    return surrogate;
  }
}
