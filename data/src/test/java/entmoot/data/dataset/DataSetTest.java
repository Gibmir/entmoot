package entmoot.data.dataset;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.exceptions.NoSuchRowIndexException;
import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.api.data.dataset.selectable.Selectable;
import entmoot.api.data.dataset.selectable.exceptions.WrongDataSetFormatException;
import entmoot.api.data.dataset.test.DataSets;
import entmoot.api.data.dataset.test.TestDataSet;
import org.junit.jupiter.api.Test;

import java.util.stream.DoubleStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DataSetTest {

  private final int incorrectRowIndex = -1;

  @Test
  void minorDataSetTest() {
    final TestDataSet dots =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();
    final Selectable minorDataSet = dots.getMinorDataSet();

    assertEquals(2, minorDataSet.rowsCount());
    assertEquals(3, minorDataSet.getPredictionInRow(0).get());
  }

  @Test
  void majorDataSetTest() {
    final TestDataSet dots =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build();
    final Selectable majorDataSet = dots.getMajorDataSet();

    assertEquals(6, majorDataSet.rowsCount());
    assertEquals(2, majorDataSet.getPredictionInRow(0).get());
  }

  @Test
  void getClassByExceptionTest() {
    DataSet testDataSet =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();

    assertThrows(NoSuchRowIndexException.class, () -> testDataSet.getPredictionInRow(incorrectRowIndex));
  }

  @Test
  void getSignsVectorsByExceptionTest() {
    DataSet testDataSet =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();

    assertThrows(
        NoSuchRowIndexException.class,
        () -> testDataSet.getFeaturesInRow(incorrectRowIndex));
  }

  @Test
  void isNumericFeaturesExceptionTest() {
    FeaturesRecognizable testDataSet =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();

    int wrongFeaturesIndex = 10000;

    assertThrows(
        NoSuchRowIndexException.class, () -> testDataSet.isNumericFeatures(wrongFeaturesIndex));
  }

  @Test
  void isCategoricalFeaturesExceptionTest() {
    FeaturesRecognizable testDataSet =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();

    int wrongFeaturesIndex = 10000;

    assertThrows(
        NoSuchRowIndexException.class, () -> testDataSet.isCategoricalFeatures(wrongFeaturesIndex));
  }

  @Test
  void addExampleVectorExceptionTest() {
    Selectable testDataSet =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();

    TestDataSet.TestFeature[] wrongVector = DoubleStream.of(1d, 2d, 3d, 4d, 5d)
      .mapToObj(TestDataSet.TestFeature::new)
      .toArray(TestDataSet.TestFeature[]::new);

    assertThrows(WrongDataSetFormatException.class, () -> testDataSet.addExample(wrongVector,new TestDataSet.TestPrediction(0)));
  }

  @Test
  void addExampleClassExceptionTest() {
    Selectable testDataSet =
        TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(false).build();

    TestDataSet.TestFeature[] vector = DoubleStream.of(2d)
      .mapToObj(TestDataSet.TestFeature::new)
      .toArray(TestDataSet.TestFeature[]::new);
    assertThrows(
        WrongDataSetFormatException.class,
        () -> testDataSet.addExample(vector, new TestDataSet.TestPrediction(-1)));
  }
}
