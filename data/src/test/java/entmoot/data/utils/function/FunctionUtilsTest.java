package entmoot.data.utils.function;

import entmoot.api.data.utils.function.FunctionalUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FunctionUtilsTest {
  @Test
  void testCurry() {
    Assertions.assertEquals(2, FunctionalUtils.curry(this::testBiFunction).apply(1).apply(1));
  }

  @Test
  void testInitWith() {
    Assertions.assertEquals(2, FunctionalUtils.initWith(this::testFunction).apply(2));
  }

  @Test
  void testInitWithBiFunction() {
    Assertions.assertEquals(4, FunctionalUtils.initWith(this::testBiFunction).apply(1, 3));
  }

  @Test
  void testDoubleInitWith() {
    Assertions.assertEquals(
        1.0,
        FunctionalUtils.doubleInitWith(this::testDoubleUnaryOperator).applyAsDouble(1.0),
        1e-5);
  }

  private int testFunction(int i) {
    return i;
  }

  private int testBiFunction(int i, int j) {
    return i + j;
  }

  private double testDoubleUnaryOperator(double i) {
    return i;
  }
}
