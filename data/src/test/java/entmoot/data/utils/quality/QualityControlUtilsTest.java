package entmoot.data.utils.quality;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.api.data.dataset.test.DataSets;
import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.api.data.quality.QualityControl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QualityControlUtilsTest {

  private static final Classifier CLASSIFIER =
      features -> {
        if (features[1].get() <= 2) {
          return new TestDataSet.TestPrediction(1);
        } else {
          return new TestDataSet.TestPrediction(2);
        }
      };
  private static final QualityControl QUALITY =
      new QualityControl(
          TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build());
  private static final double DELTA = 1e-3;
  private static final Prediction<?> EXAMPLE_CLASS = new TestDataSet.TestPrediction(1);

  @Test
  void calculateAccuracyTest() {
    final double accuracy = QUALITY.calculateAccuracy(CLASSIFIER);
    assertEquals(0.333, accuracy, DELTA);
  }

  @Test
  void getPrecisionTest() {
    assertEquals(0.333, QUALITY.calculatePrecision(CLASSIFIER, EXAMPLE_CLASS), DELTA);
  }

  @Test
  void getRecallTest() {
    assertEquals(0.5, QUALITY.calculateRecall(CLASSIFIER, EXAMPLE_CLASS), DELTA);
  }

  @Test
  void getFTest() {
    assertEquals(0.4, QUALITY.calculateMeasure(CLASSIFIER, EXAMPLE_CLASS), DELTA);
  }

  @Test
  void getFWithBetaTest() {
    assertEquals(0.4, QUALITY.calculateMeasure(1, CLASSIFIER, EXAMPLE_CLASS), DELTA);
  }

  @Test
  void getSensitivityTest() {
    assertEquals(0.5, QUALITY.calculateSensitivity(CLASSIFIER, EXAMPLE_CLASS), DELTA);
  }

  @Test
  void getSpecificity() {
    assertEquals(0.5, QUALITY.calculateSensitivity(CLASSIFIER, EXAMPLE_CLASS), DELTA);
  }
}
