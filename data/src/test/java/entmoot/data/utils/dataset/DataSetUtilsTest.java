package entmoot.data.utils.dataset;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.test.DataSets;
import entmoot.api.data.dataset.test.TestDataSet;
import entmoot.api.data.utils.dataset.DataSetUtils;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DataSetUtilsTest {
  private final static DataSet TEST_DATA_SET = TestDataSet.builder().setTestDataSet(DataSets.DOTS).setCategorical(true).build();

  @Test
  void getClassesCountTest() {
    assertEquals(3, DataSetUtils.getClassesCountFor(TEST_DATA_SET));
  }

  @Test
  void getMinorClassTest() {
    assertEquals(3, DataSetUtils.getMinorClassFrom(TEST_DATA_SET).get());
  }

  @Test
  void getFrequentClassTest() {
    final List<Integer> indices = IntStream.range(0, TEST_DATA_SET.rowsCount()).boxed().collect(Collectors.toList());

    assertEquals(2, DataSetUtils.findFrequentClass(indices, TEST_DATA_SET).get());
  }
}
