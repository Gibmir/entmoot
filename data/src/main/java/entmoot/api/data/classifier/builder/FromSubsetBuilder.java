package entmoot.api.data.classifier.builder;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.DataSet;

/**
 * Represents a classifier builder that use a part of the {@link DataSet}.
 *
 * @author Pavel Usachev
 */
public interface FromSubsetBuilder extends ClassifierBuilder {
  /**
   * Builds classifier from subset by indices of data set.
   *
   * @param indices indices of data set
   * @return classifier
   */
  Classifier buildFromSubset(int... indices);
}
