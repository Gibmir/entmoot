package entmoot.api.data.classifier.builder;

import entmoot.api.data.classifier.Classifier;

/**
 * Represents a classifier builder.
 *
 * @author Pavel Usachev
 */
public interface ClassifierBuilder {
  /**
   * Builds the classifier.
   *
   * @return classifier
   */
  Classifier build();
}
