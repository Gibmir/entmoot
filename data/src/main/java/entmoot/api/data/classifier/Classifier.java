package entmoot.api.data.classifier;

import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;

/**
 * Represents a classifying algorithms.
 *
 * @author Pavel Usachev
 */
@FunctionalInterface
public interface Classifier {
  /**
   * Predicts class represent in {@link Integer} format by features vector.
   *
   * @param features example vector of features for predicting class
   * @return predicted class
   */
  Prediction<?> classify(Feature<Double>[] features);
}
