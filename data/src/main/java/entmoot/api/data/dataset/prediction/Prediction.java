package entmoot.api.data.dataset.prediction;

import java.util.function.Supplier;

/**
 * Represents class prediction.
 *
 * @param <T> class data type
 */
public interface Prediction<T> extends Supplier<T> {

}
