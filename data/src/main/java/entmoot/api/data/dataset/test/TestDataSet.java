package entmoot.api.data.dataset.test;

import entmoot.api.data.dataset.exceptions.NoSuchRowIndexException;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.api.data.dataset.recognizable.FeaturesRecognizable;
import entmoot.api.data.dataset.selectable.Selectable;
import entmoot.api.data.dataset.selectable.exceptions.WrongDataSetFormatException;
import entmoot.api.data.utils.dataset.DataSetUtils;
import entmoot.api.data.utils.pair.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents data sets for testing.
 *
 * @author Pavel Usachev
 */
public class TestDataSet implements Selectable, FeaturesRecognizable {
  private boolean isCategorical = false;
  private List<Feature<Double>[]> examplesFeaturesVectors = new ArrayList<>();
  private List<Prediction<?>> classes = new ArrayList<>();

  /**
   * Provides builder for creating test data set.
   *
   * @return test data set builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Constructor.
   *
   * @param isCategorical is categorical data set flag
   * @param exampleVectors features
   * @param classes classes
   */
  public TestDataSet(boolean isCategorical, Double[][] exampleVectors, Integer[] classes) {
    this.isCategorical = isCategorical;
    examplesFeaturesVectors.addAll(
        Stream.of(exampleVectors).map(TestDataSet::map).collect(Collectors.toList()));
    this.classes.addAll(
        Arrays.stream(classes).map(TestPrediction::new).collect(Collectors.toList()));
  }

  private TestDataSet(
      List<Feature<Double>[]> examplesFeaturesVectors, List<Prediction<?>> classes) {
    this.examplesFeaturesVectors = examplesFeaturesVectors;
    this.classes = classes;
  }

  private static TestFeature[] map(Double[] array) {
    TestFeature[] testFeatures = new TestFeature[array.length];
    for (int i = 0; i < array.length; i++) {
      testFeatures[i] = new TestFeature(array[i]); //
    }
    return testFeatures;
  }

  @Override
  public Feature<Double>[] getFeaturesInRow(int dataSetRowIndex) {
    if (dataSetRowIndex < 0 || dataSetRowIndex > examplesFeaturesVectors.size()) {
      throw new NoSuchRowIndexException(dataSetRowIndex, examplesFeaturesVectors.size());
    }
    return examplesFeaturesVectors.get(dataSetRowIndex);
  }

  @Override
  public Prediction<?> getPredictionInRow(int dataSetRowIndex) {
    if (dataSetRowIndex < 0 || dataSetRowIndex > classes.size()) {
      throw new NoSuchRowIndexException(dataSetRowIndex, examplesFeaturesVectors.size());
    }

    return classes.get(dataSetRowIndex);
  }

  @Override
  public int rowsCount() {
    return examplesFeaturesVectors.size();
  }

  @Override
  public Feature<Double>[] getFeatureValuesVectorBy(int featureIndex) {
    return examplesFeaturesVectors.stream()
        .map(features -> features[featureIndex])
        .distinct()
        .toArray(TestFeature[]::new);
  }

  @Override
  public boolean isCategoricalFeatures(int featuresIndex) {
    if (featuresIndex > getFeaturesCount()) {
      throw new NoSuchRowIndexException(
          "No such row index in data set:" + featuresIndex + ". Border:" + getFeaturesCount());
    }
    return isCategorical;
  }

  @Override
  public boolean isNumericFeatures(int featuresIndex) {
    if (featuresIndex > getFeaturesCount()) {
      throw new NoSuchRowIndexException(
          "No such row index in data set:" + featuresIndex + ". Border:" + getFeaturesCount());
    }
    return !isCategorical;
  }

  @Override
  public int getFeaturesCount() {
    return examplesFeaturesVectors.get(0).length;
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < examplesFeaturesVectors.size(); i++) {
      stringBuilder
          .append("vector:")
          .append(Arrays.toString(examplesFeaturesVectors.get(i)))
          .append(" class:")
          .append(classes.get(i))
          .append('\n');
    }
    return stringBuilder.toString();
  }

  @Override
  public Selectable getMajorDataSet() {
    List<Pair<Feature<Double>[], Prediction<?>>> majorPairs = new ArrayList<>();
    Prediction<?> majorClass = DataSetUtils.getMajorClassFrom(this);
    for (int i = 0; i < classes.size(); i++) {
      if (classes.get(i).equals(majorClass)) {
        majorPairs.add(new Pair<>(examplesFeaturesVectors.get(i), classes.get(i)));
      }
    }
    return getDataSetFrom(majorPairs);
  }

  @Override
  public Selectable getMinorDataSet() {
    List<Pair<Feature<Double>[], Prediction<?>>> minorPairs = new ArrayList<>();
    Prediction<?> minorClass = DataSetUtils.getMinorClassFrom(this);
    for (int i = 0; i < classes.size(); i++) {
      if (classes.get(i).equals(minorClass)) {
        minorPairs.add(new Pair<>(examplesFeaturesVectors.get(i), minorClass));
      }
    }
    return getDataSetFrom(minorPairs);
  }

  private TestDataSet getDataSetFrom(List<Pair<Feature<Double>[], Prediction<?>>> pairs) {
    List<Prediction<?>> newClasses = new ArrayList<>();
    List<Feature<Double>[]> newExampleVectors = new ArrayList<>();
    for (Pair<Feature<Double>[], Prediction<?>> pair : pairs) {
      newExampleVectors.add(pair.getKey());
      newClasses.add(pair.getValue());
    }
    return new TestDataSet(newExampleVectors, newClasses);
  }

  @Override
  public void addExample(Feature<Double>[] exampleFeaturesVector, Prediction<?> exampleClass) {
    if (exampleFeaturesVector.length != examplesFeaturesVectors.get(0).length) {
      throw new WrongDataSetFormatException(
          "Wrong format of data"
              + ":"
              + Arrays.toString(exampleFeaturesVector)
              + "-"
              + exampleClass);
    }
    examplesFeaturesVectors.add(exampleFeaturesVector);
    classes.add(exampleClass);
  }

  /** Represents test data set builder. */
  public static final class Builder {
    private DataSets testDataSet;
    private boolean isCategorical;

    /**
     * Provides current test data set.
     *
     * @return test data set
     */
    public DataSets getTestDataSet() {
      return testDataSet;
    }

    /**
     * Sets current test data set.
     *
     * @param testDataSet data set for testing
     * @return this
     */
    public Builder setTestDataSet(DataSets testDataSet) {
      this.testDataSet = testDataSet;
      return this;
    }

    /**
     * Provides current features type flag.
     *
     * @return true if data set has categorical features type
     */
    public boolean isCategorical() {
      return isCategorical;
    }

    /**
     * Sets current features type flag.
     *
     * @param categorical true if data set has categorical features type
     * @return this
     */
    public Builder setCategorical(boolean categorical) {
      isCategorical = categorical;
      return this;
    }

    /**
     * Builds {@link TestDataSet} with specified parameters.
     *
     * @return data set for testing
     */
    public TestDataSet build() {

      if (testDataSet == null) {
        return new TestDataSet(
            isCategorical, DataSets.DOTS.getExampleVectors(), DataSets.DOTS.getClassesVectors());
      }
      return new TestDataSet(
          isCategorical, testDataSet.getExampleVectors(), testDataSet.getClassesVectors());
    }
  }

  public static class TestPrediction implements Prediction<Integer> {
    private final Integer integer;

    public TestPrediction(Integer integer) {
      this.integer = integer;
    }

    @Override
    public Integer get() {
      return integer;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      TestPrediction that = (TestPrediction) o;
      return Objects.equals(integer, that.integer);
    }

    @Override
    public int hashCode() {
      return integer.hashCode();
    }
  }

  public static class TestFeature implements Feature<Double>, Prediction<Double> {
    private final Double data;

    public TestFeature(Double data) {
      this.data = data;
    }

    @Override
    public Double get() {
      return data;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      TestFeature that = (TestFeature) o;
      return Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
      return Objects.hash(data);
    }

    @Override
    public String toString() {
      return "TestFeature{" + "data=" + data + '}';
    }
  }
}
