package entmoot.api.data.dataset.selectable;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;
import entmoot.api.data.dataset.selectable.exceptions.WrongDataSetFormatException;

/**
 * Represents a data set for sampling.
 *
 * @author Pavel Usachev
 * @see DataSet
 */
public interface Selectable extends DataSet {
  /**
   * Produces a major data set.
   *
   * @return {@link Selectable major data set}
   */
  Selectable getMajorDataSet();

  /**
   * Produces a minor data set.
   *
   * @return {@link Selectable minor data set}
   */
  Selectable getMinorDataSet();

  /**
   * Adds a new data set example. This example consist from a <b>example vector of features</b> and
   * a <b>class</b>.
   *
   * @param exampleFeaturesVector represent an example vector of features
   * @param examplePrediction represent a class
   * @throws WrongDataSetFormatException if example vector of features is wrong format
   */
  void addExample(Feature<Double>[] exampleFeaturesVector, Prediction<?> examplePrediction);
}
