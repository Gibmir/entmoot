package entmoot.api.data.dataset;

import entmoot.api.data.dataset.exceptions.NoSuchRowIndexException;
import entmoot.api.data.dataset.feature.Feature;
import entmoot.api.data.dataset.prediction.Prediction;

/**
 * Represents a data set. It is a collection of pairs of examples and classes which can be received
 * by index.
 *
 * @author Pavel Usachev
 */
public interface DataSet {
  /**
   * Produces <b>example vector of features</b> by its index in data set.
   *
   * @param dataSetRowIndex index of row in data set
   * @return <b>example vector of features</b>
   * @throws NoSuchRowIndexException if row with this index does not contains in data set
   */
  Feature<Double>[] getFeaturesInRow(int dataSetRowIndex);

  /**
   * Produces a <b>class</b> in {@link Integer} representation by its index in data set.
   *
   * @param dataSetRowIndex index of row in data set
   * @return <b>class</b>
   * @throws NoSuchRowIndexException if row with this index does not contains in data set
   */
  Prediction<?> getPredictionInRow(int dataSetRowIndex);

  /**
   * Produces a size of data set.
   *
   * @return size
   */
  int rowsCount();

  /**
   * Produces a vector of feature values by its index.
   *
   * @param featureIndex index of feature
   * @return vector of features
   */
  Feature<Double>[] getFeatureValuesVectorBy(int featureIndex);

  /**
   * Produces a count of features.
   *
   * @return count of features
   */
  int getFeaturesCount();
}
