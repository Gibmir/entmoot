package entmoot.api.data.dataset.feature;

/** Represents data set double feature. */
public interface DoubleFeature extends Feature<Double> {}
