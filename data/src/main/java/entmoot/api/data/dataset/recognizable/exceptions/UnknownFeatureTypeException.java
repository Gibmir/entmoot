package entmoot.api.data.dataset.recognizable.exceptions;

/**
 * Thrown to indicate that the data set can't recognize sign for specified index.
 *
 * @author Pavel Usachev
 */
public class UnknownFeatureTypeException extends RuntimeException {

  public UnknownFeatureTypeException(String message) {
    super(message);
  }
}
