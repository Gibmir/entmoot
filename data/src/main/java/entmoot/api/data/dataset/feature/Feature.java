package entmoot.api.data.dataset.feature;

import java.util.function.Supplier;

/**
 * Represents data set feature.
 *
 * @param <T> feature data type
 */
public interface Feature<T> extends Supplier<T> {
}
