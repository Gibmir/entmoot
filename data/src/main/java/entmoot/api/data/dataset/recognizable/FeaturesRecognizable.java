package entmoot.api.data.dataset.recognizable;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.recognizable.exceptions.UnknownFeatureTypeException;

/**
 * Represents a data set which can recognize the type of features.
 *
 * @author Pavel Usachev
 * @see DataSet
 */
public interface FeaturesRecognizable extends DataSet {
  /**
   * Returns a true if type of a features vector received by the index is categorical.
   *
   * @param featuresIndex index by which features vector received
   * @throws UnknownFeatureTypeException if data set can't determine what type of feature
   */
  boolean isCategoricalFeatures(int featuresIndex);

  /**
   * Returns a true if type of a features vector received by the index is numerical.
   *
   * @param featuresIndex index by which features vector received
   * @throws UnknownFeatureTypeException if data set can't determine what type of feature
   */
  boolean isNumericFeatures(int featuresIndex);
}
