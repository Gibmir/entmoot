package entmoot.api.data.dataset.selectable.exceptions;

/**
 * Thrown to indicate that added data set info is wrong formatted.
 *
 * @author Pavel Usachev
 */
public class WrongDataSetFormatException extends RuntimeException {

  public WrongDataSetFormatException(String message) {
    super(message);
  }
}
