package entmoot.api.data.dataset.exceptions;

/**
 * Thrown to indicate that there is no available data for specified index.
 *
 * @author Pavel Usachev
 */
public class NoSuchRowIndexException extends RuntimeException {

  public NoSuchRowIndexException(String message) {
    super(message);
  }

  public NoSuchRowIndexException(int dataSetRowIndex, int border) {
    super("No such row index in data set:" + dataSetRowIndex + ". Border:" + border);
  }
}
