package entmoot.api.data.quality;

import entmoot.api.data.classifier.Classifier;

@FunctionalInterface
public interface QualityEvaluator {
  /**
   * Evaluates specified classifier quality.
   *
   * @param classifier prediction model
   * @return classifier quality
   */
  double evaluate(Classifier classifier);
}
