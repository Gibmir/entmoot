package entmoot.api.data.quality;

import entmoot.api.data.classifier.Classifier;
import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.prediction.Prediction;

/**
 * Provides methods for evaluation quality of classifier.
 *
 * @author Pavel Usachev
 */
public class QualityControl {
  private final DataSet testDataSet;

  public QualityControl(DataSet testDataSet) {
    this.testDataSet = testDataSet;
  }

  /**
   * Calculates accuracy of classifier.
   *
   * @param classifier the classifier
   * @return accuracy of classifier
   */
  public double calculateAccuracy(Classifier classifier) {
    final int dataSetSize = testDataSet.rowsCount();
    double properlyClassified = 0;
    for (int i = 0; i < dataSetSize; i++) {
      final Prediction<?> predicted =
          classifier.classify(testDataSet.getFeaturesInRow(i));
      if (predicted.equals(testDataSet.getPredictionInRow(i))) {
        properlyClassified++;
      }
    }
    return properlyClassified / dataSetSize;
  }

  /**
   * Provides precision.
   *
   * @return precision
   */
  public double calculatePrecision(Classifier classifier, Prediction<?> examplePrediction) {
    final AnswersDistribution answersDistribution =
        calculateAnswersDistribution(classifier, examplePrediction);
    return calculatePrecision(answersDistribution);
  }

  private double calculatePrecision(AnswersDistribution answersDistribution) {
    return answersDistribution.truePositive
        / (answersDistribution.truePositive + answersDistribution.falsePositive);
  }

  /**
   * Provides recall.
   *
   * @return recall
   */
  public double calculateRecall(Classifier classifier, Prediction<?> examplePrediction) {
    final AnswersDistribution answersDistribution =
        calculateAnswersDistribution(classifier, examplePrediction);
    return calculateRecall(answersDistribution);
  }

  private double calculateRecall(AnswersDistribution answersDistribution) {
    return answersDistribution.truePositive
        / (answersDistribution.truePositive + answersDistribution.falseNegative);
  }

  /**
   * Provides f measure.
   *
   * @return f measure
   */
  public double calculateMeasure(Classifier classifier, Prediction<?> examplePrediction) {
    final AnswersDistribution answersDistribution =
        calculateAnswersDistribution(classifier, examplePrediction);
    final double precision = calculatePrecision(answersDistribution);
    final double recall = calculateRecall(answersDistribution);
    return 2 * (precision * recall) / (precision + recall);
  }

  /**
   * Provides f measure.
   *
   * @param beta the beta coefficient(defines preference between precision and recall)
   * @return f measure
   */
  public double calculateMeasure(
      double beta, Classifier classifier, Prediction<?> examplePrediction) {
    final double measure = calculateMeasure(classifier, examplePrediction);
    return (measure / 2) * (Math.pow(beta, 2) + 1);
  }

  /**
   * Provides sensitivity.
   *
   * @return sensitivity
   */
  public double calculateSensitivity(Classifier classifier, Prediction<?> examplePrediction) {
    final AnswersDistribution answersDistribution =
        calculateAnswersDistribution(classifier, examplePrediction);
    return answersDistribution.truePositive
        / (answersDistribution.truePositive + answersDistribution.falseNegative);
  }

  /**
   * Provides specificity.
   *
   * @return specificity
   */
  public double calculateSpecificity(Classifier classifier, Prediction<?> examplePrediction) {
    final AnswersDistribution answersDistribution =
        calculateAnswersDistribution(classifier, examplePrediction);
    return answersDistribution.trueNegative
        / (answersDistribution.trueNegative + answersDistribution.falsePositive);
  }

  private AnswersDistribution calculateAnswersDistribution(
      Classifier classifier, Prediction<?> examplePrediction) {
    final AnswersDistribution answersDistribution = new AnswersDistribution();
    for (int i = 0; i < testDataSet.rowsCount(); i++) {
      final Prediction<?> classified =
          classifier.classify(testDataSet.getFeaturesInRow(i));
      if (testDataSet.getPredictionInRow(i).equals(examplePrediction)) {
        if (classified.equals(examplePrediction)) {
          answersDistribution.truePositive++;
        } else {
          answersDistribution.falseNegative++;
        }
      } else {
        if (classified.equals(examplePrediction)) {
          answersDistribution.falsePositive++;
        } else {
          answersDistribution.trueNegative++;
        }
      }
    }
    return answersDistribution;
  }

  private static class AnswersDistribution {
    private double truePositive;
    private double falsePositive;
    private double trueNegative;
    private double falseNegative;
  }
}
