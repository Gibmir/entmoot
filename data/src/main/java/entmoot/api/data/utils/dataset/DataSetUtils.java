package entmoot.api.data.utils.dataset;

import entmoot.api.data.dataset.DataSet;
import entmoot.api.data.dataset.prediction.Prediction;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Provides static methods for working with a {@link DataSet data set}.
 *
 * @author Pavel Usachev
 */
public class DataSetUtils {
  private DataSetUtils() {}

  /**
   * Produces count of classes in data set.
   *
   * @param dataSet {@link DataSet}
   * @return count of classes
   */
  public static long getClassesCountFor(DataSet dataSet) {
    return IntStream.range(0, dataSet.rowsCount())
        .mapToObj(dataSet::getPredictionInRow)
        .distinct()
        .count();
  }

  /**
   * Produces classes distribution map for subset.
   *
   * @param dataSet {@link DataSet}
   * @param indices data set indices
   * @return classes distribution map
   */
  public static Map<Prediction<?>, Long> getClassesDistributionMapFor(
      List<Integer> indices, DataSet dataSet) {
    Map<Prediction<?>, Long> classesDistributionMap = new HashMap<>();
    for (Integer index : indices) {
      classesDistributionMap.compute(
          dataSet.getPredictionInRow(index),
          (key, classOccurrences) -> computeClassOccurrences(classOccurrences));
    }
    return classesDistributionMap;
  }

  /**
   * Produces classes distribution map.
   *
   * @param dataSet {@link DataSet}
   * @return classes distribution map
   */
  public static Map<Prediction<?>, Long> getClassesDistributionMapFor(DataSet dataSet) {
    return IntStream.range(0, dataSet.rowsCount())
        .mapToObj(dataSet::getPredictionInRow)
        .collect(Collectors.groupingBy(prediction -> prediction, Collectors.counting()));
  }

  private static Long computeClassOccurrences(Long classOccurrences) {
    if (classOccurrences == null) {
      return 1L;
    }
    return ++classOccurrences;
  }

  /**
   * Produces a minor class presented in current data set.
   *
   * @param dataSet current {@link DataSet data set}
   * @return a minor class
   */
  public static Prediction<?> getMinorClassFrom(DataSet dataSet) {
    return getClassesDistributionMapFor(dataSet).entrySet().stream()
        .min(Comparator.comparingDouble(Map.Entry::getValue))
        .map(Map.Entry::getKey)
        .orElseThrow(NullPointerException::new);
  }

  /**
   * Produces a major class presented in current data set.
   *
   * @param dataSet current {@link DataSet data set}
   * @return a minor class
   */
  public static Prediction<?> getMajorClassFrom(DataSet dataSet) {
    return getClassesDistributionMapFor(dataSet).entrySet().stream()
        .max(Comparator.comparingLong(Map.Entry::getValue))
        .map(Map.Entry::getKey)
        .orElseThrow(NullPointerException::new);
  }

  /**
   * Finds a frequent class in data set.
   *
   * @param indices indices of data set
   * @param dataSet {@link DataSet}
   * @return int class representation
   */
  public static Prediction<?> findFrequentClass(List<Integer> indices, DataSet dataSet) {
    return getClassesDistributionMapFor(indices, dataSet).entrySet().stream()
        .max(Comparator.comparingLong(Map.Entry::getValue))
        .orElseThrow(NullPointerException::new)
        .getKey();
  }
}
