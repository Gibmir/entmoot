package entmoot.api.data.utils.function;

import java.util.function.BiFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;

/**
 * Util class that provides method for functional chaining and currying.
 *
 * @author Pavel Usachev
 */
public class FunctionalUtils {
  private FunctionalUtils() {}

  /**
   * Init chaining with provided function.
   *
   * @param biFunction binary function which starts chaining
   * @param <T> function first argument type
   * @param <U> function second argument type
   * @param <R> function return type
   * @return binary function that performs chaining
   */
  public static <T, U, R> BiFunction<T, U, R> initWith(BiFunction<T, U, R> biFunction) {
    return biFunction;
  }

  /**
   * Init chaining with provided function.
   *
   * @param function function which starts chaining
   * @param <T> function argument type
   * @param <R> function return type
   * @return function that performs chaining
   */
  public static <T, R> Function<T, R> initWith(Function<T, R> function) {
    return function;
  }

  /**
   * Init chaining with provided function.
   *
   * @param doubleUnaryOperator double function which starts chaining
   * @return double function that performs chaining
   */
  public static DoubleUnaryOperator doubleInitWith(DoubleUnaryOperator doubleUnaryOperator) {
    return doubleUnaryOperator;
  }

  /**
   * Provides currying.
   *
   * @param biFunction binary function which starts chaining
   * @param <T> function first argument type
   * @param <U> function second argument type
   * @param <R> function return type
   * @return curried function
   */
  public static <T, U, R> Function<T, Function<U, R>> curry(BiFunction<T, U, R> biFunction) {
    return t -> u -> biFunction.apply(t, u);
  }
}
