package entmoot.api.data.utils.pair;

/**
 * Represents an immutable pair of key and value.
 *
 * @param <K> the pair key
 * @param <V> the pair value
 * @author Pavel Usachev
 */
public class Pair<K, V> {
  private final K key;
  private final V value;

  public Pair(K key, V value) {
    this.key = key;
    this.value = value;
  }

  /**
   * Provides the pair key.
   *
   * @return key
   */
  public K getKey() {
    return key;
  }

  /**
   * Provides the pair value.
   *
   * @return value.
   */
  public V getValue() {
    return value;
  }

  @Override
  public String toString() {
    return '{' + "key=" + key + ", value=" + value + '}';
  }
}
