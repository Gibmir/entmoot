# entmoot
The name of the project is a reference to the works of Tolkien. 
Entmoot was the name for a meeting of the Ents of Fangorn Forest.

You can find trello board on <https://trello.com/b/vAEWn94A/entmoot>.
Project represents a machine learning library. It consist of several modules:
---
## data:

* data set API; 
* classifier API;
* quality control API;
* useful utils for data set;
* data set's information provider;
* builder of correlation matrix for data set.
---
## tree:

* tree builders;
* node quality evaluator;
* classification and regression tree (CART) algorithm implementation.
---
## composition:

* ensemble API;
* learners API;
* bagging;
* boosting.
---
## palantir:

* csv file reader;
* structure for data analyze.
 
## shelob:

* neural network lib